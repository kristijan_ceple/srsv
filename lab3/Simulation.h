//
// Created by kikyy_99 on 14. 12. 2020..
//

#ifndef LAB3_SIMULATION_H
#define LAB3_SIMULATION_H


#include <vector>
#include <random>
#include <list>
#include <unordered_set>
#include "Lift.h"
#include "LiftController.h"
#include "Graphics.h"
#include "Passenger.h"

class Simulation {
private:
    // Building and lifts
    std::vector<util::Floor> building{my_global_context::CREATION_BUILDING_MAX_FLOOR + 1};
    Lift lab3Lift{0, my_global_context::LIFT1_CAPACITY, 0, building};
    LiftController liftController{building, &lab3Lift};

    // Severity level
    util::LoadSeverity currLoadSeverity = util::LoadSeverity::MEDIUM;

    // Graphics-related fields
    Graphics graphics{building};

    // People-related fields
    std::unordered_set<unsigned int> available_passenger_ids;
    static constexpr unsigned int PASSENGER_ID_LOWEST = 48;
    static constexpr unsigned int PASSENGER_ID_HIGHEST = 126;
    std::list<Passenger> passengers{};

    bool checkChance(double d);
    unsigned int generateN();
    void passengerGeneration();
    void generationSleep();
    void joinPassengers();
public:
    Simulation();
    [[noreturn]] void run();
    ~Simulation();
};


#endif //LAB3_SIMULATION_H
