//
// Created by kikyy_99 on 06.01.21.
//

#ifndef LAB3_NOT_IMPLEMENTED_EXCEPTION_H
#define LAB3_NOT_IMPLEMENTED_EXCEPTION_H


#include <exception>
#include <string>

class not_implemented_exception : std::exception {
private:
    const std::string reason;
public:
    not_implemented_exception(std::string reason);
    const char* what();
};


#endif //LAB3_NOT_IMPLEMENTED_EXCEPTION_H
