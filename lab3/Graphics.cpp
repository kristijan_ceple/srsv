//
// Created by kikyy_99 on 14. 12. 2020..
//
#include <thread>
#include <iostream>
#include <fmt/format.h>
#include "Graphics.h"
#include "util.h"
#include "not_implemented_exception.h"
#include "Lift.h"

using namespace std;

std::ostream& operator<<(std::ostream& out, std::array<std::string,15>& arr);

Graphics::Graphics(vector<util::Floor> &building)  : building{building}
{
    if(building.size() <= 1) {
        throw invalid_argument("Building is supposed to have at least 2 floors!");
    } else if(building.size() != 4) {
        throw not_implemented_exception("Graphics component only implemented for buildings with 4 floors!");
    }

    // Initialise floor exited ASCII positions
    for(int i = 0; i <= my_global_context::CREATION_BUILDING_MAX_FLOOR; i++) {
        this->floorExitedHorizontalTrackers[i] = my_global_context::ASCII_EXITED_HORIZONTAL_START;
    }

    auto ptr = const_cast<std::array<unsigned int, my_global_context::CREATION_BUILDING_MAX_FLOOR + 1>*>(&this->floorVerticalTrackers);
    for(int j = 3, i = my_global_context::CREATION_BUILDING_MAX_FLOOR; i >= 0; i--, j += 2) {
        (*ptr)[i] = j;
    }
}

void Graphics::run() {
    this->graphicsThread = make_unique<thread>(
            ref(*this)
    );
}

[[noreturn]] void Graphics::operator()() {
    cout << "[GFX] Starting up" << endl;
    this->draw();

    util::GraphicsMessage tmpGraphicsMessage;
    while(true) {
        // Code below takes a string from the queue and prints it
        {
            unique_lock<mutex> ul(my_global_context::graphicsMutex);
            if(my_global_context::graphicsQueue.empty()) {
                // Wait - but only if the queue is empty!
                my_global_context::graphicsCv.wait(ul, [this]() {
                    return (my_global_context::graphicsMessageReady);
                });

                // A weird SIGSEGV bug can happen here if the queue isn't updated fast enough
                if(my_global_context::graphicsQueue.empty()) {
                    continue;
                }

                my_global_context::graphicsMessageReady = false;       // Lower the flag so signal that we've popped the message
            }

            tmpGraphicsMessage = my_global_context::graphicsQueue.front();
            my_global_context::graphicsQueue.pop();
        }

        // cout << tmpGraphicsMessage << endl;
        this->processMessage(tmpGraphicsMessage);
    }
}

Graphics::~Graphics() {
    this->graphicsThread->join();
}

void Graphics::draw() {
//    cout << "Drawing!" << endl;
    cout << this->currentAsciiGraphics;
}

ostream& operator<<(ostream& out, std::array<string,15>& arr)
{
    for(const string& currStr : arr) {
        out << currStr << endl;
    }

    return out;
}

void Graphics::processMessage(util::GraphicsMessage toProcess) {
    util::SimulationPurpose tmpEntity = toProcess.purpose;
    switch(tmpEntity) {
        case util::SimulationPurpose::LIFT:
            {
//                cout << "Lift graphics message!" << endl;
//                cout << any_cast<string>(toProcess.data["toPrint"]) << endl;
                auto liftState =  any_cast<Lift::LiftState>(toProcess.data["liftState"]);
                auto cmd = any_cast<Lift::Command>(toProcess.data["command"]);
                auto destinationFloor = any_cast<unsigned int>(toProcess.data["destinationFloor"]);
                auto liftID = any_cast<unsigned int>(toProcess.data["liftID"]);
                this->updateAscii4Lift(liftID, liftState, cmd, destinationFloor);
//                this->printCurrLiftStateAndOrder(liftID, liftState, cmd, destinationFloor);

                if(this->prevAsciiGraphics != this->currentAsciiGraphics) {
                    this->draw();
                }

                this->lift1PreviousState = liftState;
                this->prevAsciiGraphics = this->currentAsciiGraphics;
            }
            break;
        case util::SimulationPurpose::PASSENGER:
        {
            cout << "Passenger graphics message!" << endl;
            cout << any_cast<string>(toProcess.data["toPrint"]) << endl;

            auto tmpCycleState = any_cast<util::PassengerCycleState>(toProcess.data["cycleState"]);
            switch(tmpCycleState) {
                case util::PassengerCycleState::WAITING:
                {
                    // Have to add the ID of this passenger to the display
                    auto floor = any_cast<unsigned int>(toProcess.data["from"]);
                    auto to = any_cast<unsigned int>(toProcess.data["to"]);

                    unsigned int x = this->floorVerticalTrackers[floor];
                    //unsigned int y = this->floorHorizontalTrackers[floor]++;

                    char id = any_cast<char>(toProcess.data["id"]);
                    //this->currentAsciiGraphics[x][y] = id;

                    this->updateWaitingList(floor);

                    // Add the passenger to the waiting list!
                    unsigned int horPos = this->passengersHorizontalTracker++;
                    this->currentAsciiGraphics[my_global_context::ASCII_PASSENGERS_VERTICAL_POS][horPos] = id;
                    this->currentAsciiGraphics[my_global_context::ASCII_FROM_VERTICAL_POS][horPos] = '0' + floor;
                    this->currentAsciiGraphics[my_global_context::ASCII_TO_VERTICAL_POS][horPos] = '0' + to;

                    this->draw();
                    break;
                }
                case util::PassengerCycleState::BUTTON_PRESS:
                    // Nothing for now
                    break;
                case util::PassengerCycleState::RIDING:
                {
                    // Remove from the waiting list
                    auto floor = any_cast<unsigned int>(toProcess.data["floor"]);
                    char id = any_cast<char>(toProcess.data["id"]);
//
//                    unsigned int x = this->floorVerticalTrackers[floor];
//                    unsigned int maxY = this->floorHorizontalTrackers[floor];
//
//                    for(unsigned int y = my_global_context::ASCII_FLOOR_WAITING_HORIZONTAL_START; y <= maxY; y++) {
//
//                    }

                    this->updateWaitingList(floor);
                    this->updateLiftList(id, Lift::UpdateKey::newPerson);
                    this->draw();
                    break;
                }
                case util::PassengerCycleState::EXITED:
                {
                    auto floor = any_cast<unsigned int>(toProcess.data["floor"]);
                    char id = any_cast<char>(toProcess.data["id"]);
                    this->updateExitList(id, floor);
                    this->updateLiftList(id, Lift::UpdateKey::delPerson);
                    this->draw();
                    break;
                }
                default:
                    throw invalid_argument("Graphics action not implemented for this Passenger Cycle State!");
            }
            break;
        }
        case util::SimulationPurpose::LIFT_CONTROLLER:
            {
                cout << "Lift Controller graphics message!" << endl;
                cout << any_cast<string>(toProcess.data["toPrint"]) << endl;
                auto tmpState = any_cast<util::RequestState>(toProcess.data["requestState"]);
                switch(tmpState) {
                    case util::RequestState::IN_PROGRESS:
                        {
                            auto floor = any_cast<unsigned int>(toProcess.data["floor"]);
                            unsigned int x = this->floorVerticalTrackers[floor];
                            // Put the star at the request being served
                            this->currentAsciiGraphics[x][my_global_context::ASCII_INDICATOR_LIFT1_STATIONS_HORIZONTAL] = '*';
                            this->draw();
                            break;
                        }
                    case util::RequestState::IN_PROGRESS_VECTOR:
                    {
                        const auto& floors = any_cast<std::vector<unsigned int>&>(toProcess.data["floors"]);
                        for(const unsigned int floor : floors) {
                            unsigned int x = this->floorVerticalTrackers[floor];
                            // Put the star at the request being served
                            this->currentAsciiGraphics[x][my_global_context::ASCII_INDICATOR_LIFT1_STATIONS_HORIZONTAL] = '*';
                        }
                        this->draw();
                        break;
                    }
                    case util::RequestState::DONE:
                        {
                            auto floor = any_cast<unsigned int>(toProcess.data["floor"]);
                            unsigned int x = this->floorVerticalTrackers[floor];
                            // Remove the star at the position floor!
                            this->currentAsciiGraphics[x][my_global_context::ASCII_INDICATOR_LIFT1_STATIONS_HORIZONTAL] = ' ';
                            this->draw();
                            break;
                        }
                    default:
                        throw invalid_argument("Graphics action not implemented for this Passenger Cycle State!");
                }
            }
            break;
        case util::SimulationPurpose::STDOUT_PRINT:
            cout << "Stdout graphics message!" << endl;
            cout << any_cast<string>(toProcess.data["toPrint"]) << endl;
            break;
        default:
            throw invalid_argument("Graphics action not implemented for this Simulation Entity!");
    }
}

void Graphics::send2Gfx(const util::GraphicsMessage& toSend) {
    {
        lock_guard<mutex> lg{my_global_context::graphicsMutex};
        my_global_context::graphicsQueue.emplace(toSend);
        my_global_context::graphicsMessageReady = true;
    }
    my_global_context::graphicsCv.notify_one();
}

void Graphics::updateAscii4Lift(const unsigned int liftID, const Lift::LiftState& state, const Lift::Command cmd,
                                const unsigned int destinationFloor, Graphics::LiftGfxPurpose liftGfxPurpose) {
    // TODO: Insert lift1 and lift2 separate updating
    // God help me...
    // Let's first draw the lift at the new position - if necessary!
    if(liftGfxPurpose == LiftGfxPurpose::OTHER_LIFT_STATE_CHANGE) {
        if(state.currPosition != this->lift1PreviousState.currPosition ||
           state.currVerticalPosition != this->lift1PreviousState.currVerticalPosition) {
            /*
             * Lift Moving or Stopping --> Draw new positions, erase old!
             */
            // First Erase Old
            for(int i = my_global_context::ASCII_LIFT1_OPENING_BRACE_HORIZONTAL; i <= my_global_context::ASCII_LIFT1_CLOSING_BRACE_HORIZONTAL; i++) {
                this->currentAsciiGraphics[this->lift1VerticalTracker][i] = ' ';
            }

            if(state.currLiftWay == util::Way::UP) {
                this->lift1VerticalTracker--;
            } else if(state.currLiftWay == util::Way::DOWN) {
                this->lift1VerticalTracker++;
            }

            // Then Draw New
            this->currentAsciiGraphics[this->lift1VerticalTracker][my_global_context::ASCII_LIFT1_OPENING_BRACE_HORIZONTAL] = '[';
            int i = my_global_context::ASCII_LIFT1_OPENING_BRACE_HORIZONTAL + 1;
            for(char personID : state.peopleInLift) {
                this->currentAsciiGraphics[this->lift1VerticalTracker][i++] = personID;
            }
            this->currentAsciiGraphics[this->lift1VerticalTracker][my_global_context::ASCII_LIFT1_CLOSING_BRACE_HORIZONTAL] = ']';
        } else {
            char asciiWay = util::WayAsciiCode(state.currLiftWay);
            std::array<char, 2> asciiDoor = Lift::DoorStateAsciiCode(state.currDoorState);

            this->currentAsciiGraphics[my_global_context::ASCII_LIFT_INDICATOR_WAY_VERTICAL][my_global_context::ASCII_LIFT1_INDICATOR_WAY_HORIZONTAL] = asciiWay;
            this->currentAsciiGraphics[my_global_context::ASCII_LIFT_INDICATOR_DOOR_VERTICAL][my_global_context::ASCII_LIFT1_INDICATOR_DOOR_HORIZONTAL] = asciiDoor[0];
            this->currentAsciiGraphics[my_global_context::ASCII_LIFT_INDICATOR_DOOR_VERTICAL][my_global_context::ASCII_LIFT1_INDICATOR_DOOR_HORIZONTAL+1] = asciiDoor[1];
        }
    } else if(liftGfxPurpose == LiftGfxPurpose::PASSENGERS_RIDING_CHANGE) {
        // Redraw
        int i = my_global_context::ASCII_LIFT1_OPENING_BRACE_HORIZONTAL + 1;
        for(char personID : state.peopleInLift) {
            this->currentAsciiGraphics[this->lift1VerticalTracker][i++] = personID;
        }
    } else {
        throw invalid_argument("Action not implemented for this LiftGfxPurpose!");
    }
}

void
Graphics::printCurrLiftStateAndOrder(const unsigned int liftID, const Lift::LiftState &state, const Lift::Command cmd,
                                     const unsigned int destinationFloor) {
    string toPrint = fmt::format(
            "Current Lift ID: {}\n"
            "Current Lift Position: {}\n"
            "Current Lift Door State: {}\n"
            "Current Lift Momentum: {}\n"
            "Current Lift Vertical Position: {}\n"
            "Current Lift Way: {}\n",
            liftID, state.currPosition, Lift::doorState2String(state.currDoorState),
            Lift::momentum2String(state.currLiftMomentum), Lift::verticalPosition2String(state.currVerticalPosition),
            util::Way2Str(state.currLiftWay)
    );
    cout << toPrint;
    cout << "People currently in Lift: ";
    for(char person : state.peopleInLift) {
        cout << person << ", ";
    }
    cout << endl;

    cout << "Current Lift Command: " << Lift::command2String(cmd) << endl;
    cout << "Current Lift Destination Floor: " << destinationFloor << endl;
}

void Graphics::updateWaitingList(const unsigned int floor) {
    unsigned int x = this->floorVerticalTrackers[floor];
    unsigned int y = my_global_context::ASCII_FLOOR_WAITING_HORIZONTAL_START;

    std::list<char> peopleIDs;
    {
        shared_lock<shared_mutex> sl{this->building[floor].peopleWaitingMutex};
        peopleIDs = this->building[floor].peopleWaitingAtThisFloor;
    }

    for(const char personID :  peopleIDs) {
        this->currentAsciiGraphics[x][y++] = personID;
    }

    while(y < my_global_context::ASCII_FLOOR_WAITING_HORIZONTAL_END) {
        this->currentAsciiGraphics[x][y++] = ' ';
    }
}

void Graphics::updateLiftList(const char personID, const Lift::UpdateKey updateKey) {
    unsigned int x = this->lift1VerticalTracker;
    if(updateKey == Lift::UpdateKey::newPerson) {
        this->lift1PreviousState.peopleInLift.emplace(personID);
    } else if (updateKey == Lift::UpdateKey::delPerson) {
        this->lift1PreviousState.peopleInLift.erase(personID);
    } else {
        throw invalid_argument("Unsupported UpdateKey!");
    }

    unsigned int y = my_global_context::ASCII_LIFT1_HORIZONTAL_START;
    for(const char currPersonID : this->lift1PreviousState.peopleInLift) {
        this->currentAsciiGraphics[x][y++] = currPersonID;
    }

    while(y < my_global_context::ASCII_LIFT1_CLOSING_BRACE_HORIZONTAL) {
        this->currentAsciiGraphics[x][y++] = ' ';
    }
}

void Graphics::updateExitList(const char personID, const unsigned int floor) {
    unsigned int x = this->floorVerticalTrackers[floor];
    this->currentAsciiGraphics[x][this->floorExitedHorizontalTrackers[floor]++] = personID;
}
