#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err58-cpp"
//
// Created by kikyy_99 on 14. 12. 2020..
//

#ifndef LAB3_UTIL_H
#define LAB3_UTIL_H

#include <array>
#include <random>
#include <mutex>
#include <shared_mutex>
#include <condition_variable>
#include <queue>
#include <vector>
#include <unordered_map>
#include <any>
#include <list>

namespace util {
    using button = bool;

    enum class LoadSeverity {
        LOW, MEDIUM, HIGH
    };
    enum class Way {
        NONE, UP, DOWN
    };
    enum class SimulationPurpose {
        LIFT, PASSENGER, LIFT_CONTROLLER, STDOUT_PRINT
    };
    enum class PassengerCycleState {
        WAITING, BUTTON_PRESS, RIDING, EXITED
    };

    struct Floor {
        button upPressed;
        std::mutex upMutex;

        button downPressed;
        std::mutex downMutex;

        std::list<char> peopleWaitingAtThisFloor;
        std::shared_mutex peopleWaitingMutex;

        std::vector<char> peopleExitedAtThisFloor;
        std::shared_mutex peopleExitedMutex;

        std::pair<button &, std::mutex &> operator[](const Way &index) {
            switch (index) {
                case Way::UP:
                    return {this->upPressed, this->upMutex};
                case Way::DOWN:
                    return {this->downPressed, this->downMutex};
                default:
                    throw std::invalid_argument("Action not implemented for this severity load level!");
            }
        }
    };

    struct Request {
        unsigned int srcFloor = 0;
        unsigned int dstFloor = 0;
        util::Way way = util::Way::NONE;

        Request() = default;

        Request(unsigned int srcFloor, unsigned int dstFloor, util::Way way)
                : srcFloor{srcFloor}, dstFloor{dstFloor}, way{way} {};
    };

    inline bool requestCompareBySrcFloor(const Request& req1, const Request& req2) {
        return req1.srcFloor < req2.srcFloor;
    }

    inline bool requestCompareByDstFloor(const Request& req1, const Request& req2) {
        return req1.dstFloor < req2.dstFloor;
    }

    enum class RequestState {IN_PROGRESS, IN_PROGRESS_VECTOR, DONE};

    struct GraphicsMessage {
        SimulationPurpose purpose;
        std::unordered_map<std::string, std::any> data;
    };

    inline std::string Way2Str(Way toConv) {
        switch(toConv) {
            case Way::DOWN:
                return "DOWN";
            case Way::NONE:
                return "NONE";
            case Way::UP:
                return "UP";
            default:
                throw std::invalid_argument("Action not implemented for this Way!");
        }
    }

    inline char WayAsciiCode(Way toConv) {
        switch(toConv) {
            case Way::DOWN:
                return 'D';
            case Way::NONE:
                return '0';
            case Way::UP:
                return 'U';
            default:
                throw std::invalid_argument("Action not implemented for this Way!");
        }
    }
}

namespace my_global_context
{
    // Building and Floor constants
    inline constexpr unsigned int FLOOR_MIN_WAITING_PEOPLE = 0;
    inline constexpr unsigned int FLOOR_MAX_WAITING_PEOPLE = 10;

    // Lift time constants and constants
    inline constexpr unsigned int LIFT_MIN_CAPACITY = 6;
    inline constexpr unsigned int LIFT_MAX_CAPACITY = 18;
    inline constexpr unsigned int LIFT1_CAPACITY = 8;
    inline constexpr unsigned int LIFT2_CAPACITY = 6;
    inline constexpr unsigned int LIFT_TIME_BETWEEN_FLOOR_TRAVERSAL_S = 3;
    inline constexpr unsigned int LIFT_TIME_DOOR_OPENING_CLOSING_S = 2;
    inline constexpr unsigned int LIFT_TIME_DOOR_OPEN_S = 5;
    inline constexpr unsigned int LIFT_TIME_CLOSED_WAIT_S = 1;
    inline constexpr unsigned int UCPU_POLL_SLEEP_MS = 25;

    // Creation - entity generation constants
    inline constexpr unsigned int CREATION_BUILDING_MAX_FLOOR = 3;

    inline constexpr double CREATION_LOW_SEVR_CHANCE = 0.20;
    inline constexpr double CREATION_MED_SEVR_CHANCE = 0.45;
    inline constexpr double CREATION_HIGH_SEVR_CHANCE = 0.80;

    inline constexpr unsigned int CREATION_LOW_SEVR_SLEEP_TIME_S = 10;
    inline constexpr unsigned int CREATION_MED_SEVR_SLEEP_TIME_S = 7;
    inline constexpr unsigned int CREATION_HIGH_SEVR_SLEEP_TIME_S = 4;

    inline constexpr std::array<unsigned int, 2> CREATION_LOW_SEVR_RANGE{0, CREATION_BUILDING_MAX_FLOOR};
    inline constexpr unsigned int CREATION_LOW_SEVR_RANGE_DELTA = CREATION_LOW_SEVR_RANGE[1] - CREATION_LOW_SEVR_RANGE[0];
    inline constexpr std::array<unsigned int, 2> CREATION_MED_SEVR_RANGE{CREATION_BUILDING_MAX_FLOOR / 2, CREATION_BUILDING_MAX_FLOOR};
    inline constexpr unsigned int CREATION_MED_SEVR_RANGE_DELTA = CREATION_MED_SEVR_RANGE[1] - CREATION_MED_SEVR_RANGE[0];
    inline constexpr std::array<unsigned int, 2> CREATION_HIGH_SEVR_RANGE{CREATION_BUILDING_MAX_FLOOR, 3 * CREATION_BUILDING_MAX_FLOOR};
    inline constexpr unsigned int CREATION_HIGH_SEVR_RANGE_DELTA = CREATION_HIGH_SEVR_RANGE[1] - CREATION_HIGH_SEVR_RANGE[0];

    // Passenger constants
    inline constexpr double WAY_UP_CHANCE = 0.5;
    inline constexpr unsigned int HUMAN_REACTION_TIME_MS = 500;
    inline constexpr unsigned int PERSON_ARRIVAL_TIME_MS = 1000;

    // Random number generator
    inline std::random_device rd{};
    inline std::mt19937_64 engine{rd()};

    inline std::uniform_real_distribution<double> randomGenerator{0, 1};
    inline std::uniform_int_distribution<unsigned int> floorsRandom{0, CREATION_BUILDING_MAX_FLOOR};

    // Graphics
    inline std::mutex graphicsMutex{};
    inline std::queue<util::GraphicsMessage> graphicsQueue{};
    inline std::condition_variable graphicsCv{};
    inline bool graphicsMessageReady = false;

    // Lift Controller
    inline std::mutex liftControllerRequestsMutex{};
    inline std::queue<util::Request> liftControllerRequestsQueue{};
    inline std::condition_variable liftControllerRequestsCv{};
    inline bool liftControllerRequestReady = false;

    // Lift Controller - Lift Command exchange
    inline std::shared_mutex liftControllerLiftOrderSharedMutex{};
    inline std::shared_mutex liftStateSharedMutex{};

    inline const std::array<std::string, 15> GRAPHICS_ASCII_EMPTY{
            "                Lift1      Lift2                                                                                                                                                                                                                                                                                                                 ",
            "Way/Door:       N  -C      N  -C                                                                                                                                                                                                                                                                                                                 ",
            "========================================== Exited ===============================================================================================================================================================================================================================================================================================",
            "3:          |           |         |                                                                                                                                                                                                                                                                                                              ",
            "  ==========|           |         |                                                                                                                                                                                                                                                                                                              ",
            "2:          |           |         |                                                                                                                                                                                                                                                                                                              ",
            "  ==========|           |         |                                                                                                                                                                                                                                                                                                              ",
            "1:          |           |         |                                                                                                                                                                                                                                                                                                              ",
            "  ==========|           |         |                                                                                                                                                                                                                                                                                                              ",
            "0:          | [        ]| [      ]|                                                                                                                                                                                                                                                                                                              ",
            "================================================================================================================================================================================================================================================================================================================================================",
            "Passengers:                                                                                                                                                                                                                                                                                                                                     ",
            "      From:                                                                                                                                                                                                                                                                                                                                     ",
            "        To:                                                                                                                                                                                                                                                                                                                                     ",
            "                                                                                                                                                                                                                                                                                                                                                "
    };

    inline constexpr unsigned int ASCII_LIFT1_HORIZONTAL_START = 15;
    inline constexpr unsigned int ASCII_LIFT2_HORIZONTAL_START = 27;
    inline constexpr unsigned int ASCII_LIFT_VERTICAL_START = 9;

    inline constexpr unsigned int ASCII_PASSENGERS_HORIZONTAL_START = 12;
    inline constexpr unsigned int ASCII_EXITED_HORIZONTAL_START = 35;
    inline constexpr unsigned int ASCII_FLOOR_WAITING_HORIZONTAL_START = 2;
    inline constexpr unsigned int ASCII_FLOOR_WAITING_HORIZONTAL_END = 12;      // Exclusive

    inline constexpr unsigned int ASCII_PASSENGERS_VERTICAL_POS = 11;
    inline constexpr unsigned int ASCII_FROM_VERTICAL_POS = ASCII_PASSENGERS_VERTICAL_POS + 1;
    inline constexpr unsigned int ASCII_TO_VERTICAL_POS = ASCII_FROM_VERTICAL_POS + 1;

    inline constexpr unsigned int ASCII_LIFT1_INDICATOR_WAY_HORIZONTAL = 16;
    inline constexpr unsigned int ASCII_LIFT1_INDICATOR_DOOR_HORIZONTAL = 19;
    inline constexpr unsigned int ASCII_LIFT1_INDICATOR_DOOR_HORIZONTAL_PLUS_ONE = ASCII_LIFT1_INDICATOR_DOOR_HORIZONTAL + 1;
    inline constexpr unsigned int ASCII_LIFT2_INDICATOR_WAY_HORIZONTAL = 27;
    inline constexpr unsigned int ASCII_LIFT2_INDICATOR_DOOR_HORIZONTAL = 30;
    inline constexpr unsigned int ASCII_LIFT2_INDICATOR_DOOR_HORIZONTAL_PLUS_ONE = ASCII_LIFT2_INDICATOR_DOOR_HORIZONTAL + 1;
    inline constexpr unsigned int ASCII_LIFT_INDICATOR_WAY_VERTICAL = 1;
    inline constexpr unsigned int ASCII_LIFT_INDICATOR_DOOR_VERTICAL = 1;

    inline constexpr unsigned int ASCII_INDICATOR_LIFT1_STATIONS_HORIZONTAL = 13;
    inline constexpr unsigned int ASCII_INDICATOR_LIFT2_STATIONS_HORIZONTAL = 25;

    inline constexpr unsigned int ASCII_LIFT1_OPENING_BRACE_HORIZONTAL = 14;
    inline constexpr unsigned int ASCII_LIFT1_CLOSING_BRACE_HORIZONTAL = 23;

    inline constexpr unsigned int ASCII_LIFT2_OPENING_BRACE_HORIZONTAL = 26;
    inline constexpr unsigned int ASCII_LIFT2_CLOSING_BRACE_HORIZONTAL = 33;
}

#endif //LAB3_UTIL_H
#pragma clang diagnostic pop