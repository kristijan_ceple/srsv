//
// Created by kikyy_99 on 14. 12. 2020..
//

#ifndef LAB3_PASSENGER_H
#define LAB3_PASSENGER_H


#include <cstdint>
#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <queue>
#include <condition_variable>
#include "util.h"
#include "Lift.h"

class Passenger {
private:
    char id;
    unsigned int sourceFloor;
    unsigned int destFloor;
    util::Way myWay;

    std::vector<util::Floor>& building;      // Used for pressing buttons!
    Lift& lab3Lift;

    std::unique_ptr<std::thread> myThread;
    bool threadLaunched = false;
    bool threadDone = false;

    util::PassengerCycleState currState = util::PassengerCycleState::WAITING;
    bool pressedAgain = false;

    void pressButton();
    void waitForLift();
    void riding();
public:
    Passenger(
            char id,
            unsigned int sourceFloor, unsigned int destFloor, util::Way myWay,
            std::vector<util::Floor>& building,
            Lift& lab3Lift
            );
    void run();
    void operator()();

    bool hasPassengerThreadStarted() {return this->threadLaunched;};
    bool hasPassengerExited() {return this->threadDone;};
    void joinPassengerThread() {this->myThread->join();};

    ~Passenger();
};


#endif //LAB3_PASSENGER_H
