//
// Created by kikyy_99 on 14. 12. 2020..
//

#ifndef LAB3_LIFT_H
#define LAB3_LIFT_H

#include <vector>
#include <shared_mutex>
#include <thread>
#include <list>
#include <unordered_set>
#include "util.h"

class Lift {
    friend class LiftController;
    friend class Graphics;
private:
    std::unique_ptr<std::thread> myThread;
    const unsigned int maxCapacity;
    const unsigned int liftID;
    const std::vector<util::Floor>& building;        // Used for pressing buttons!

    /*
     * The Lift Controller issued commands that Lift listens to, and reacts according to them
     */
    enum class Command{Stop, Move, OpenDoor, CloseDoor, None};
    Command currCommand = Command::None;
    //Command prevCommand = Command::None;
    unsigned int destinationFloor = 0;

    /*
     * Lift State Management
     */
    enum class DoorState{Open, Closed, Opening, Closing};
    static std::array<char,2> DoorStateAsciiCode(DoorState);
    /*
     * Stationary = standing with doors open or closed, no request to get moving just yet
     * StationaryPrepping = Stationary with doors closed, but need to wait for certain amount of time to elapse
     *  before we can get moving
     * StationaryPrepped = standing with doors closed and the required time has elapsed - can get moving at once
     * Moving = self-explanatory
     *
     * PostMoveStopping = Lift isn't moving, and the doors are opening. After the doors have been opened, they are kept
     *  open for some time.
     * ForcedStopping = an order for the lift to stop moving has been issued, and then the lift enters this state. In it
     *  halts at the nearest(next) floor. In this state itself the lift is traversing the last floor while coming to a
     *  stop.
     *
     *  Passengers can enter the lift in either Stationary, or PostMoveStopping, state.
     */
    enum class Momentum{Stationary, StationaryPrepping, StationaryPrepped, Moving, PostMoveStopping, ForcedStopping};
    // InterSpace = Space between currPosition and nextPosition
    enum class VerticalPosition{Floor, InterSpace};

    struct LiftState{
        unsigned int currPosition = 0;
        std::unordered_set<char> peopleInLift{};                   // People IDs
        DoorState currDoorState = DoorState::Closed;
        Momentum currLiftMomentum = Momentum::Stationary;
        VerticalPosition currVerticalPosition = VerticalPosition::Floor;
        util::Way currLiftWay = util::Way::NONE;
    };
    LiftState currLiftState{};

    /**
     * Managed by the Lift Controller - the Way to the final Request.
     * Used so that passengers do not enter Lifts that do not go in their desired way, but have an intermediary
     * station on the waiting passenger's floor.
     */
    util::Way genLiftWay = util::Way::NONE;
    std::shared_mutex genLiftWaySharedMutex{};

    // Time measurement
    std::chrono::time_point<std::chrono::high_resolution_clock> endPoint{};

    // Helper functions
    static std::string command2String(Command);
    static std::string doorState2String(DoorState);
    static std::string momentum2String(Momentum);
    static std::string verticalPosition2String(VerticalPosition);

    [[nodiscard]] LiftState getCurrLiftState() const;
    bool checkControllerCommands();
    void executeCurrentState();
    void sendState2Gfx(std::string toPrint = "Lift State Update sent 2 Gfx!");
public:
    Lift(
            unsigned int currentPosition,
            unsigned int maxCapacity,
            unsigned int liftID,
            std::vector<util::Floor>& building
            );
    void run();
    [[noreturn]] void operator()();

    [[nodiscard]] bool isLiftDoorOpen() const;
    [[nodiscard]] bool isLiftStatic() const;
    bool isLiftReadyAtFloor(unsigned int floor) const;
    bool hasLiftGotNoCommandsIssued() const;
    bool isLiftReadyForCommand() const;
    [[nodiscard]] unsigned int getCurrentPosition() const;
    util::Way getCurrentGenLiftWay();
    bool enterLift(char personId);
    bool leaveLift(char personId);

    enum class UpdateKey{newDoorState, newLiftMomentum, newVertPos, newPosition, newPeople, newPerson, newLiftWay,
            delPeople, delPerson};
    void updateLiftState(std::unordered_map<UpdateKey, std::any> kwargs);
};


#endif //LAB3_LIFT_H
