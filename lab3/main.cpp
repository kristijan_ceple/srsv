#include <iostream>
#include <vector>
#include <thread>
#include "util.h"
#include "LiftController.h"
#include "Simulation.h"

using namespace std;
using namespace my_global_context;

int main() {
    cout << "Lab3 -- Lift Simulation!" << endl;

    Simulation sim{};
    sim.run();

    return 0;
}