//
// Created by kikyy_99 on 14. 12. 2020..
//

#ifndef LAB3_LIFTCONTROLLER_H
#define LAB3_LIFTCONTROLLER_H


#include "util.h"
#include "Lift.h"
#include <utility>
#include <vector>
#include <thread>
#include <memory>
#include <set>
#include <functional>

class LiftController {
private:
    enum class LiftControllerStep {Step1, Step2, Step3};
    LiftControllerStep lab3LiftStep = LiftControllerStep::Step1;

    std::unique_ptr<std::thread> myThread;

    std::vector<util::Floor>& building;
    std::shared_ptr<Lift> lab3Lift;
    std::vector<unsigned int> lab3LiftStations;
    util::Request lab3NearestRequest;
    unsigned int nextStop;

    // TODO: Lift4 things here!

    std::multiset<util::Request, std::function<bool(const util::Request&,const util::Request&)>> srcFloorSortedAllRequests{util::requestCompareBySrcFloor};
    //std::set<util::Request, std::function<bool(const util::Request&,const util::Request&)>> dstFloorSortedAllRequests{util::requestCompareByDstFloor};
    //std::deque<util::Request> lab3LiftAssignedRequests;

//    struct LiftOrder {
//        Lift::Command cmd;
//        unsigned int floor;
//    };
    void sendOrder2Lift(Lift::Command cmd, unsigned int floor) const;
    void sendOrder2Lift(Lift::Command cmd) const;
    void sendNewRequest2Gfx(const util::Request &toProcess) const;
    void sendClearRequestStar2Gfx(const util::Request &toProcess) const;
    void sendNewRequest2Gfx(const unsigned int starFloor) const;
    void sendClearRequestStar2Gfx(const unsigned int starFloor) const;
    void sendNewRequest2Gfx(const std::vector<unsigned int>&) const;

    bool updateRequests();
    void processRequest(const util::Request &toProcess);

    void beelineServe(const util::Request &request);
    void smartServe();
    void step2a();
    void step2b();
public:
    explicit LiftController(std::vector<util::Floor>& building, Lift* toSetLab3Lift = nullptr)
    : building{building}
    {lab3Lift.reset(toSetLab3Lift);}
    void setLab3Lift(Lift& toSet);

    void run();
    [[noreturn]] void operator()();
};


#endif //LAB3_LIFTCONTROLLER_H
