//
// Created by kikyy_99 on 14. 12. 2020..
//

#include "LiftController.h"
#include "Graphics.h"
#include <thread>
#include <fmt/format.h>
#include <iostream>
#include <cstdlib>
#include <algorithm>

using namespace std;

void LiftController::setLab3Lift(Lift &toSet) {
    this->lab3Lift.reset(&toSet);
}

void LiftController::run() {
    this->myThread = make_unique<thread>(
            ref(*this)
            );
}

[[noreturn]] void LiftController::operator()() {
    Graphics::send2Gfx({
        .purpose = util::SimulationPurpose::STDOUT_PRINT,
        .data = {
                {"toPrint", std::string{"Lift Controller online!"}}
        }
    });

    while(true) {
        // uCPU frequency
        this_thread::sleep_for(chrono::milliseconds{my_global_context::UCPU_POLL_SLEEP_MS});

        this->updateRequests();

        // Main algorithm part
        this->smartServe();
    }
}

void LiftController::processRequest(const util::Request &toProcess) {
    /*
     * This is where the control logic of the lift goes.
     */
    // First send notification to graphics to display the request
    this->sendNewRequest2Gfx(toProcess);
    this->beelineServe(toProcess);
}

void LiftController::sendNewRequest2Gfx(const util::Request &toProcess) const {
    string requestMessage = fmt::format(
            "Request for lift at floor {}, destination floor: {}, Way: {}",
            toProcess.srcFloor, toProcess.dstFloor, util::Way2Str(toProcess.way)
    );
    util::GraphicsMessage requestGfxMsg = {
            util::SimulationPurpose::LIFT_CONTROLLER,
            {
                    {"toPrint", requestMessage},
                    {"requestState", util::RequestState::IN_PROGRESS},
                    {"floor", toProcess.srcFloor},
            }
    };

    Graphics::send2Gfx(requestGfxMsg);
}

void LiftController::sendClearRequestStar2Gfx(const util::Request &toProcess) const {
    string requestMessage = fmt::format(
            "Clearing the picked up request -> srcFloor: {}, dstFloor: {}, Way:{}",
            toProcess.srcFloor, toProcess.dstFloor, util::Way2Str(toProcess.way)
    );
    util::GraphicsMessage requestGfxMsg = {
            util::SimulationPurpose::LIFT_CONTROLLER,
            {
                    {"toPrint", requestMessage},
                    {"requestState", util::RequestState::DONE},
                    {"floor", toProcess.srcFloor},
            }
    };

    Graphics::send2Gfx(requestGfxMsg);
}

void LiftController::sendNewRequest2Gfx(const unsigned int starFloor) const {
    string requestMessage = "Lift Controller Star added!";
    util::GraphicsMessage requestGfxMsg = {
            util::SimulationPurpose::LIFT_CONTROLLER,
            {
                    {"toPrint", requestMessage},
                    {"requestState", util::RequestState::IN_PROGRESS},
                    {"floor", starFloor},
            }
    };

    Graphics::send2Gfx(requestGfxMsg);
}

void LiftController::sendNewRequest2Gfx(const std::vector<unsigned int>& starFloors) const {
    string requestMessage = "Lift Controller Star added!";
    util::GraphicsMessage requestGfxMsg = {
            util::SimulationPurpose::LIFT_CONTROLLER,
            {
                    {"toPrint", requestMessage},
                    {"requestState", util::RequestState::IN_PROGRESS_VECTOR},
                    {"floors", starFloors},
            }
    };

    Graphics::send2Gfx(requestGfxMsg);
}


void LiftController::sendClearRequestStar2Gfx(const unsigned int starFloor) const {
    string requestMessage = "Lift Controller Star cleared!";
    util::GraphicsMessage requestGfxMsg = {
            util::SimulationPurpose::LIFT_CONTROLLER,
            {
                    {"toPrint", requestMessage},
                    {"requestState", util::RequestState::DONE},
                    {"floor", starFloor},
            }
    };

    Graphics::send2Gfx(requestGfxMsg);
}

/**
 * A beeline serve to request
 * @param request Request being served
 */
void LiftController::beelineServe(const util::Request &request) {
    // Move the lift to this request, and wait until it has been served
    this->sendOrder2Lift(Lift::Command::Move, request.srcFloor);

    // Now I need to wait until the lift has reached the destination floor, and opened its doors
    while(true) {
        this_thread::sleep_for(chrono::milliseconds(my_global_context::UCPU_POLL_SLEEP_MS));
        Lift::LiftState currLiftState = this->lab3Lift->getCurrLiftState();

        if(
                currLiftState.currLiftMomentum == Lift::Momentum::Stationary
                && currLiftState.currDoorState == Lift::DoorState::Open
                && currLiftState.currVerticalPosition == Lift::VerticalPosition::Floor
                && currLiftState.currLiftWay == util::Way::NONE
                && currLiftState.currPosition == request.srcFloor
                ) {
            break;
        }
    }
    this->sendClearRequestStar2Gfx(request);        // Request has been picked up
    this->sendOrder2Lift(Lift::Command::Move, request.dstFloor);
    // Wait until the Request has been served!!
    while(true) {
        this_thread::sleep_for(chrono::milliseconds(my_global_context::UCPU_POLL_SLEEP_MS));
        Lift::LiftState currLiftState = this->lab3Lift->getCurrLiftState();

        if(
                currLiftState.currLiftMomentum == Lift::Momentum::Stationary
                && currLiftState.currDoorState == Lift::DoorState::Open
                && currLiftState.currVerticalPosition == Lift::VerticalPosition::Floor
                && currLiftState.currLiftWay == util::Way::NONE
                && currLiftState.currPosition == request.dstFloor
                ) {
            break;
        }
    }
}

void  LiftController::sendOrder2Lift(Lift::Command cmd, unsigned int floor) const {
    {
        lock_guard<shared_mutex> lg(my_global_context::liftControllerLiftOrderSharedMutex);
        this->lab3Lift->destinationFloor = floor;
        this->lab3Lift->currCommand = cmd;
    }
}

void LiftController::sendOrder2Lift(Lift::Command cmd) const {
    {
        lock_guard<shared_mutex> lg(my_global_context::liftControllerLiftOrderSharedMutex);
        this->lab3Lift->currCommand = cmd;
    }
}

/**
 * Checks if any new requests are present.
 * @return true if there were some new requests, false otherwise
 */
bool LiftController::updateRequests() {
    util::Request tmp;
    {
        lock_guard<mutex> ul(my_global_context::liftControllerRequestsMutex);
        if(!my_global_context::liftControllerRequestsQueue.empty()) {
            tmp = my_global_context::liftControllerRequestsQueue.front();
            my_global_context::liftControllerRequestsQueue.pop();
        } else {
            return false;
        }
    }

    if(tmp.srcFloor == tmp.dstFloor) {
        // Discard this Request
        return false;
    }

    this->srcFloorSortedAllRequests.emplace(tmp);
    //this->dstFloorSortedAllRequests.emplace(tmp);
    return true;
}

void LiftController::smartServe() {
    // Step 1
    if(this->lab3LiftStep == LiftControllerStep::Step1) {
        if(!this->srcFloorSortedAllRequests.empty()) {
            // No appointed Requests - find the nearest one!
            // Note that the nearest one can be at this floor as well!(Step 4.b) )
            // Go and pick up the closest request
            const unsigned int liftPos = this->lab3Lift->getCurrentPosition();
            unsigned int minDist = my_global_context::CREATION_BUILDING_MAX_FLOOR + 1;
            const util::Request* minRequest = nullptr;
            std::set<util::Request, util::Request>::iterator minIt;

            for(auto it = this->srcFloorSortedAllRequests.begin(); it != this->srcFloorSortedAllRequests.end(); it++) {
                unsigned int dist = std::abs((int)it->srcFloor - (int)liftPos);
                if(dist < minDist) {
                    minDist = dist;
                    minRequest = &*it;
                    minIt = it;
                }
            }

            this->lab3NearestRequest = *minIt;
//            this->srcFloorSortedAllRequests.erase(minIt);

            {
                lock_guard<shared_mutex> lg{this->lab3Lift->genLiftWaySharedMutex};
                this->lab3Lift->genLiftWay = this->lab3NearestRequest.way;
            }

            // Move the lift to this location!
            this->sendOrder2Lift(Lift::Command::Move, this->lab3NearestRequest.srcFloor);
            this->sendNewRequest2Gfx(this->lab3NearestRequest);

            // Proceed to Step 2
            this->lab3LiftStep = LiftControllerStep::Step2;
        } else {
            auto liftState = this->lab3Lift->getCurrLiftState();
            if(
                    (
                            liftState.currDoorState != Lift::DoorState::Closing
                            && liftState.currDoorState != Lift::DoorState::Closed
                    )
                    && liftState.currLiftMomentum == Lift::Momentum::Stationary
                    ) {
                this->sendOrder2Lift(Lift::Command::CloseDoor);         // Lift just stands there with the doors closed!
            }

            return;
        }
    }

    /*
     * Step 2 - wait until lift reaches the src floor
     *      While the lift is moving towards the closest request - more requests are coming, and an even closer one
     *      can appear
     */
    if(this->lab3LiftStep == LiftControllerStep::Step2) {
        if(this->lab3Lift->isLiftReadyAtFloor(this->lab3NearestRequest.srcFloor)) {
            this->step2b();          // Process all the requests currently present in the system!
            this->sendClearRequestStar2Gfx(this->lab3NearestRequest.srcFloor);
            this->lab3LiftStep = LiftControllerStep::Step3;

            if(this->lab3LiftStations.empty()) {
                cout << "Multiple button presses!" << endl;
                this->lab3LiftStep = LiftControllerStep::Step1;     // Need to find another request
                return;
            }

            this->nextStop = this->lab3LiftStations.back();
            this->lab3LiftStations.pop_back();
            this->sendOrder2Lift(Lift::Command::Move, this->nextStop);
            // Last-Stop edge case - only dstFloor, not srcFloor, therefore no one should be entering until their request is served!
            if(this->lab3LiftStations.empty()) {
                {
                    lock_guard<shared_mutex> lg{this->lab3Lift->genLiftWaySharedMutex};
                    this->lab3Lift->genLiftWay = util::Way::NONE;
                }
            }
        } else {
            // Scan other requests that could maybe be closer?
            this->step2a();
            return;
        }
    }

    // Step 3
    // Move the lift to stations one by one!
    if(this->lab3Lift->isLiftReadyAtFloor(this->nextStop)) {
        this->sendClearRequestStar2Gfx(this->nextStop);

        if(this->lab3LiftStations.empty()) {
            this->lab3LiftStep = LiftControllerStep::Step1;
            return;
        } else {
            this->nextStop = this->lab3LiftStations.back();
            this->lab3LiftStations.pop_back();
            this->sendOrder2Lift(Lift::Command::Move, this->nextStop);

            // Last-Stop edge case - only dstFloor, not srcFloor, therefore no one should be entering until their request is served!
            if(this->lab3LiftStations.empty()) {
                {
                    lock_guard<shared_mutex> lg{this->lab3Lift->genLiftWaySharedMutex};
                    this->lab3Lift->genLiftWay = util::Way::NONE;
                }
            }
        }
    } else {
        return;
    }
}

void LiftController::step2a() {
    //unsigned int liftPos = this->lab3Lift->getCurrentPosition();
    const Lift::LiftState currLiftState = this->lab3Lift->getCurrLiftState();
    util::Request tmp{
            .srcFloor = currLiftState.currPosition,
            .dstFloor = 0,
            .way = util::Way::NONE
    };
    const util::Request* minReq = nullptr;

    if(currLiftState.currLiftWay == util::Way::UP) {
        auto it = this->srcFloorSortedAllRequests.upper_bound(tmp);
        while(it != this->srcFloorSortedAllRequests.end()) {
            if(it->srcFloor < this->lab3NearestRequest.srcFloor) {
                if(it->way == util::Way::UP) {
                    minReq = &*it;
                    break;
                }
            } else {
                break;
            }

            it++;
        }
    } else if(currLiftState.currLiftWay == util::Way::DOWN) {
        auto it = this->srcFloorSortedAllRequests.lower_bound(tmp);
        it--;
        while(it != this->srcFloorSortedAllRequests.rend().base()) {
            if(it->srcFloor > this->lab3NearestRequest.srcFloor) {
                if(it->way == util::Way::DOWN) {
                    minReq = &*it;
                    break;
                }
            } else {
                break;
            }

            it++;
        }
    }

    if(minReq != nullptr) {
        // A closer Request was found!
        this->lab3NearestRequest = *minReq;

        // Move the lift to this location!
        this->sendOrder2Lift(Lift::Command::Move, this->lab3NearestRequest.srcFloor);
        this->sendNewRequest2Gfx(this->lab3NearestRequest);
    }
}

void LiftController::step2b() {
    //this->lab3LiftStations.emplace_back(this->lab3NearestRequest.dstFloor);

    if(this->lab3NearestRequest.way == util::Way::UP) {
        auto it = this->srcFloorSortedAllRequests.lower_bound(this->lab3NearestRequest);
        while(it != this->srcFloorSortedAllRequests.end()) {
            if(it->way == util::Way::UP) {
                this->lab3LiftStations.emplace_back(it->srcFloor);
                this->lab3LiftStations.emplace_back(it->dstFloor);
                it = this->srcFloorSortedAllRequests.erase(it);
            } else {
                it++;
            }
        }

        // Sort Ascending
        auto uq_it = unique(this->lab3LiftStations.begin(), this->lab3LiftStations.end());
        this->lab3LiftStations.resize(std::distance(this->lab3LiftStations.begin(), uq_it));
        sort(this->lab3LiftStations.begin(), this->lab3LiftStations.end());
        reverse(this->lab3LiftStations.begin(), this->lab3LiftStations.end());
    } else if(this->lab3NearestRequest.way == util::Way::DOWN) {
        auto it = this->srcFloorSortedAllRequests.upper_bound(this->lab3NearestRequest);
        it--;
//        if(it == this->srcFloorSortedAllRequests.rend().base()) {
//            throw logic_error("Weird error, not sure what!");
//        }

        while(it != this->srcFloorSortedAllRequests.begin()) {
            if(it->way == util::Way::DOWN) {
                this->lab3LiftStations.emplace_back(it->srcFloor);
                this->lab3LiftStations.emplace_back(it->dstFloor);
                it = this->srcFloorSortedAllRequests.erase(it);
            } else {
                it--;
            }
        }

        // Begin() iterator has to be done too!
        if(it->way == util::Way::DOWN) {
            this->lab3LiftStations.emplace_back(it->srcFloor);
            this->lab3LiftStations.emplace_back(it->dstFloor);
            it = this->srcFloorSortedAllRequests.erase(it);
        }

        // Sort Descending
        auto uq_it = unique(this->lab3LiftStations.begin(), this->lab3LiftStations.end());
        this->lab3LiftStations.resize(std::distance(this->lab3LiftStations.begin(), uq_it));
        sort(this->lab3LiftStations.begin(), this->lab3LiftStations.end());
    } else {
        throw logic_error("Way is NONE during LC Step 2?!?");
    }

    // If src floor is present - remove it!
    auto srcPos = find(this->lab3LiftStations.begin(), this->lab3LiftStations.end(), this->lab3NearestRequest.srcFloor);
    if(srcPos != this->lab3LiftStations.end()) {
        this->lab3LiftStations.erase(srcPos);
    }

    if(this->lab3LiftStations.size() > 4) {
        cout << "Oops" << endl;
        this->step2b();
    }

    // Star drawing
    this->sendNewRequest2Gfx(this->lab3LiftStations);
}
