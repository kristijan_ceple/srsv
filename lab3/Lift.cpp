//
// Created by kikyy_99 on 14. 12. 2020..
//

#include "Lift.h"
#include "Graphics.h"
#include <thread>
#include <iostream>
#include <shared_mutex>
using namespace std;

Lift::Lift(
        unsigned int currentPosition,
        unsigned int maxCapacity,
        unsigned int liftID,
        std::vector<util::Floor> &building
        )
        :
        maxCapacity{maxCapacity},
        building{building},
        liftID{liftID}
{
    currLiftState.currPosition = currentPosition;
}

void Lift::run() {
    this->myThread = make_unique<thread>(
            ref(*this)
            );
}

[[noreturn]] void Lift::operator()() {
    Graphics::send2Gfx({
        .purpose = util::SimulationPurpose::STDOUT_PRINT,
        .data = {
                {"toPrint", std::string{"Lift online!"}}
        }
    });

    while(true) {
        // Do nothing but receive commands from the lift controller and then execute them!
        this_thread::sleep_for(chrono::milliseconds(my_global_context::UCPU_POLL_SLEEP_MS));         // Lift uCPU freq - TODO: this can later (maybe)be replaced by a wake up call and a conditional variable
        {
            scoped_lock sl{my_global_context::liftStateSharedMutex, my_global_context::liftControllerLiftOrderSharedMutex};
            bool cmdProcessed = this->checkControllerCommands();        // Change state depending on input!
            this->executeCurrentState();            // Depending on the current state, do the output action
            this->sendState2Gfx();
            if(cmdProcessed) {
                this->currCommand = Command::None;      // We applied the input, so now reset the command
            }
        }
    }
}

/**
 * Adds the person to the lift and sets him to riding!
 *
 * @param personId ID of the person that is added
 * @return true if the person entered the lift, false otherwise
 */
bool Lift::enterLift(const char personId) {
    bool tmp;
    {
        lock_guard<shared_mutex> sl{my_global_context::liftStateSharedMutex};
        tmp = this->currLiftState.peopleInLift.size() >= this->maxCapacity;
        if(tmp){
            return false;
        } else {
            this->currLiftState.peopleInLift.emplace(personId);
            return true;
        }
    }
}

/**
 * Removes the person from the lift, and into the exited category!
 *
 * @param personId ID of the person that is to be removed
 * @return true if the person has been removed, false otherwise(e.g. not found in the lift)
 */
bool Lift::leaveLift(const char personId) {
    bool tmp;
    {
        lock_guard<shared_mutex> lg{my_global_context::liftStateSharedMutex};
        tmp = this->currLiftState.peopleInLift.erase(personId);
    }

    return tmp;
}

/**
 * Checks State Machine inputs!
 * @return true if the input command was executed, false if it was delayed!
 */
bool Lift::checkControllerCommands() {
    // Certain states prohibit commands!
    if(this->currLiftState.currLiftMomentum == Momentum::PostMoveStopping || this->currLiftState.currLiftMomentum == Momentum::ForcedStopping) {
        return false;
    }

    // What do I have to do?
    switch(this->currCommand) {
        case Command::None:
            // Lift stays in the same state - no changes
            break;
        case Command::Stop:
            // Stop moving the lift, or stop closing the door, or stop opening the door!
            if(this->currLiftState.currLiftMomentum == Momentum::Moving) {
                // Set destination floor to the next nearest floor
                if(this->currLiftState.currVerticalPosition == VerticalPosition::Floor) {
                    // Stop here!
                    this->destinationFloor = this->currLiftState.currPosition;
                    this->currLiftState.currLiftMomentum = Momentum::PostMoveStopping;
                    this->currLiftState.currLiftWay = util::Way::NONE;
                    this->currLiftState.currDoorState = DoorState::Opening;
                    this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds{my_global_context::LIFT_TIME_DOOR_OPENING_CLOSING_S};
                } else if(this->currLiftState.currVerticalPosition == VerticalPosition::InterSpace) {
                    if(this->currLiftState.currLiftWay == util::Way::UP) {
                        this->destinationFloor = this->currLiftState.currPosition + 1;
                    } else if (this->currLiftState.currLiftWay == util::Way::DOWN) {
                        this->destinationFloor = this->currLiftState.currPosition - 1;
                    }
                    this->currLiftState.currLiftMomentum = Momentum::ForcedStopping;
                    this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds {my_global_context::LIFT_TIME_BETWEEN_FLOOR_TRAVERSAL_S};
                }
            } else if(this->currLiftState.currDoorState == DoorState::Closing) {
                // Need to open the door
                this->currLiftState.currDoorState = DoorState::Opening;
                this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds{my_global_context::LIFT_TIME_DOOR_OPENING_CLOSING_S};
            } else if (this->currLiftState.currDoorState == DoorState::Opening) {
                // Need to close the door
                this->currLiftState.currDoorState = DoorState::Closing;
                this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds{my_global_context::LIFT_TIME_DOOR_OPENING_CLOSING_S};
            }
            break;
        case Command::OpenDoor:
            if(this->currLiftState.currLiftMomentum == Momentum::StationaryPrepping || this->currLiftState.currLiftMomentum == Momentum::StationaryPrepped) {
                this->currLiftState.currLiftMomentum = Momentum::PostMoveStopping;
            } else if(this->currLiftState.currLiftMomentum == Momentum::Moving) {
                throw logic_error("Request to open door while the lift is moving!?!?");
            }

            /*
             * If door isn't opened start opening it
             * If the door is opened, refresh the door opened period
             * If door is opening - go on and continue opening it!
             */
            if(this->currLiftState.currDoorState == DoorState::Open) {
                this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds{my_global_context::LIFT_TIME_DOOR_OPEN_S};
            } else if(this->currLiftState.currDoorState == DoorState::Closed || this->currLiftState.currDoorState == DoorState::Closing) {
                this->currLiftState.currDoorState = DoorState::Opening;
                this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds{my_global_context::LIFT_TIME_DOOR_OPENING_CLOSING_S};
            }
            break;
        case Command::CloseDoor:
            if(this->currLiftState.currDoorState == DoorState::Open || this->currLiftState.currDoorState == DoorState::Opening) {
                this->currLiftState.currDoorState = DoorState::Closing;
                this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds{my_global_context::LIFT_TIME_DOOR_OPENING_CLOSING_S};
            }
            // If the lift is moving, then this command is ignored(The door must be closed in the moving state)
            break;
        case Command::Move:
            if(this->currLiftState.currLiftMomentum == Momentum::Stationary
            && this->destinationFloor == this->currLiftState.currPosition
            && this->currLiftState.currVerticalPosition == VerticalPosition::Floor
            ) {
                // First check whether we are there already? No need to get going then! - Beginning of simulation scenario that can happen!
                this->currLiftState.currLiftMomentum = Momentum::PostMoveStopping;

                if(this->currLiftState.currDoorState == DoorState::Closed) {
                    this->currLiftState.currDoorState = DoorState::Opening;
                    this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds{my_global_context::LIFT_TIME_DOOR_OPENING_CLOSING_S};
                } else if(this->currLiftState.currDoorState == DoorState::Open) {
                    // Prolong the time that lift doors are open!
                    this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds{my_global_context::LIFT_TIME_DOOR_OPEN_S};
                }
                return true;
            } else if(this->currLiftState.currLiftMomentum == Momentum::Stationary) {
                // Get the lift ready to move!
                // Stay in Stationary state while the door isn't closed
                if(this->currLiftState.currDoorState != DoorState::Closed) {
                    //throw logic_error("Door not closed, and Move command was issued - Lift is not ready for such a command yet!");
                    if(this->currLiftState.currDoorState == DoorState::Closing) {
                        return false;       // Wait for the door to close! Then execute the Move command!
                    } else {
                        this->currLiftState.currDoorState = DoorState::Closing;
                        this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds{my_global_context::LIFT_TIME_DOOR_OPENING_CLOSING_S};
                        return false;       // Upper branch will execute on the next CPU cycle and wait for the doors to close!
                    }
                }

                // Once the door is closed go into StationaryPrepping!
                // Set the mov cmd -> prepping -> mov-ing chain into motion!
                this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds{my_global_context::LIFT_TIME_CLOSED_WAIT_S};
                this->currLiftState.currLiftMomentum = Momentum::StationaryPrepping;
            }

            /*
             * If the lift is moving, prepping or prepped and receives another move command then it just goes in this new direction.
             * Change lift way if already moving!
             */
            if(this->currLiftState.currLiftMomentum == Momentum::Moving) {
                if(this->destinationFloor > this->currLiftState.currPosition ) {
                    this->currLiftState.currLiftWay = util::Way::UP;
                } else if(this->destinationFloor < this->currLiftState.currPosition) {
                    this->currLiftState.currLiftWay = util::Way::DOWN;
                }
            }
            break;
        default:
            throw invalid_argument("Action not implemented for this Lift Command!");
    }

    return true;
}

void Lift::executeCurrentState() {
    // This part of state machine checks the current state, and the desired outputs - and progresses according to these values
    switch(this->currLiftState.currLiftMomentum) {
        case Momentum::Stationary:
            // Lift is just chilling there
            if(this->currLiftState.currLiftWay != util::Way::NONE) {
                throw logic_error("Lift had a Way, yet the Lift's Momentum was Stationary?!");
            } else if(this->currLiftState.currVerticalPosition != VerticalPosition::Floor) {
                throw logic_error("Lift was stationary in Floor InterSpace?!");
            }

            // Check Door States and commands!
            switch(currLiftState.currDoorState) {
                case DoorState::Closing:
                {
                    auto now = chrono::high_resolution_clock::now();
                    if(now >= this->endPoint) {
                        this->currLiftState.currDoorState = DoorState::Closed;
                    }
                    break;
                }
                case DoorState::Open:
                    break;          // Door Open - do nothing if no command is present
                case DoorState::Closed:
                    break;          // Door Closed - do nothing if no command is present
                default:
                    throw invalid_argument("Action not implemented for this Door State!");
            }

            break;
        case Momentum::StationaryPrepping:
        {
            // Is the door closed? Doors must be closed for a certain amount of time while prepping -> after this time elapses the lift is prepped!
            if(this->currLiftState.currDoorState != DoorState::Closed) {
                throw logic_error("Lift doors were not Closed, yet the Lift's Momentum was Stationary Prepping?!");
            } else if(this->currLiftState.currLiftWay != util::Way::NONE) {
                throw logic_error("Lift had a Way, yet the Lift's Momentum was Stationary Prepping?!");
            }

            // Has enough time passed?
            auto now = chrono::high_resolution_clock::now();
            if(now >= this->endPoint) {
                this->currLiftState.currLiftMomentum = Momentum::StationaryPrepped;
                // No break - rather collapse and go into the stationaryPrepped switch case!
            } else {
                break;
            }
        }
        case Momentum::StationaryPrepped:
            // If the door is closed and stationary prep time has elapsed we can start moving
            // Check doors
            if(this->currLiftState.currDoorState != DoorState::Closed) {
                throw logic_error("Door not closed during Lift Stationary Prepped!?");
            } else if(this->currLiftState.currLiftWay != util::Way::NONE) {
                throw logic_error("Lift already had a Way, yet the Lift's Momentum was Stationary Prepped(not Moving)?!");
            }

            // Update way, set us to moving
            if(destinationFloor > currLiftState.currPosition) {
                currLiftState.currLiftWay = util::Way::UP;
            } else if (destinationFloor < currLiftState.currPosition) {
                currLiftState.currLiftWay = util::Way::DOWN;
            } else {
                throw logic_error("Lift Source Floor and Destination Floor are the same?!");
            }

            this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds{my_global_context::LIFT_TIME_BETWEEN_FLOOR_TRAVERSAL_S};
            this->currLiftState.currLiftMomentum = Momentum::Moving;
        case Momentum::Moving:
        {
            if(this->currLiftState.currDoorState != DoorState::Closed) {
                throw logic_error("Door not closed during Lift Moving!?");
            } else if(this->destinationFloor > my_global_context::CREATION_BUILDING_MAX_FLOOR || this->destinationFloor < 0) {
                throw invalid_argument("Destination floor not in allowed range!");
            }

            // Check traversal time
            auto now = chrono::high_resolution_clock::now();
            if(now < this->endPoint) {
                break;          // Not yet! Wait some more!
            }

            if(this->currLiftState.currPosition == this->destinationFloor && this->currLiftState.currVerticalPosition == VerticalPosition::Floor) {
                // Arrival to our Destination! Auto-Open the Door
                this->currLiftState.currLiftMomentum = Momentum::PostMoveStopping;
                this->currLiftState.currLiftWay = util::Way::NONE;
                this->currLiftState.currDoorState = DoorState::Opening;
                this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds(my_global_context::LIFT_TIME_DOOR_OPENING_CLOSING_S);
            } else {
                switch(this->currLiftState.currLiftWay) {
                    case util::Way::UP:
                        switch(this->currLiftState.currVerticalPosition) {
                            case VerticalPosition::Floor:
                                this->currLiftState.currVerticalPosition = VerticalPosition::InterSpace;
                                break;
                            case VerticalPosition::InterSpace:
                                this->currLiftState.currVerticalPosition = VerticalPosition::Floor;
                                this->currLiftState.currPosition++;
                                break;
                            default:
                                throw invalid_argument("Action not implemented for this VerticalPosition!");
                        }
                        break;
                    case util::Way::DOWN:
                        switch(this->currLiftState.currVerticalPosition) {
                            case VerticalPosition::Floor:
                                this->currLiftState.currVerticalPosition = VerticalPosition::InterSpace;
                                break;
                            case VerticalPosition::InterSpace:
                                this->currLiftState.currVerticalPosition = VerticalPosition::Floor;
                                this->currLiftState.currPosition--;
                                break;
                            default:
                                throw invalid_argument("Action not implemented for this VerticalPosition!");
                        }
                        break;
                    default:
                        throw logic_error("Lift is Moving, but no Way is set!?");
                }
                // The travelling time to the next stop
                this->endPoint += chrono::seconds(my_global_context::LIFT_TIME_BETWEEN_FLOOR_TRAVERSAL_S);
            }
            break;
        }
        case Momentum::PostMoveStopping:
        {
            if(this->currLiftState.currLiftWay != util::Way::NONE) {
                throw logic_error("Lift Way was not NONE, yet the Lift's Momentum was PostMoveStopping?!");
            } else if(this->currLiftState.currDoorState != DoorState::Opening && this->currLiftState.currDoorState != DoorState::Open) {
                throw logic_error("Lift Door State was not Opening or Open, yet the Lift's Momentum was PostMoveStopping?!");
            }

            // Checking whether enough time has passed for substates
            auto now = chrono::high_resolution_clock::now();
            if(now < this->endPoint) {
                break;
            } else {
                if(this->currLiftState.currDoorState == DoorState::Opening) {
                    // Door has been opened -> go to another sub-state
                    this->currLiftState.currDoorState = DoorState::Open;
                    this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds{my_global_context::LIFT_TIME_DOOR_OPEN_S};
                } else if(this->currLiftState.currDoorState == DoorState::Open) {
                    // Excellent, we're free now for further Orders!
                    this->currLiftState.currLiftMomentum = Momentum::Stationary;
                }
            }
        }
        break;
        case Momentum::ForcedStopping:
        {
            // Sleep one more floor traversal seconds cycle - and then stop!
            // Is the door closed?
            if(this->currLiftState.currDoorState != DoorState::Closed) {
                throw logic_error("Lift doors were not Closed, yet the Lift's Momentum was ForcedStopping?!");
            }

            // Has enough time passed
            auto now = chrono::high_resolution_clock::now();
            if(now >= this->endPoint) {
                this->currLiftState.currLiftMomentum = Momentum::PostMoveStopping;
                this->currLiftState.currLiftWay = util::Way::NONE;

                // Set door to Opening!
                this->currLiftState.currDoorState = DoorState::Opening;
                this->endPoint = chrono::high_resolution_clock::now() + chrono::seconds{my_global_context::LIFT_TIME_DOOR_OPENING_CLOSING_S};
            }
            break;
        }
        default:
            throw invalid_argument("Action not implemented for this Momentum!");
    }
}

bool Lift::isLiftDoorOpen() const {
    bool toRet;
    {
        shared_lock<shared_mutex> lg(my_global_context::liftStateSharedMutex);
        toRet = this->currLiftState.currDoorState == DoorState::Open;
    }
    return toRet;
}

unsigned int Lift::getCurrentPosition() const {
    unsigned int toRet;
    {
        shared_lock<shared_mutex> lg(my_global_context::liftStateSharedMutex);
        toRet = this->currLiftState.currPosition;
    }
    return toRet;
}

bool Lift::isLiftStatic() const {
    bool toRet;
    {
        shared_lock<shared_mutex> lg(my_global_context::liftStateSharedMutex);
        toRet = this->currLiftState.currLiftMomentum == Momentum::Stationary ||
                this->currLiftState.currLiftMomentum == Momentum::PostMoveStopping;
    }
    return toRet;
}

Lift::LiftState Lift::getCurrLiftState() const {
    Lift::LiftState toRet;
    {
        shared_lock<shared_mutex> lg(my_global_context::liftStateSharedMutex);
        toRet = this->currLiftState;
    }
    return toRet;
}

void Lift::updateLiftState(std::unordered_map<UpdateKey, std::any> kwargs) {
    {
        lock_guard<shared_mutex> lg{my_global_context::liftStateSharedMutex};
        for(auto & kwarg : kwargs) {
            UpdateKey key = kwarg.first;
            switch(key) {
                case UpdateKey::newDoorState:
                {
                    auto newDoorState = any_cast<Lift::DoorState>(kwarg.second);
                    this->currLiftState.currDoorState = newDoorState;
                }
                    break;
                case UpdateKey::newLiftMomentum:
                {
                    auto newLiftMomentum = any_cast<Lift::Momentum>(kwarg.second);
                    this->currLiftState.currLiftMomentum = newLiftMomentum;
                }
                    break;
                case UpdateKey::newVertPos:
                {
                    auto newVertPos = any_cast<Lift::VerticalPosition>(kwarg.second);
                    this->currLiftState.currVerticalPosition = newVertPos;
                }
                    break;
                case UpdateKey::newPosition:
                {
                    auto newPosition = any_cast<unsigned int>(kwarg.second);
                    this->currLiftState.currPosition = newPosition;
                }
                    break;
                case UpdateKey::newPeople:
                {
                    auto newPeople = any_cast<std::vector<char>>(kwarg.second);
                    for(char newPersonID : newPeople) {
                        this->currLiftState.peopleInLift.emplace(newPersonID);
                    }
                }
                    break;
                case UpdateKey::newLiftWay:
                {
                    auto newLiftWay = any_cast<util::Way>(kwarg.second);
                    this->currLiftState.currLiftWay = newLiftWay;
                }
                    break;
                case UpdateKey::newPerson:
                {
                    auto newPersonID = any_cast<char>(kwarg.second);
                    this->currLiftState.peopleInLift.emplace(newPersonID);
                }
                    break;
                case UpdateKey::delPeople:
                {
                    auto delPeople = any_cast<std::vector<char>>(kwarg.second);
                    for(char delPersonID : delPeople) {
                        this->currLiftState.peopleInLift.erase(delPersonID);
                    }
                }
                    break;
                case UpdateKey::delPerson:
                {
                    auto delPersonID = any_cast<char>(kwarg.second);
                    this->currLiftState.peopleInLift.erase(delPersonID);
                }
                    break;
                default:
                    throw invalid_argument("Action not implemented for this UpdateKey!");
            }
        }
    }
}

std::string Lift::command2String(const Lift::Command cmd) {
    switch(cmd){
        case Command::None:
            return "None";
        case Command::Move:
            return "Move";
        case Command::CloseDoor:
            return "CloseDoor";
        case Command::OpenDoor:
            return "OpenDoor";
        case Command::Stop:
            return "Stop";
        default:
            throw invalid_argument("String returning not implemented for this Command!");
    }
}

std::string Lift::doorState2String(const Lift::DoorState doorState) {
    switch(doorState) {
        case DoorState::Closed:
            return "Closed";
        case DoorState::Closing:
            return "Closing";
        case DoorState::Open:
            return "Open";
        case DoorState::Opening:
            return "Opening";
        default:
            throw invalid_argument("String returning not implemented for this DoorState!");
    }
}

std::string Lift::momentum2String(const Lift::Momentum momentum) {
    switch(momentum) {
        case Lift::Momentum::Stationary:
            return "Stationary";
        case Lift::Momentum::StationaryPrepping:
            return "StationaryPrepping";
        case Lift::Momentum::StationaryPrepped:
            return "StationaryPrepped";
        case Lift::Momentum::Moving:
            return "Moving";
        case Lift::Momentum::ForcedStopping:
            return "ForcedStopping";
        default:
            throw invalid_argument("String returning not implemented for this Momentum!");
    }
}

std::string Lift::verticalPosition2String(const Lift::VerticalPosition verticalPosition) {
    switch(verticalPosition) {
        case VerticalPosition::Floor:
            return "Floor";
        case VerticalPosition::InterSpace:
            return "InterSpace";
        default:
            throw invalid_argument("String returning not implemented for this VerticalPosition!");
    }
}

std::array<char,2> Lift::DoorStateAsciiCode(Lift::DoorState doorState) {
    switch(doorState) {
        case DoorState::Closed:
            return {'-', 'C'};
        case DoorState::Closing:
            return {'!', 'C'};
        case DoorState::Open:
            return {'-', 'O'};
        case DoorState::Opening:
            return {'!', 'O'};
        default:
            throw invalid_argument("String returning not implemented for this DoorState!");
    }
}

void Lift::sendState2Gfx(std::string toPrint) {
    util::GraphicsMessage toSend{
            .purpose = util::SimulationPurpose::LIFT,
            .data = {
                    {"toPrint", toPrint},
                    {"liftState", this->currLiftState},
                    {"command", this->currCommand},
                    {"destinationFloor", this->destinationFloor},
                    {"liftID", this->liftID}
            }
    };

    Graphics::send2Gfx(toSend);
}

bool Lift::isLiftReadyAtFloor(unsigned int floor) const {
    Lift::LiftState currLiftStateTmp = this->getCurrLiftState();
    bool liftNotBusy = this->hasLiftGotNoCommandsIssued();
    return (
            currLiftStateTmp.currLiftMomentum == Lift::Momentum::Stationary
            && currLiftStateTmp.currVerticalPosition == Lift::VerticalPosition::Floor
            && currLiftStateTmp.currLiftWay == util::Way::NONE
            && currLiftStateTmp.currPosition == floor
            && liftNotBusy
            );
}

util::Way Lift::getCurrentGenLiftWay() {
    util::Way toRet;
    {
        std::shared_lock<std::shared_mutex> lg{this->genLiftWaySharedMutex};
        toRet = this->genLiftWay;
    }
    return toRet;
}

bool Lift::hasLiftGotNoCommandsIssued() const {
    bool toRet;
    {
        shared_lock<shared_mutex> sl{my_global_context::liftControllerLiftOrderSharedMutex};
        toRet = this->currCommand == Lift::Command::None;
    }

    return toRet;
}

bool Lift::isLiftReadyForCommand() const {
    Lift::LiftState currLiftStateTmp = this->getCurrLiftState();
    return (
            currLiftStateTmp.currLiftMomentum == Lift::Momentum::Stationary
            && currLiftStateTmp.currVerticalPosition == Lift::VerticalPosition::Floor
            && currLiftStateTmp.currLiftWay == util::Way::NONE
    );
}
