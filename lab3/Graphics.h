//
// Created by kikyy_99 on 14. 12. 2020..
//

#ifndef LAB3_GRAPHICS_H
#define LAB3_GRAPHICS_H

#include <thread>
#include <mutex>
#include <queue>
#include <condition_variable>
#include <shared_mutex>
#include "util.h"
#include "Lift.h"
#include "Passenger.h"

class Graphics {
private:
    enum class LiftGfxPurpose {PASSENGERS_RIDING_CHANGE, OTHER_LIFT_STATE_CHANGE};

    std::unique_ptr<std::thread> graphicsThread;
    std::vector<util::Floor>& building;
    std::array<std::string, 15> prevAsciiGraphics = my_global_context::GRAPHICS_ASCII_EMPTY;
    std::array<std::string, 15> currentAsciiGraphics = my_global_context::GRAPHICS_ASCII_EMPTY;

//    struct LiftAsciiPos {
//        unsigned int floor = 0;
//        Lift::VerticalPosition vertPos = Lift::VerticalPosition::Floor;
//    };
    Lift::LiftState lift1PreviousState{};
    Lift::LiftState lift2PreviousState{};

    // Ascii current position trackers
    unsigned int lift1HorizontalTracker = my_global_context::ASCII_LIFT1_HORIZONTAL_START;
    unsigned int lift2HorizontalTracker = my_global_context::ASCII_LIFT2_HORIZONTAL_START;
    unsigned int lift1VerticalTracker = my_global_context::ASCII_LIFT_VERTICAL_START;
    unsigned int lift2VerticalTracker = my_global_context::ASCII_LIFT_VERTICAL_START;

    unsigned int passengersHorizontalTracker = my_global_context::ASCII_PASSENGERS_HORIZONTAL_START;
    std::array<unsigned int, my_global_context::CREATION_BUILDING_MAX_FLOOR + 1> floorExitedHorizontalTrackers{};
    const std::array<unsigned int, my_global_context::CREATION_BUILDING_MAX_FLOOR + 1> floorVerticalTrackers{};

    void processMessage(util::GraphicsMessage toProcess);
    void draw();
    void updateAscii4Lift(unsigned int liftID, const Lift::LiftState& state, Lift::Command, unsigned int,
                          Graphics::LiftGfxPurpose liftGfxPurpose = LiftGfxPurpose::OTHER_LIFT_STATE_CHANGE);
    void printCurrLiftStateAndOrder(unsigned int liftID, const Lift::LiftState &state, Lift::Command cmd,
                                    unsigned int destinationFloor);
    void updateWaitingList(unsigned int floor);
    void updateLiftList(char personID, Lift::UpdateKey);
    void updateExitList(char personID, unsigned int floor);
public:
    explicit Graphics(std::vector<util::Floor>& building);
    static void send2Gfx(const util::GraphicsMessage& toSend);
    void run();

    [[noreturn]] void operator()();
    ~Graphics();
};


#endif //LAB3_GRAPHICS_H
