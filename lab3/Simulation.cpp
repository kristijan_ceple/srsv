//
// Created by kikyy_99 on 14. 12. 2020..
//

#include <thread>
#include "Simulation.h"
#include "util.h"
#include "Passenger.h"

using namespace std;
using namespace my_global_context;

[[noreturn]] void Simulation::run() {
    graphics.run();
    liftController.run();
    lab3Lift.run();

    while(true) {
        this->passengerGeneration();
        this->generationSleep();
        this->joinPassengers();
    }
}

Simulation::~Simulation() {
    cout << "End of Simulation!" << endl;
}

bool Simulation::checkChance(double chance) {
    switch(currLoadSeverity) {
        case util::LoadSeverity::LOW:
            return chance < my_global_context::CREATION_LOW_SEVR_CHANCE;
        case util::LoadSeverity::MEDIUM:
            return chance < my_global_context::CREATION_MED_SEVR_CHANCE;
        case util::LoadSeverity::HIGH:
            return chance < my_global_context::CREATION_HIGH_SEVR_CHANCE;
        default:
            throw invalid_argument("Action not implemented for this severity load level!");
    }
}

unsigned int Simulation::generateN() {
    double rand = randomGenerator(engine);
    switch(currLoadSeverity) {
        case util::LoadSeverity::LOW:
            return rand * my_global_context::CREATION_LOW_SEVR_RANGE_DELTA + my_global_context::CREATION_LOW_SEVR_RANGE[0];
        case util::LoadSeverity::MEDIUM:
            return rand * my_global_context::CREATION_MED_SEVR_RANGE_DELTA + my_global_context::CREATION_MED_SEVR_RANGE[0];
        case util::LoadSeverity::HIGH:
            return rand * my_global_context::CREATION_HIGH_SEVR_RANGE_DELTA + my_global_context::CREATION_HIGH_SEVR_RANGE[0];
        default:
            throw invalid_argument("Action not implemented for this severity load level!");
    }
}

void Simulation::passengerGeneration() {
    // Will we even be creating anything? Take a look at chance!
    double chance = randomGenerator(engine);
    bool toContinue = this->checkChance(chance);
    if(!toContinue) {
        return;
    }

    // Choose random amount of passengers to create
    unsigned int n = this->generateN();
    for(int i = 0; i < n; i++) {
        if(this->available_passenger_ids.empty()){
            continue;       // No ids available - full capacity lifts!
        }

        // Take an available id!
        auto it = this->available_passenger_ids.begin();
        unsigned int id = *it;
        this->available_passenger_ids.erase(id);

        // For each Passenger, choose a random source, and random dest floor
        unsigned int srcFloor = floorsRandom(engine);
        // Check if this floor max capacity has already been reached
        unsigned int peopleN;
        {
            shared_lock<shared_mutex> sl{this->building[srcFloor].peopleWaitingMutex};
            peopleN = this->building[srcFloor].peopleWaitingAtThisFloor.size();
        }
        if(peopleN >= my_global_context::FLOOR_MAX_WAITING_PEOPLE) {
            continue;       // Do not generate this Passenger!
        }

        unsigned int destFloor;
        do {
            destFloor = floorsRandom(engine);
        } while(destFloor == srcFloor);

        util::Way myWay = util::Way::DOWN;
        if(destFloor > srcFloor) {
            myWay = util::Way::UP;
        }

        Passenger& lastPerson = this->passengers.emplace_back(
                id, srcFloor, destFloor, myWay, this->building,
                lab3Lift
                );
        {
            lock_guard<shared_mutex> lg{this->building[srcFloor].peopleWaitingMutex};
            this->building[srcFloor].peopleWaitingAtThisFloor.emplace_back(id);
        }
        //this_thread::sleep_for(chrono::milliseconds(my_global_context::PERSON_ARRIVAL_TIME_MS));
        lastPerson.run();
    }
}

void Simulation::generationSleep() {
    switch(currLoadSeverity) {
        case util::LoadSeverity::LOW:
            this_thread::sleep_for(chrono::seconds(CREATION_LOW_SEVR_SLEEP_TIME_S));
            break;
        case util::LoadSeverity::MEDIUM:
            this_thread::sleep_for(chrono::seconds(CREATION_MED_SEVR_SLEEP_TIME_S));
            break;
        case util::LoadSeverity::HIGH:
            this_thread::sleep_for(chrono::seconds(CREATION_HIGH_SEVR_SLEEP_TIME_S));
            break;
        default:
            throw invalid_argument("Action not implemented for this severity load level!");
    }
}

Simulation::Simulation() {
    // Generate available IDs into the set
    for(int i = Simulation::PASSENGER_ID_LOWEST; i <= Simulation::PASSENGER_ID_HIGHEST; i++) {
        this->available_passenger_ids.emplace(i);
    }
}


/**
 * Simple Garbage Collection system
 */
void Simulation::joinPassengers() {
    auto currPassenger = this->passengers.begin();
    auto passengersEnd = this->passengers.end();

    while(true) {
        if(currPassenger != passengersEnd && currPassenger->hasPassengerExited()) {
            currPassenger->joinPassengerThread();
            currPassenger = this->passengers.erase(currPassenger);
        } else {
            break;
        }
    }
}
