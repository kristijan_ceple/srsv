toProcess = """
                Lift1      Lift2
Way/Door:        D Z        G O
========================================== Exited =======
4:          |           |         |
  ==========|           |         |
3:          |           |         |
  ==========|           |         |
2:          |           |         | 
  ==========|           |         |
1:          | [        ]| [      ]|
========================================================
Passengers: 
      From: 
        To: 
"""

toProcessLines = toProcess.split("\n")
del toProcessLines[0]
for line in toProcessLines:
    print(f'"{line:57}"')

print(f"The lifts ascii contains {len(toProcessLines)} lines!")
