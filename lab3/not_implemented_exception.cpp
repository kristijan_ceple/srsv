//
// Created by kikyy_99 on 06.01.21.
//

#include "not_implemented_exception.h"

#include <utility>

not_implemented_exception::not_implemented_exception(std::string reason) : reason{std::move(reason)} {}

const char *not_implemented_exception::what() {
    return this->reason.c_str();
}
