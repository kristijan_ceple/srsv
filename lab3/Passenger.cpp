//
// Created by kikyy_99 on 14. 12. 2020..
//

#include <fmt/format.h>
#include <thread>
#include "Passenger.h"
#include "Graphics.h"

using namespace std;

Passenger::Passenger(
        char id,
        unsigned int sourceFloor, unsigned int destFloor, util::Way myWay,
        std::vector<util::Floor>& building,
        Lift& lab3Lift
        )
    :
    id{id},
    sourceFloor {sourceFloor},
    destFloor {destFloor},
    myWay {myWay},
    building{building},
    lab3Lift{lab3Lift}
{
//    std::cout <<
//        fmt::format("Passenger ID:{} Constructed! Source Floor:{}, Dest Floor:{}, this = {}",
//                    id, sourceFloor, destFloor, reinterpret_cast<intptr_t>(this))
//        << std::endl;
}

void Passenger::run() {
// Launch myself into a separate thread
    this->myThread = make_unique<thread>(
            ref(*this)
            );
    this->threadLaunched = true;
}

void Passenger::operator()() {
    //this_thread::sleep_for(chrono::milliseconds(my_global_context::PERSON_ARRIVAL_TIME_MS));

    // Arrival - need to tell the graphics component I've arrived
    string arrivalMessage = fmt::format(
            "Passenger ID:{} Arrived! I am waiting at: {}; I want to go to: {} - therefore my Way is: {}",
            id, sourceFloor, destFloor, util::Way2Str(myWay)
    );
    util::GraphicsMessage arrivalGfxMsg = {
            util::SimulationPurpose::PASSENGER,
            {
                    {"toPrint", arrivalMessage},
                    {"cycleState", util::PassengerCycleState::WAITING},
                    {"id", this->id},
                    {"from", this->sourceFloor},
                    {"to", this->destFloor}
            }
    };

    Graphics::send2Gfx(arrivalGfxMsg);

    // Now I have to press the button in a random interval
    this_thread::sleep_for(chrono::milliseconds(my_global_context::HUMAN_REACTION_TIME_MS));
    this->pressButton();
    this->waitForLift();
    this->riding();

    // Add myself to the building exit list
    {
        lock_guard<shared_mutex> lg{this->building[this->sourceFloor].peopleExitedMutex};
        this->building[this->sourceFloor].peopleExitedAtThisFloor.emplace_back(this->id);
    }
}

void Passenger::pressButton() {
    // First get the button and mutex
    auto ret = this->building[this->sourceFloor][this->myWay];
    {
        unique_lock<mutex> ul{get<mutex &>(ret)};
        get<bool &>(ret) = true;
    }

    // Now notify the Lift Controller and the Graphics component
    // Time to notify the Lift Controller - Make a request
    {
        lock_guard<mutex> lg{my_global_context::liftControllerRequestsMutex};
        my_global_context::liftControllerRequestsQueue.emplace(this->sourceFloor, this->destFloor, this->myWay);
        my_global_context::liftControllerRequestReady = true;
    }
    my_global_context::liftControllerRequestsCv.notify_one();

    this->currState = util::PassengerCycleState::BUTTON_PRESS;

    // Notify the Gfx component
    string buttonPressMsg = fmt::format(
            "Pressing the button - Id: {}, SourceF: {}, DestF:{}, My Way:{}",
            this->id, this->sourceFloor, this->destFloor, util::Way2Str(this->myWay)
    );
    util::GraphicsMessage arrivalGfxMsg = {
            util::SimulationPurpose::PASSENGER,
            {
                    {"toPrint", buttonPressMsg},
                    {"cycleState", this->currState},
                    {"id", this->id},
                    {"from", this->sourceFloor}
            }
    };
    Graphics::send2Gfx(arrivalGfxMsg);
}

void Passenger::waitForLift() {
    // Sit at my floor, while taking a look at my lift indicator
    while(true) {
        this_thread::sleep_for(chrono::milliseconds{my_global_context::HUMAN_REACTION_TIME_MS});

        // Time to take a look at the lift door on my floor
        unsigned int currLiftPos = this->lab3Lift.getCurrentPosition();
        bool doorOpen = this->lab3Lift.isLiftDoorOpen();
        bool liftStatic = this->lab3Lift.isLiftStatic();
        util::Way lab3LiftGenWay = this->lab3Lift.getCurrentGenLiftWay();

        // (lab3LiftGenWay == myWay || lab3LiftGenWay == util::Way::NONE)
        if(currLiftPos == this->sourceFloor && doorOpen && liftStatic && lab3LiftGenWay == this->myWay) {
            // Need to enter the lift
            bool entered = lab3Lift.enterLift(this->id);

            if(!entered) {
                if(!this->pressedAgain) {
                    this->pressButton();    // Need to make another Request
                    this->pressedAgain = true;
                }

                continue;
            }

            this->currState = util::PassengerCycleState::RIDING;
            break;
        } else {
            this->pressedAgain = false;
        }
    }

    // Notify the Gfx component
    string ridingMsg = fmt::format(
            "Passenger ID: {} entered the lift!",
            this->id
    );
    util::GraphicsMessage ridingGfxMessage = {
            util::SimulationPurpose::PASSENGER,
            {
                    {"toPrint", ridingMsg},
                    {"cycleState", this->currState},
                    {"floor", this->sourceFloor},
                    {"id", this->id}
            }
    };
    Graphics::send2Gfx(ridingGfxMessage);

    // Remove myself from the building waiting list
    {
        lock_guard<shared_mutex> lg{this->building[this->sourceFloor].peopleWaitingMutex};
        this->building[this->sourceFloor].peopleWaitingAtThisFloor.remove(this->id);
    }
}

void Passenger::riding() {
    // Sit in the Lift, while taking a look at my lift indicator
    while(true) {
        this_thread::sleep_for(chrono::milliseconds{my_global_context::HUMAN_REACTION_TIME_MS});

        // Time to take a look at the lift door on my floor
        unsigned int currLiftPos = this->lab3Lift.getCurrentPosition();
        bool doorOpen = this->lab3Lift.isLiftDoorOpen();
        bool liftStatic = this->lab3Lift.isLiftStatic();

        if(currLiftPos == this->destFloor && doorOpen && liftStatic) {
            // Need to enter the lift
            lab3Lift.leaveLift(this->id);
            this->currState = util::PassengerCycleState::EXITED;
            break;
        }
    }

    // Notify the Gfx component
    string exitMsg = fmt::format(
            "Passenger ID: {} exited the lift!",
            this->id
    );
    util::GraphicsMessage exitGfxMessage = {
            util::SimulationPurpose::PASSENGER,
            {
                    {"toPrint", exitMsg},
                    {"cycleState", this->currState},
                    {"floor", this->destFloor},
                    {"id", this->id}
            }
    };
    Graphics::send2Gfx(exitGfxMessage);
}

Passenger::~Passenger() {
    this->threadDone = true;
}
