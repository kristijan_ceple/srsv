#include <iostream>
#include <vector>
#include <tuple>
using namespace std;

inline std::string trafficASCIIFilledLab2[] {
        std::string(R"(                         (NORTH)                       )") + "\n",
        std::string(R"(                                                       )") + "\n",
        std::string(R"(                    |       | W R D |                  )") + "\n",
        std::string(R"(                    |       | W R D |                  )") + "\n",
        std::string(R"(                    |       | W R D |                  )") + "\n",
        std::string(R"(                    | D R N | W R D |                  )") + "\n",
        std::string(R"(                   p| p p p | p p p |p                 )") + "\n",
        std::string(R"(                  p | D R N | W R D | p                )") + "\n",
        std::string(R"(            --------+ / | \ | W R D +--------          )") + "\n",
        std::string(R"(            D D D p D D R N   W R D \ p D              )") + "\n",
        std::string(R"(            R R R p R R R N R W R R - p R              )") + "\n",
        std::string(R"(            S S S p S S R X W E R E / p E              )") + "\n",
        std::string(R"((WEST)      ---------   R  XX   R   ---------    (EAST))") + "\n",
        std::string(R"(                W p / W R X N X R N N p N N N          )") + "\n",
        std::string(R"(                R p - R R R R R R R R p R R R          )") + "\n",
        std::string(R"(                D p \ D R E   S R D D p D D D          )") + "\n",
        std::string(R"(            --------+ D R E | \ | / +--------          )") + "\n",
        std::string(R"(                  p | D R E | S R D | p                )") + "\n",
        std::string(R"(                   p| p p p | p p p |p                 )") + "\n",
        std::string(R"(                    | D R E | S R D |                  )") + "\n",
        std::string(R"(                    | D R E |       |                  )") + "\n",
        std::string(R"(                    | D R E |       |                  )") + "\n",
        std::string(R"(                    | D R E |       |                  )") + "\n",
        std::string(R"(                                                       )") + "\n",
        std::string(R"(                         (SOUTH)                       )") + "\n"
};

inline std::string trafficASCIIEmptyLab2[] {
        std::string(R"(                         (NORTH)                       )") + "\n",
        std::string(R"(                                                       )") + "\n",
        std::string(R"(                    |       |       |                  )") + "\n",
        std::string(R"(                    |       |       |                  )") + "\n",
        std::string(R"(                    |       |       |                  )") + "\n",
        std::string(R"(                    | D R L |       |                  )") + "\n",
        std::string(R"(                   p|       |       |p                 )") + "\n",
        std::string(R"(                  p |       |       | p                )") + "\n",
        std::string(R"(            --------+ / | \ |       +--------          )") + "\n",
        std::string(R"(                                    \  D               )") + "\n",
        std::string(R"(                                    -  R               )") + "\n",
        std::string(R"(                                    /  L               )") + "\n",
        std::string(R"((WEST)      ---------               ---------    (EAST))") + "\n",
        std::string(R"(                  L /                                  )") + "\n",
        std::string(R"(                  R -                                  )") + "\n",
        std::string(R"(                  D \                                  )") + "\n",
        std::string(R"(            --------+       | \ | / +--------          )") + "\n",
        std::string(R"(                  p |       |       | p                )") + "\n",
        std::string(R"(                   p|       |       |p                 )") + "\n",
        std::string(R"(                    |       | L R D |                  )") + "\n",
        std::string(R"(                    |       |       |                  )") + "\n",
        std::string(R"(                    |       |       |                  )") + "\n",
        std::string(R"(                    |       |       |                  )") + "\n",
        std::string(R"(                                                       )") + "\n",
        std::string(R"(                         (SOUTH)                       )") + "\n"
};

inline std::string trafficLab2Sandbox[] {
        std::string(R"(                         (;;;;;)                       )") + "\n",
        std::string(R"(                                                       )") + "\n",
        std::string(R"(                    |       |       |                  )") + "\n",
        std::string(R"(                    |       |       |                  )") + "\n",
        std::string(R"(                    |       |       |                  )") + "\n",
        std::string(R"(                    |       |       |                  )") + "\n",
        std::string(R"(                   p|       |       |p                 )") + "\n",
        std::string(R"(                  p |       |       | p                )") + "\n",
        std::string(R"(            --------+       |       +--------          )") + "\n",
        std::string(R"(                                                       )") + "\n",
        std::string(R"(                                                       )") + "\n",
        std::string(R"(                              L L L L                  )") + "\n",
        std::string(R"((;;;;)      ---------       L       ---------    (;;;;))") + "\n",
        std::string(R"(                          L                            )") + "\n",
        std::string(R"(                          L                            )") + "\n",
        std::string(R"(                          L                            )") + "\n",
        std::string(R"(            --------+     L |       +--------          )") + "\n",
        std::string(R"(                  p |     L |       | p                )") + "\n",
        std::string(R"(                   p|     L |       |p                 )") + "\n",
        std::string(R"(                    |     L |       |                  )") + "\n",
        std::string(R"(                    |     L |       |                  )") + "\n",
        std::string(R"(                    |     L |       |                  )") + "\n",
        std::string(R"(                    |     L |       |                  )") + "\n",
        std::string(R"(                                                       )") + "\n",
        std::string(R"(                         (;;;;;)                       )") + "\n"
};

void specificFinder()
{
    char toFind[] = {'D', 'R', 'L', 'P', '\\', ':', '/'};
    vector<tuple<unsigned, unsigned, char>> positions;

    unsigned array_len = 25;
    unsigned string_len = trafficLab2Sandbox[0].size();
    for(unsigned i = 0; i < array_len; i++) {
        const string& currString = trafficLab2Sandbox[i];
        for(unsigned j = 0; j < string_len; j++) {
            const char& currChar = currString[j];
            for(const char& cmpChar : toFind) {
                if(currChar == cmpChar) {
                    positions.emplace_back(i, j, currChar);
                }
            }
        }
    }

    cout << "The following positions have been found:" << endl;
    char prevChar = ';', currChar;
    for(const tuple<unsigned, unsigned, char>& currTuple : positions) {
        currChar = get<2>(currTuple);
        if(currChar != prevChar) {
            prevChar = currChar;
            printf("%c:\n", prevChar);
        }

        printf("std::array<unsigned,2>{%u,%u},\n", get<0>(currTuple), get<1>(currTuple), get<2>(currTuple));
    }
}

void formatsPrinter()
{
    std::cout << "This program prints the whole ASCII trafic in 2 formats: " << std::endl;
    cout << "\ta) Normal string" << endl;
    cout << "\tb) Instead of each character prints (x, char, y)" << endl;
    cout << "\nThis program was used in lab2 to help enumerate positions of various characters!" << endl << endl;

    cout << "a) Original" << endl;
    unsigned array_len = 25;
    unsigned string_len = trafficASCIIFilledLab2[0].size();
    for(int i = 0; i < array_len; i++) {
        cout << trafficASCIIFilledLab2[i];
    }

    cout << endl << endl << endl;
    cout << "b) Positions" << endl;
    string* currString = nullptr;
    for(int i = 0; i < array_len; i++) {
        currString = &(trafficASCIIFilledLab2[i]);
        for(int j = 0; j < string_len; j++) {
            printf("(%d,%c,%d)", i, (*currString).at(j), j);
        }
        cout << endl;
    }
}

int main() {
    specificFinder();
    return 0;
}
