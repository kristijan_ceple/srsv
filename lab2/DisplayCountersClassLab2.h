//
// Created by kikyy_99 on 03. 11. 2020..
//

#ifndef LAB2_DISPLAYCOUNTERSCLASSLAB2_H
#define LAB2_DISPLAYCOUNTERSCLASSLAB2_H

#include <array>
#include <variant>
#include "util.h"

class DisplayCountersClassLab2 {
    friend class Display;
private:
    // Cars
//    std::array<unsigned,3> carsWaitingNorth{0,0,0};
//    std::array<unsigned,3>  carsWaitingSouth{0,0,0};
//    std::array<unsigned,3>  carsWaitingWest{0,0,0};
//    std::array<unsigned,3>  carsWaitingEast{0,0,0};

//    std::array<unsigned,3>  carsPassingNorth{0,0,0};
//    std::array<unsigned,3>  carsPassingSouth{0,0,0};
//    std::array<unsigned,3>  carsPassingWest{0,0,0};
//    std::array<unsigned,3>  carsPassingEast{0,0,0};

    std::array<std::array<unsigned,3>,4> carsWaiting{
            std::array<unsigned,3>{0, 0, 0},
            std::array<unsigned,3>{0, 0, 0},
            std::array<unsigned,3>{0, 0, 0},
            std::array<unsigned,3>{0, 0, 0}
    };

    std::array<std::array<unsigned,3>,4> carsPassing{
            std::array<unsigned,3>{0, 0, 0},
            std::array<unsigned,3>{0, 0, 0},
            std::array<unsigned,3>{0, 0, 0},
            std::array<unsigned,3>{0, 0, 0}
    };


    // Pedestrians
    std::array<std::array<unsigned, 4>, 4> pedestriansWaiting{{
                                                                      {0, 0, 0, 0},
                                                                      {0, 0, 0, 0},
                                                                      {0, 0, 0, 0},
                                                                      {0, 0, 0, 0},
                                                              }};


    std::array<unsigned,4> pedestriansPassing{0, 0, 0, 0};

    DisplayCountersClassLab2() = default;

//    unsigned& getCarCounter(util::State state, util::Compass carLocation, util::Direction carDirection);
    std::pair<
            std::variant<
                    std::reference_wrapper<const std::array<unsigned, 2>>,                    // For WAITING Position
                    std::reference_wrapper<const std::vector<std::array<unsigned,2>>>         // For PASSING Positions
            >,
            std::reference_wrapper<unsigned>
    > getCarDataStructures(
            util::State state, util::Compass carLocation,
            util::Direction carDirection
            );

    std::pair<
            std::variant<
                    std::reference_wrapper<const std::array<unsigned,2>>,
                    std::reference_wrapper<const std::array<std::array<unsigned,2>, 6>>
            >,
            std::reference_wrapper<unsigned>
    > getPedestrianDataStructures(
        const util::PedestrianCrossing& pedestrianCrossing,
        util::State pedestrianState
        );
};


#endif //LAB2_DISPLAYCOUNTERSCLASSLAB2_H
