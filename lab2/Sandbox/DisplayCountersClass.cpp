//
// Created by kikyy_99 on 21. 10. 2020..
//

#include "DisplayCountersClass.h"
#include <variant>

DisplayCountersClass::InnerHolderClass& DisplayCountersClass::operator[](util::TrafficEntity te) {
    switch(te) {
        case util::TrafficEntity::Car:
            return innerHolderClassCars;
        case util::TrafficEntity::Pedestrian:
            return innerHolderClassPedestrians;
        default:
            throw std::invalid_argument("Indexing not possible for the passed argument!");
    }
}

std::pair<
        unsigned&,
        std::variant<
                unsigned,
                std::reference_wrapper<const std::array<unsigned, 12>>,
                std::reference_wrapper<const std::array<unsigned, 14>>,
                std::reference_wrapper<const std::pair<unsigned, unsigned>>
        >
> DisplayCountersClass::InnerHolderClass::DataCorePedestriansWaiting::operator[](util::PedestrianCrossing index) {
    switch(index.start) {
        case util::Compass::NorthWest:
            switch(index.end) {
                case util::Compass::SouthWest:
                    return {this->data[0], util::ATI_PEDESTRIAN_NORTH_WEST_WAITING_POS };
                case util::Compass::NorthEast:
                    return { this->data[1], util::ATI_PEDESTRIAN_WEST_NORTH_WAITING_POS };
                default:
                    throw std::invalid_argument("Indexing not possible for the passed argument!");
            }
        case util::Compass::NorthEast:
            switch(index.end) {
                case util::Compass::NorthWest:
                    return {this->data[2], util::ATI_PEDESTRIAN_EAST_NORTH_WAITING_POS};
                case util::Compass::SouthEast:
                    return {this->data[3], util::ATI_PEDESTRIAN_NORTH_EAST_WAITING_POS};
                default:
                    throw std::invalid_argument("Indexing not possible for the passed argument!");
            }
        case util::Compass::SouthWest:
            switch(index.end) {
                case util::Compass::NorthWest:
                    return {this->data[4], util::ATI_PEDESTRIAN_SOUTH_WEST_WAITING_POS};
                case util::Compass::SouthEast:
                    return {this->data[5], util::ATI_PEDESTRIAN_WEST_SOUTH_WAITING_POS};
                default:
                    throw std::invalid_argument("Indexing not possible for the passed argument!");
            }
        case util::Compass::SouthEast:
            switch(index.end) {
                case util::Compass::NorthEast:
                    return {this->data[6], util::ATI_PEDESTRIAN_SOUTH_EAST_WAITING_POS};
                case util::Compass::SouthWest:
                    return {this->data[7], util::ATI_PEDESTRIAN_EAST_SOUTH_WAITING_POS};
                default:
                    throw std::invalid_argument("Indexing not possible for the passed argument!");
            }
        default:
            throw std::invalid_argument("Indexing not possible for the passed argument!");
    }
}

std::pair<
        unsigned&,
        std::variant<
                unsigned,
                std::reference_wrapper<const std::array<unsigned, 12>>,
                std::reference_wrapper<const std::array<unsigned, 14>>,
                std::reference_wrapper<const std::pair<unsigned, unsigned>>
        >
> DisplayCountersClass::InnerHolderClass::DataCorePedestriansPassing::operator[](util::PedestrianCrossing index) {
    util::Compass zebraSide;

    switch(index.start) {
        case util::Compass::NorthWest:
            switch(index.end) {
                case util::Compass::SouthWest:
                    // WEST ZEBRA
                    zebraSide = util::Compass::West;
                    break;
                case util::Compass::NorthEast:
                    // NORTH ZEBRA
                    zebraSide = util::Compass::North;
                    break;
                default:
                    throw std::invalid_argument("Indexing not possible for the passed argument!");
            }
            break;
        case util::Compass::NorthEast:
            switch(index.end) {
                case util::Compass::NorthWest:
                    // NORTH ZEBRA
                    zebraSide = util::Compass::North;
                    break;
                case util::Compass::SouthEast:
                    // EAST ZEBRA
                    zebraSide = util::Compass::East;
                    break;
                default:
                    throw std::invalid_argument("Indexing not possible for the passed argument!");
            }
            break;
        case util::Compass::SouthWest:
            switch(index.end) {
                case util::Compass::NorthWest:
                    // WEST ZEBRA
                    zebraSide = util::Compass::West;
                    break;
                case util::Compass::SouthEast:
                    // SOUTH ZEBRA
                    zebraSide = util::Compass::South;
                    break;
                default:
                    throw std::invalid_argument("Indexing not possible for the passed argument!");
            }
            break;
        case util::Compass::SouthEast:
            switch(index.end) {
                case util::Compass::NorthEast:
                    // EAST ZEBRA
                    zebraSide = util::Compass::East;
                    break;
                case util::Compass::SouthWest:
                    // SOUTH ZEBRA
                    zebraSide = util::Compass::South;
                    break;
                default:
                    throw std::invalid_argument("Indexing not possible for the passed argument!");
            }
            break;
        default:
            throw std::invalid_argument("Indexing not possible for the passed argument!");
    }

    switch(zebraSide) {
        case util::Compass::North:
            return {data[0], util::ATI_PEDESTRIAN_NORTH_ZEBRA_POS_PAIR};
        case util::Compass::South:
            return {data[1], util::ATI_PEDESTRIAN_SOUTH_ZEBRA_POS_PAIR};
        case util::Compass::East:
            return {data[2], util::ATI_PEDESTRIAN_EAST_ZEBRA_POS_PAIR};
        case util::Compass::West:
            return {data[3], util::ATI_PEDESTRIAN_WEST_ZEBRA_POS_PAIR};
        default:
            throw std::logic_error("Impossible Pedestrian Zebra Compass value!");
    }
}

std::variant<DisplayCountersClass::InnerHolderClass::DataCoreCars*, DisplayCountersClass::InnerHolderClass::DataCorePedestrians*>
DisplayCountersClass::InnerHolderClassCars::operator[](util::State waitOrPass)
{
    switch(waitOrPass) {
        case util::State::Waiting:
            return &dataCoreCarsWaiting;
        case util::State::Passing:
            return &dataCoreCarsPassing;
        default:
            throw std::invalid_argument("Indexing not possible for the passed argument!");
    }
}

std::variant<DisplayCountersClass::InnerHolderClass::DataCoreCars*,DisplayCountersClass::InnerHolderClass::DataCorePedestrians*>
    DisplayCountersClass::InnerHolderClassPedestrians::operator[](util::State waitOrPass)
{
    switch(waitOrPass) {
        case util::State::Waiting:
            return &dataCorePedestriansWaiting;
        case util::State::Passing:
            return &dataCorePedestriansPassing;
        default:
            throw std::invalid_argument("Indexing not possible for the passed argument!");
    }
}