//
// Created by kikyy_99 on 15. 10. 2020..
//

#ifndef LAB1_CONTROLLER_H
#define LAB1_CONTROLLER_H

#include <mutex>
#include <shared_mutex>
#include <queue>
#include "util.h"


class Controller {
private:
    std::shared_mutex& semaphoresDisplaySharedMutex;
    util::TrafficIntersectionState& semaphoresDisplay;

    std::mutex& metalDetectorsMutex;
    std::queue<util::Compass>& metalDetectorsPositionsQueue;

    std::mutex& pressersMutex;
    std::queue<util::Compass>& pressersPositionsQueue;
public:
    Controller(
            std::shared_mutex& semaphoresDisplaySharedMutex,
            util::TrafficIntersectionState& semaphoresDisplay,
            std::mutex& metalDetectorsMutex,
            std::queue<util::Compass>& metalDetectorsPositionsQueue,
            std::mutex& pressersMutex,
            std::queue<util::Compass>& pressersPositionsQueue
            );
    void run();
    void operator()();
};


#endif //LAB1_CONTROLLER_H
