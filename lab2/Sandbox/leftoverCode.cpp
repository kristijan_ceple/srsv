//
// Created by kikyy_99 on 15. 10. 2020..
//

// ###########################################          OBSOLETE            ############################################
// ###########################################          OBSOLETE            ############################################
// ###########################################          OBSOLETE            ############################################

// ###########################################       DATA STRUCTURES         ###########################################


class AbstractSemaphore {
private:
    string name;
    long id;

public:
    string getName()
    {
        return name;
    }

    long getId()
    {
        return id;
    }

    virtual ~AbstractSemaphore() =0;
};

struct CarSemaphore: AbstractSemaphore {

};

struct PedestrianSemaphore: AbstractSemaphore {

};

struct Lane {
private:
    vector<shared_ptr<CarSemaphore>>& carSemaphores;
    vector<shared_ptr<PedestrianSemaphore>>& pedestrianSemaphores;
public:
    Lane();
};

// ###########################################       DATA STRUCTURES         ###########################################

/*
 * The code below is actually used in Lab1. For Lab2 a remade version was deployed
 */
//bool Display::processCarMessageImproved(const util::CarPedestrianDisplayPayload& payload)
//{
//    Compass carLocation = get<Compass>(payload.payload);
//
//    bool displayChanged = false;
//    if(payload.arrival) {
//        // Other 2 must be false!
//        if(payload.transfer || payload.done) {
//            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
//        }
//
//
//        auto ret = (*std::get<DisplayCountersClass::InnerHolderClass::DataCoreCars*>
//                (this->displayCountersClass[util::TrafficEntity::Car][util::State::Waiting]))[carLocation];
//        unsigned& counterWaiting = ret.first;
//        unsigned position = std::get<unsigned>(ret.second);
//
//        counterWaiting++;
//        if(counterWaiting == 1) {
//            // Turn the display on!
//            this->currentAsciiTraffic[position] = 'A';
//            displayChanged=true;
//        }
//    } else if(payload.transfer) {
//        // Other 2 must be false!
//        if(payload.arrival || payload.done) {
//            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
//        }
//
//        auto retWaiting = (*std::get<DisplayCountersClass::InnerHolderClass::DataCoreCars*>
//                (this->displayCountersClass[util::TrafficEntity::Car][util::State::Waiting]))[carLocation];
//        auto retPassing = (*std::get<DisplayCountersClass::InnerHolderClass::DataCoreCars*>
//                (this->displayCountersClass[util::TrafficEntity::Car][util::State::Passing]))[carLocation];
//        unsigned& counterWaiting = retWaiting.first;
//        unsigned position = std::get<unsigned>(retWaiting.second);
//        counterWaiting--;
//        if(counterWaiting == 0) {
//            this->currentAsciiTraffic[position] = ' ';
//            displayChanged = true;
//        }
//
//        unsigned& counterPassing = retPassing.first;
//        counterPassing++;
//
//        bool holdsArray12 = std::holds_alternative<
//                    std::reference_wrapper<const array<unsigned, 12>>
//                >(retPassing.second);
//        if(holdsArray12) {
//            const array<unsigned, 12>& arr = std::get<std::reference_wrapper<const array<unsigned, 12>>>(retPassing.second);
//
//            if(counterPassing == 1) {
//                for(unsigned num : arr) {
//                    this->currentAsciiTraffic[num] = 'A';
//                }
//
//                displayChanged = true;
//            }
//        } else {
//            const array<unsigned, 14>& arr = std::get<std::reference_wrapper<const array<unsigned, 14>>>(retPassing.second);
//
//            if(counterPassing == 1) {
//                for(unsigned num : arr) {
//                    this->currentAsciiTraffic[num] = 'A';
//                }
//
//                displayChanged = true;
//            }
//        }
//    } else if(payload.done) {
//        // Other 2 must be false!
//        if(payload.arrival || payload.transfer) {
//            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
//        }
//
//        auto retPassing = (*std::get<DisplayCountersClass::InnerHolderClass::DataCoreCars*>
//                (this->displayCountersClass[util::TrafficEntity::Car][util::State::Passing]))[carLocation];
//        unsigned& counterPassing = retPassing.first;
//        counterPassing--;
//
//        bool holdsArray12 = std::holds_alternative<
//                std::reference_wrapper<const array<unsigned, 12>>
//        >(retPassing.second);
//        if(holdsArray12) {
//            const array<unsigned, 12>& arr = std::get<std::reference_wrapper<const array<unsigned, 12>>>(retPassing.second);
//
//            if(counterPassing == 0) {
//                for(unsigned num : arr) {
//                    this->currentAsciiTraffic[num] = ' ';
//                }
//
//                displayChanged = true;
//            }
//        } else {
//            const array<unsigned, 14>& arr = std::get<std::reference_wrapper<const array<unsigned, 14>>>(retPassing.second);
//
//            if(counterPassing == 0) {
//                for(unsigned num : arr) {
//                    this->currentAsciiTraffic[num] = ' ';
//                }
//
//                displayChanged = true;
//            }
//        }
//    } else {
//        throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
//    }
//
//    return displayChanged;
//}
//
//bool Display::processPedestrianMessageImproved(const util::CarPedestrianDisplayPayload& payload)
//{
//    PedestrianCrossing pedestrianCrossing = get<PedestrianCrossing>(payload.payload);
//    bool displayChanged = false;
//
//    if(payload.arrival) {
//        //      ########################################################################################################
//        //      #########################################       ARRIVAL        #########################################
//        //      ########################################################################################################
//        // The other 2 must not be true!
//        if(payload.transfer || payload.done) {
//            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
//        }
//
//        auto retWaiting = (*std::get<DisplayCountersClass::InnerHolderClass::DataCorePedestrians*>
//                (this->displayCountersClass[util::TrafficEntity::Pedestrian][util::State::Waiting]))[pedestrianCrossing];
//        unsigned& waitingCounter = retWaiting.first;
//        const unsigned location = get<unsigned>(retWaiting.second);
//
//        waitingCounter++;
//        if(waitingCounter == 1) {
//            this->currentAsciiTraffic[location] = 'p';
//            displayChanged = true;
//        }
//    } else if(payload.transfer) {
//        //      ########################################################################################################
//        //      #########################################       TRANSFER        ########################################
//        //      ########################################################################################################
//        // Check that everything else is false!
//        if(payload.arrival || payload.done) {
//            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
//        }
//
//        auto retWaiting = (*std::get<DisplayCountersClass::InnerHolderClass::DataCorePedestrians*>
//                (this->displayCountersClass[util::TrafficEntity::Pedestrian][util::State::Waiting]))[pedestrianCrossing];
//        unsigned& waitingCounter = retWaiting.first;
//        const unsigned location = get<unsigned>(retWaiting.second);
//
//        waitingCounter--;
//        if(waitingCounter == 0) {
//            this->currentAsciiTraffic[location] = ' ';
//            displayChanged = true;
//        }
//
//        auto retPassing = (*std::get<DisplayCountersClass::InnerHolderClass::DataCorePedestrians*>
//                (this->displayCountersClass[util::TrafficEntity::Pedestrian][util::State::Passing]))[pedestrianCrossing];
//        unsigned& passingCounter = retPassing.first;
//        const pair<unsigned, unsigned>& zebra = std::get<reference_wrapper<const std::pair<unsigned, unsigned>>>(retPassing.second);
//        passingCounter++;
//        if(passingCounter == 1) {
//            this->currentAsciiTraffic[zebra.first] = 'p';
//            this->currentAsciiTraffic[zebra.second] = 'p';
//            displayChanged = true;
//        }
//    } else if(payload.done) {
//        //      ########################################################################################################
//        //      #########################################       DONE        ############################################
//        //      ########################################################################################################
//
//        // Check that everything else is false!
//        if(payload.arrival || payload.transfer) {
//            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
//        }
//
//        auto retPassing = (*std::get<DisplayCountersClass::InnerHolderClass::DataCorePedestrians*>
//                (this->displayCountersClass[util::TrafficEntity::Pedestrian][util::State::Passing]))[pedestrianCrossing];
//        unsigned& passingCounter = retPassing.first;
//        const pair<unsigned, unsigned>& zebra = std::get<reference_wrapper<const std::pair<unsigned, unsigned>>>(retPassing.second);
//        passingCounter--;
//        if(passingCounter == 0) {
//            this->currentAsciiTraffic[zebra.first] = ' ';
//            this->currentAsciiTraffic[zebra.second] = ' ';
//            displayChanged = true;
//        }
//
//    } else {
//        throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
//    }
//
//    return displayChanged;
//}

/*
 * The intersection from Lab1
 */
// ###########################################       ASCII TRAFFIC INTERSECTION        ##########################################
inline const unsigned ATI_COLS = 48;
inline const unsigned ATI_ROWS = 21;
inline const std::array<unsigned, 12> ATI_EAST_WEST_CARS_PASSING_POS = {451, 453, 455, 457, 459, 461,
                                                                        547, 549, 551, 553, 555, 557};
inline const std::array<unsigned, 14> ATI_NORTH_SOUTH_CARS_PASSING_POS = {358, 406, 454, 502, 550, 598, 646,
                                                                          362, 410, 458, 506, 554, 602, 650};
inline const unsigned ATI_CAR_NORTH_WAITING_POS = 262;
inline const unsigned ATI_CAR_SOUTH_WAITING_POS = 746;
inline const unsigned ATI_CAR_EAST_WAITING_POS = 464;
inline const unsigned ATI_CAR_WEST_WAITING_POS = 544;

inline const unsigned ATI_PEDESTRIAN_NORTH_WEST_WAITING_POS = 354;
inline const unsigned ATI_PEDESTRIAN_NORTH_EAST_WAITING_POS = 366;
inline const unsigned ATI_PEDESTRIAN_SOUTH_WEST_WAITING_POS = 642;
inline const unsigned ATI_PEDESTRIAN_SOUTH_EAST_WAITING_POS = 654;
inline const unsigned ATI_PEDESTRIAN_WEST_NORTH_WAITING_POS = 307;
inline const unsigned ATI_PEDESTRIAN_WEST_SOUTH_WAITING_POS = 691;
inline const unsigned ATI_PEDESTRIAN_EAST_NORTH_WAITING_POS = 317;
inline const unsigned ATI_PEDESTRIAN_EAST_SOUTH_WAITING_POS = 701;

inline const std::pair<unsigned, unsigned> ATI_PEDESTRIAN_NORTH_ZEBRA_POS_PAIR = {310, 314};
inline const std::pair<unsigned, unsigned> ATI_PEDESTRIAN_SOUTH_ZEBRA_POS_PAIR = {694, 698};
inline const std::pair<unsigned, unsigned> ATI_PEDESTRIAN_WEST_ZEBRA_POS_PAIR = {450, 546};
inline const std::pair<unsigned, unsigned> ATI_PEDESTRIAN_EAST_ZEBRA_POS_PAIR = {462, 558};

inline const std::array<unsigned, 2> ATI_PEDESTRIAN_NORTH_ZEBRA_POS = {310, 314};
inline const std::array<unsigned, 2> ATI_PEDESTRIAN_SOUTH_ZEBRA_POS = {694, 698};
inline const std::array<unsigned, 2> ATI_PEDESTRIAN_WEST_ZEBRA_POS = {450, 546};
inline const std::array<unsigned, 2> ATI_PEDESTRIAN_EAST_ZEBRA_POS = {462, 558};

inline std::string trafficASCIIFilledLab1 =
        "                     (NORTH)                   \n"
        "                                               \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "                    | A |   |                  \n"
        "                   p| p | p |p                 \n"
        "                  p | A | A | p                \n"
        "            --------+ A | A +--------          \n"
        "                  pA AAA AAA Ap A              \n"
        "(WEST)      --------- A   A ---------    (EAST)\n"
        "                A pA AAA AAA Ap                \n"
        "            --------+ A | A +--------          \n"
        "                  p | A | A | p                \n"
        "                   p| p | p |p                 \n"
        "                    |   | A |                  \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "                                               \n"
        "                     (SOUTH)                   \n";

inline std::string trafficASCIIEmptyLab1 =
        "                     (NORTH)                   \n"
        "                                               \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "            --------+   |   +--------          \n"
        "                                               \n"
        "(WEST)      ---------       ---------    (EAST)\n"
        "                                               \n"
        "            --------+   |   +--------          \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "                    |   |   |                  \n"
        "                                               \n"
        "                     (SOUTH)                   \n";


/*
 * The controller from Lab1
 */
[[noreturn]] void controllerLab1(
        shared_mutex& semaphoreDisplaySharedMutex,
        TrafficIntersectionState& semaphoreDisplay,
        mutex& metalDetectorsMutex,
        queue<Compass>& metalDetectorsPositionsQueue,
        mutex& pressersMutex,
        queue<Compass>& pressersPositionQueue,
        mutex& displayMutex,
        queue<DisplayMessage>& displayQueue,
        condition_variable& displayCv,
        bool& displayMessage
)
{
    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        cout << "[CONTROLLER] Starting simulation..." << endl;
        cout << "[CONTROLLER] Controller thread starting..." << endl;
        cout << "[CONTROLLER] Initialising the intersection..." << endl << endl;
//        cout << "[CONTROLLER] Debug info:" << endl;
//        printf("TIME_PEDESTRIAN_GREEN_FULL = %u\n",
//               TIME_PEDESTRIAN_GREEN_FULL);
//        printf("TIME_PEDESTRIAN_GREEN_REDUCED = %u\n",
//               TIME_PEDESTRIAN_GREEN_REDUCED);
//        printf("TIME_PEDESTRIANS_FULL_CROSS_BOOL = %s\n",
//               TIME_PEDESTRIANS_FULL_CROSS_BOOL ? "true" : "false");
//        printf("TIME_PEDESTRIANS_REDUCED_CROSS_BOOL = %s\n",
//               TIME_PEDESTRIANS_REDUCED_CROSS_BOOL ? "true" : "false");
    }

    // Start the semaphore thread, pass the monitor to it
    // ###################################          MONITOR         ####################################################
    mutex controller_sem_mutex;
    condition_variable controller_sem_cv;
    bool changeHappened;
    TrafficIntersectionState trafficIntersectionState
            { Colour::Green,
              Colour::Green,
              Colour::Red,
              Colour::Red
            };

    auto updateSemaphore = [
            &controller_sem_mutex,
            &controller_sem_cv,
            &changeHappened,
            &trafficIntersectionState
    ]
            (const TrafficIntersectionState& toUpdateState)
            -> void
    {
//        // First check whether the program is still even running?
//        if(!G_PROGRAM_RUNNING) {
//            return;
//        }
        unique_lock<mutex> ul(controller_sem_mutex);                        // Entering critical section
        trafficIntersectionState = toUpdateState;                                                                  // Change shared variables
        changeHappened = true;                                              // Change shared variables

//        cout << toPrint << endl;                                            // To keep trace of time in the chat print before notifying the SEM

        ul.unlock();                                                        // All the shared variables changed - exit critical section

        controller_sem_cv.notify_one();                                     // Data is ready - notify the SEM thread!

        ul.lock();                                                          // Enter monitor
        controller_sem_cv.wait(                                             // Enter the queue and wait for a notification from SEM that lights have been updated
                ul,
                [&changeHappened]() { return !changeHappened || !G_PROGRAM_RUNNING; });
    };
    // ###################################          MONITOR         ####################################################
    thread semaphoreThread(
            semaphore,
            ref(controller_sem_mutex),
            ref(controller_sem_cv),
            ref(changeHappened),
            ref(trafficIntersectionState),
            ref(semaphoreDisplaySharedMutex),
            ref(semaphoreDisplay),
            ref(displayMutex),
            ref(displayQueue),
            ref(displayCv),
            ref(displayMessage)
    );

    // #######################################         HELPER VARS         #############################################
    TrafficIntersectionState toUpdateState{};

//    unsigned sleepTmp;
//    bool anyoneWaiting;                 // Regulates the phases duration
//    bool pedestriansEnoughTime;         // Regulates whether pedestrians will even get a green light(it is possible that they cannot cross due to not having enough time)

    unsigned northSouthPedestriansCounter = 0;
    unsigned northSouthCarsCounter = 0;
    unsigned eastWestPedestriansCounter = 0;
    unsigned eastWestCarsCounter = 0;

    auto updateCounters = [
            &pressersMutex,
            &pressersPositionQueue,
            &metalDetectorsMutex,
            &metalDetectorsPositionsQueue,
            &northSouthPedestriansCounter,
            &northSouthCarsCounter,
            &eastWestPedestriansCounter,
            &eastWestCarsCounter
    ] () -> void
    {
        {
            // Check if there were any pedestrians
            lock_guard<mutex> lg(pressersMutex);
            Compass compassTmp;
            while(!pressersPositionQueue.empty()) {
                compassTmp = pressersPositionQueue.front();
                pressersPositionQueue.pop();
                switch(compassTmp) {
                    case Compass::North:
                    case Compass::South:
                        northSouthPedestriansCounter++;
                        break;
                    case Compass::East:
                    case Compass::West:
                        eastWestPedestriansCounter++;
                        break;
                    default:
                        throw invalid_argument("Unsupported Compass value!");
                }
            }
        }
        {
            // Check if there are any cars waiting
            lock_guard<mutex> lg(metalDetectorsMutex);
            Compass compassTmp;
            while(!metalDetectorsPositionsQueue.empty()) {
                compassTmp = metalDetectorsPositionsQueue.front();
                metalDetectorsPositionsQueue.pop();
                switch(compassTmp) {
                    case Compass::North:
                    case Compass::South:
                        northSouthCarsCounter++;
                        break;
                    case Compass::East:
                    case Compass::West:
                        eastWestCarsCounter++;
                        break;
                    default:
                        throw invalid_argument("Unsupported Compass value!");
                }
            }
        }
    };
    // #######################################         HELPER VARS         #############################################

//    string toPrint;
    while(G_PROGRAM_RUNNING) {

        //  ---------------------------------------     EAST-WEST     --------------------------------------------------
        // First check the amount of pedestrians and cars in this lane
        /*
         * In order to check the period we have to go through the queue and count the number of cars/pedestrians waiting
         * and then depending on that regulate the phase durations.
         */
        updateCounters();

        // Now we wanna communicate with the Semaphore and tell it to change states
        {
            lock_guard<mutex> lg(util::my_stdout_mutex);
            cout << "[CONTROLLER] Changing Semaphore states > Deactivating the North-South lane..." << endl;
            cout << "[CONTROLLER] Changing Semaphore states > Activating the East-West lane..." << endl;
        }
//        toPrint = "[CONTROLLER] Changing Semaphore states > Deactivating the North-South lane...\n"
//                  "[CONTROLLER] Changing Semaphore states > Activating the East-West lane...";

        // Choose the right sleep amount depending on the number of cars and pedestrians
        // In both cases we have to take into consideration whether Pedestrians even have enough time to cross the zebra
        if(eastWestPedestriansCounter == 0 && eastWestCarsCounter == 0) {
            // Use the REDUCED mode
//            toPrint += "\n[CONTROLLER] Nobody waiting -> Reduced cycle!";
            {
                lock_guard<mutex> lg(util::my_stdout_mutex);
                cout << "[CONTROLLER] Nobody waiting -> Reduced cycle!" << endl;
            }
            if(util::TIME_PEDESTRIANS_REDUCED_CROSS_BOOL) {
                // Both Pedestrians and Cars cycles
                toUpdateState = {
                        Colour::Red,
                        Colour::Red,
                        Colour::Green,
                        Colour::Green
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_GREEN_REDUCED));

                toUpdateState = {
                        Colour::Red,
                        Colour::Red,
                        Colour::Green,
                        Colour::Red
                };
                {
                    lock_guard<mutex> lg(util::my_stdout_mutex);
                    cout << "[CONTROLLER] Changing Semaphore states > Deactivating the East-West lane pedestrians..." << endl;
                }
//                toPrint = "[CONTROLLER] Changing Semaphore states > Deactivating the East-West lane pedestrians...";
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_RED_DELTA));
            } else {
                // Only Cars cycle - don't even turn the light on for Pedestrians
//                toPrint += "\n[CONTROLLER] Not turning light on for Pedestrians since they can't cross in time!";
                toUpdateState = {
                        Colour::Red,
                        Colour::Red,
                        Colour::Green,
                        Colour::Red
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_GREEN_REDUCED));
            }
        } else {
            // Use the FULL mode
            if(util::TIME_PEDESTRIANS_FULL_CROSS_BOOL) {
                // Both Pedestrians and Cars cycles
                toUpdateState = {
                        Colour::Red,
                        Colour::Red,
                        Colour::Green,
                        Colour::Green
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_GREEN_FULL));

                toUpdateState = {
                        Colour::Red,
                        Colour::Red,
                        Colour::Green,
                        Colour::Red
                };
                {
                    lock_guard<mutex> lg(util::my_stdout_mutex);
                    cout << "[CONTROLLER] Changing Semaphore states > Deactivating the East-West lane pedestrians..." << endl;
                }
//                toPrint = "[CONTROLLER] Changing Semaphore states > Deactivating the East-West lane pedestrians...";
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_RED_DELTA));
            } else {
                // Only Cars cycle - don't even turn the light on for Pedestrians
//                toPrint += "\n[CONTROLLER] Not turning light on for Pedestrians since they can't cross in time!";
                {
                    lock_guard<mutex> lg(util::my_stdout_mutex);
                    cout << "[CONTROLLER] Not turning light on for Pedestrians since they can't cross in time!" << endl;
                }
                toUpdateState = {
                        Colour::Red,
                        Colour::Red,
                        Colour::Green,
                        Colour::Red
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_GREEN_FULL));
            }
        }

        //  ---------------------------------------     INTER-CYCLE     ------------------------------------------------
        {
            lock_guard<mutex> lg(util::my_stdout_mutex);
            cout << "[CONTROLLER] Intercycle period! All lights red!" << endl;
        }
        updateSemaphore(util::INTERCYCLE_TRAFFIC_INTERSECTION_STATE);
        this_thread::sleep_for(chrono::milliseconds(util::TIME_INTERCYCLE_PERIOD));
        //  ---------------------------------------     INTER-CYCLE     ------------------------------------------------


        // Everyone passed the intersection successfully :D
        eastWestCarsCounter = 0;
        eastWestPedestriansCounter = 0;
        //  ---------------------------------------     EAST-WEST     --------------------------------------------------


        //  ---------------------------------------     NORTH-SOUTH     ------------------------------------------------
        // Gotta count number of cars and pedestrians again
        updateCounters();

        // Now we wanna communicate with the Semaphore and tell it to change states
        {
            lock_guard<mutex> lg(util::my_stdout_mutex);
            cout << "[CONTROLLER] Changing Semaphore states > Deactivating the East-West lane..." << endl;
            cout << "[CONTROLLER] Changing Semaphore states > Activating the North-South lane..." << endl;
        }
//        toPrint = "[CONTROLLER] Changing Semaphore states > Deactivating the East-West lane...\n"
//                  "[CONTROLLER] Changing Semaphore states > Activating the North-South lane...";

        // Okay, do the same as was done with East-West except now do it with North-South
        // Choose the right sleep amount depending on the number of cars and pedestrians
        // In both cases we have to take into consideration whether Pedestrians even have enough time to cross the zebra
        if(northSouthPedestriansCounter == 0 && northSouthCarsCounter == 0) {
            // Use the REDUCED mode
            if(util::TIME_PEDESTRIANS_REDUCED_CROSS_BOOL) {
                // Both Pedestrians and Cars cycles
                toUpdateState = {
                        Colour::Green,
                        Colour::Green,
                        Colour::Red,
                        Colour::Red
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_GREEN_REDUCED));

                toUpdateState = {
                        Colour::Green,
                        Colour::Red,
                        Colour::Red,
                        Colour::Red
                };
                {
                    lock_guard<mutex> lg(util::my_stdout_mutex);
                    cout << "[CONTROLLER] Changing Semaphore states > Deactivating the North-South lane pedestrians..." << endl;
                }
//                toPrint = "[CONTROLLER] Changing Semaphore states > Deactivating the North-South lane pedestrians...";
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_RED_DELTA));
            } else {
                // Only cars cycle - don't even turn the light on for Pedestrians
                toUpdateState = {
                        Colour::Green,
                        Colour::Red,
                        Colour::Red,
                        Colour::Red
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_GREEN_REDUCED));
            }
        } else {
            // Use the FULL mode
            if(util::TIME_PEDESTRIANS_FULL_CROSS_BOOL) {
                // Both Pedestrians and Cars cycles
                toUpdateState = {
                        Colour::Green,
                        Colour::Green,
                        Colour::Red,
                        Colour::Red
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_GREEN_FULL));

                toUpdateState = {
                        Colour::Green,
                        Colour::Red,
                        Colour::Red,
                        Colour::Red
                };
                {
                    lock_guard<mutex> lg(util::my_stdout_mutex);
                    cout << "[CONTROLLER] Changing Semaphore states > Deactivating the North-South lane pedestrians..." << endl;
                }
//                toPrint = "[CONTROLLER] Changing Semaphore states > Deactivating the North-South lane pedestrians...";
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_RED_DELTA));
            } else {
                // Only Cars cycle - don't even turn the light on for Pedestrians
                toUpdateState = {
                        Colour::Green,
                        Colour::Red,
                        Colour::Red,
                        Colour::Red
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_GREEN_FULL));
            }
        }

        //  ---------------------------------------     INTER-CYCLE     ------------------------------------------------
        {
            lock_guard<mutex> lg(util::my_stdout_mutex);
            cout << "[CONTROLLER] Intercycle period! All lights red!" << endl;
        }
        updateSemaphore(util::INTERCYCLE_TRAFFIC_INTERSECTION_STATE);
        this_thread::sleep_for(chrono::milliseconds(util::TIME_INTERCYCLE_PERIOD));
        //  ---------------------------------------     INTER-CYCLE     ------------------------------------------------

        // Everyone passed the intersection successfully :D
        northSouthCarsCounter = 0;
        northSouthPedestriansCounter = 0;
        //  ---------------------------------------     NORTH-SOUTH     ------------------------------------------------
    }

    {
        lock_guard<mutex> lg(my_stdout_mutex);
        cout << "[CONTROLLER] Joining semaphore..." << endl;
    }

    controller_sem_cv.notify_one();
    semaphoreThread.join();

    {
        lock_guard<mutex> lg(my_stdout_mutex);
        cout << "[CONTROLLER] Leaving the party due to feirunt..." << endl;
    }
}

/*
 * Keeping code backed up here just in case for future use -- Code from util.h
 */
//    struct PedestriansPassingCounters {
//            unsigned northCnt;
//            unsigned southCnt;
//            unsigned eastCnt;
//            unsigned westCnt;
//    };
//
//    struct CarsPassingCounters {
//        unsigned
//    };

//    struct DisplayCounter {
//        TrafficCounters carsWaitingCounters;
//        TrafficCounters carsPassingCounters;
//        TrafficCounters pedestriansWaitingCounters;
//        TrafficCounters pedestriansPassingCounters;

//    };


/*
 * These constants are obsolete, and have been replaced by more readable and clearer ZEBRA constants  -- Code from util.h
 */
//inline unsigned ATI_PEDESTRIAN_NORTH_WEST_PASSING_POS[] = {450, 546};
//
//inline unsigned ATI_PEDESTRIAN_NORTH_EAST_PASSING_POS[] = {462, 558};
//
//inline unsigned ATI_PEDESTRIAN_SOUTH_WEST_PASSING_POS[] = {450, 546};
//
//inline unsigned ATI_PEDESTRIAN_SOUTH_EAST_PASSING_POS[] = {462, 558};
//
//inline unsigned ATI_PEDESTRIAN_WEST_NORTH_PASSING_POS[] = {310, 314};
//
//inline unsigned ATI_PEDESTRIAN_WEST_SOUTH_PASSING_POS[] = {694, 698};
//
//inline unsigned ATI_PEDESTRIAN_EAST_NORTH_PASSING_POS[] = {310, 314};
//
//inline unsigned ATI_PEDESTRIAN_EAST_SOUTH_PASSING_POS[] = {694, 698};
// ###########################################          OBSOLETE            ############################################