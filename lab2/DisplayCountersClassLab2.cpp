//
// Created by kikyy_99 on 03. 11. 2020..
//

#include "DisplayCountersClassLab2.h"
using namespace std;
using namespace util;

//unsigned &
//DisplayCountersClassLab2::getCarCounter(util::State state, util::Compass carLocation, util::Direction carDirection)
//{
//    switch(state) {
//        case util::State::Waiting:
//            switch(carLocation){
//                case Compass::North:
//                    switch();
//                case Compass::South:
//                case Compass::East:
//                case Compass::West:
//                default:
//                    throw invalid_argument("Cars can only be located at one of 4 locations: North, South, West, or East!");
//            }
//        case util::State::Passing:
//            switch(carLocation) {
//                case Compass::North:
//                    switch(carDirection) {
//                        case Direction::Left:
//                            return this->carsPassingNorthLeft;
//                        case Direction::Forward:
//                            return this->carsPassingNorthForward;
//                        case Direction::Right:
//                            return this->carsPassingNorthRight;
//                        default:
//                            throw invalid_argument("Cars can only go in one of 3 directions: Left, Forward, or Right!");
//                    }
//                case Compass::South:
//                    switch(carDirection) {
//                        case Direction::Left:
//                            return this->carsPassingSouthLeft;
//                        case Direction::Forward:
//                            return this->carsPassingSouthForward;
//                        case Direction::Right:
//                            return this->carsPassingSouthRight;
//                        default:
//                            throw invalid_argument("Cars can only go in one of 3 directions: Left, Forward, or Right!");
//                    }
//                case Compass::West:
//                    switch(carDirection) {
//                        case Direction::Left:
//                            return this->carsPassingWestLeft;
//                        case Direction::Forward:
//                            return this->carsPassingWestForward;
//                        case Direction::Right:
//                            return this->carsPassingWestRight;
//                        default:
//                            throw invalid_argument("Cars can only go in one of 3 directions: Left, Forward, or Right!");
//                    }
//                case Compass::East:
//                    switch(carDirection) {
//                        case Direction::Left:
//                            return this->carsPassingEastLeft;
//                        case Direction::Forward:
//                            return this->carsPassingEastForward;
//                        case Direction::Right:
//                            return this->carsPassingEastRight;
//                        default:
//                            throw invalid_argument("Cars can only go in one of 3 directions: Left, Forward, or Right!");
//                    }
//                default:
//                    throw invalid_argument("Cars can only be located at one of 4 locations: North, South, West, or East!");
//            }
//        default:
//            throw invalid_argument("Car counters exist only for Waiting, or Passing, Cars!");
//    }
//}

std::pair<
        std::variant<
                std::reference_wrapper<const std::array<unsigned, 2>>,
                std::reference_wrapper<const std::vector<std::array<unsigned,2>>>
        >,
        std::reference_wrapper<unsigned>
>
DisplayCountersClassLab2::getCarDataStructures(
        util::State state, util::Compass carLocation, util::Direction carDirection
        )
{
    switch(state) {
        case util::State::Waiting:
            switch(carLocation){
                case Compass::North:
                    return {ATI2_CARS_NORTH_WAITING_POS[static_cast<int>(carDirection)], this->carsWaiting[static_cast<int>(Compass::North)][static_cast<int>(carDirection)]};
                case Compass::South:
                    return {ATI2_CARS_SOUTH_WAITING_POS[static_cast<int>(carDirection)], this->carsWaiting[static_cast<int>(Compass::South)][static_cast<int>(carDirection)]};
                case Compass::East:
                    return {ATI2_CARS_EAST_WAITING_POS[static_cast<int>(carDirection)], this->carsWaiting[static_cast<int>(Compass::East)][static_cast<int>(carDirection)]};
                case Compass::West:
                    return {ATI2_CARS_WEST_WAITING_POS[static_cast<int>(carDirection)], this->carsWaiting[static_cast<int>(Compass::West)][static_cast<int>(carDirection)]};
                default:
                    throw invalid_argument("Cars can only be located at one of 4 locations: North, South, West, or East!");
            }
        case util::State::Passing:
            switch(carLocation) {
                case Compass::North:
                    return {ATI2_CARS_NORTH_PASSING_POS[static_cast<int>(carDirection)], this->carsPassing[static_cast<int>(Compass::North)][static_cast<int>(carDirection)]};
                case Compass::South:
                    return {ATI2_CARS_SOUTH_PASSING_POS[static_cast<int>(carDirection)], this->carsPassing[static_cast<int>(Compass::South)][static_cast<int>(carDirection)]};
                case Compass::West:
                    return {ATI2_CARS_WEST_PASSING_POS[static_cast<int>(carDirection)], this->carsPassing[static_cast<int>(Compass::West)][static_cast<int>(carDirection)]};
                case Compass::East:
                    return {ATI2_CARS_EAST_PASSING_POS[static_cast<int>(carDirection)], this->carsPassing[static_cast<int>(Compass::East)][static_cast<int>(carDirection)]};
                default:
                    throw invalid_argument("Cars can only be located at one of 4 locations: North, South, West, or East!");
            }
        default:
            throw invalid_argument("Car counters exist only for Waiting, or Passing, Cars!");
    }
}

std::pair<
        std::variant<
                std::reference_wrapper<const std::array<unsigned,2>>,                       // ASCII Pos of a WAITING pedestrian
                std::reference_wrapper<const std::array<std::array<unsigned,2>, 6>>         // Multiple ASCII Pos of a Pedestrian Zebra
        >,
        std::reference_wrapper<unsigned>
> DisplayCountersClassLab2::getPedestrianDataStructures(const PedestrianCrossing& pedestrianCrossing, util::State pedestrianState)
{
    switch(pedestrianState) {
        case State::Waiting:
        {
            unsigned& myCounterWaiting = this->pedestriansWaiting[(int)pedestrianCrossing.start - 4][(int)pedestrianCrossing.end - 4];
            return {util::ATI2_PEDESTRIANS_WAITING[(int)pedestrianCrossing.start - 4][(int)pedestrianCrossing.end - 4], myCounterWaiting };
        }
        case State::Passing:
        {
            Compass zebra = util::zebraFrom2Points(pedestrianCrossing);
            unsigned& myCounterPassing = this->pedestriansPassing[(int)zebra];
            switch(zebra) {
                case Compass::North:
                    return {util::ATI2_PEDESTRIAN_NORTH_ZEBRA_POS, myCounterPassing};
                case Compass::South:
                    return {util::ATI2_PEDESTRIAN_SOUTH_ZEBRA_POS, myCounterPassing};
                case Compass::West:
                    return {util::ATI2_PEDESTRIAN_WEST_ZEBRA_POS, myCounterPassing};
                case Compass::East:
                    return {util::ATI2_PEDESTRIAN_EAST_ZEBRA_POS, myCounterPassing};
                default:
                    throw invalid_argument("Only North, South, West, and East Pedestrian Crossings/Zebras exist!");
            }
        }
        default:
            throw invalid_argument("A Pedestrian can only be Waiting or Passing!");
    }
}
