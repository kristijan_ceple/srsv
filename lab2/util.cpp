//
// Created by kikyy_99 on 15. 10. 2020..
//

#include "util.h"
#include <iostream>
#include <shared_mutex>

using namespace std;
using namespace util;

std::string util::Colour2String(util::Colour toString) {
    switch(toString) {
        case Colour::Green:
            return "Green";
        case Colour::Yellow:
            return "Yellow";
        case Colour::Red:
            return "Red";
        default:
            throw invalid_argument("String returning for this Colour has not been implemented yet!");
    }
}

Compass util::Char2Compass(char side)
{
    switch(side) {
        case 0:
            return Compass::North;
        case 1:
            return Compass::South;
        case 2:
            return Compass::East;
        case 3:
            return Compass::West;
        default:
            throw invalid_argument("Compass returning for this char has not been implemented yet!");
    }
}

string util::Compass2String(Compass toString)
{
    switch(toString) {
        case Compass::North:
            return "North";
        case Compass::South:
            return "South";
        case Compass::East:
            return "East";
        case Compass::West:
            return "West";
        case Compass::NorthWest:
            return "North-West";
        case Compass::NorthEast:
            return "North-East";
        case Compass::SouthWest:
            return "South-West";
        case Compass::SouthEast:
            return "South-East";
        default:
            throw invalid_argument("String returning for this Compass has not been implemented yet!");
    }
}

TrafficEntity util::Char2TrafficEntity(char num)
{
    switch(num) {
        case 0:
            return TrafficEntity::Car;
        case 1:
            return TrafficEntity::Pedestrian;
        default:
            throw invalid_argument("TrafficEntity returning for this char has not been implemented yet!");
    }
}

[[maybe_unused]] string util::TrafficEntity2String(TrafficEntity toString)
{
    switch(toString) {
        case TrafficEntity::Car:
            return "Car";
        case TrafficEntity::Pedestrian:
            return "Pedestrian";
        default:
            throw invalid_argument("String returning for this TrafficEntity has not been implemented yet!");
    }
}

//Compass char2Compass(char toConvert)
//{
//    switch(toConvert) {
//        case 0:
//            return Compass::NorthEast;
//        case 1:
//            return Compass::NorthWest;
//        case 2:
//            return Compass::SouthWest;
//        case 3:
//            return Compass::SouthEast;
//        default:
//            throw invalid_argument("Compass returning for this char has not been implemented yet!");
//    }
//}

PedestrianCrossing util::generatePedestrianCrossing(Compass carLane, char sideOfRoadChar)
{
    /*
     * Maybe the picture would be easier for this one, I'm gonna upload it to git
     * A matrix explaining the situation would look like:
     *  -       -
     * |   1   0  |
     * |   0   1  |
     *  -       -
     * The decoding rule anti-clockwise:
     *      North:
     *          0 - NE
     *          1 - NW
     *      South:
     *          0 - SW
     *          1 - SE
     *      West:
     *          1 - NW
     *          0 - SW
     *      East:
     *          1 - SE
     *          0 - ME
     */
    PedestrianCrossing toRet{};
    switch(carLane) {
        case Compass::North:
            if(sideOfRoadChar == 0) {
                toRet.start = Compass::NorthEast;
                toRet.end = Compass::SouthEast;
            } else {
                toRet.start = Compass::NorthWest;
                toRet.end = Compass::SouthWest;
            }
            break;
        case Compass::South:
            if(sideOfRoadChar == 0) {
                toRet.start = Compass::SouthWest;
                toRet.end = Compass::NorthWest;
            } else {
                toRet.start = Compass::SouthEast;
                toRet.end = Compass::NorthEast;
            }
            break;
        case Compass::East:
            if(sideOfRoadChar == 1) {
                toRet.start = Compass::SouthEast;
                toRet.end = Compass::SouthWest;
            } else {
                toRet.start = Compass::NorthEast;
                toRet.end = Compass::NorthWest;
            }
            break;
        case Compass::West:
            if(sideOfRoadChar == 1) {
                toRet.start = Compass::NorthWest;
                toRet.end = Compass::NorthEast;
            } else {
                toRet.start = Compass::SouthWest;
                toRet.end = Compass::SouthEast;
            }
            break;
        default:
            throw invalid_argument("Pedestrians can only be assigned to 2 major car lanes: North/South or East/West!");
    }

    return toRet;
}

std::string util::Direction2String(Direction toString)
{
    switch(toString) {
        case Direction::Left:
            return "Left";
        case Direction::Forward:
            return "Forward";
        case Direction::Right:
            return "Right";
        default:
            throw invalid_argument("String returning for this TrafficEntity has not been implemented yet!");
    }
}

Direction util::Char2Direction(char num)
{
    switch(num) {
        case 0:
            return Direction::Left;
        case 1:
            return Direction::Forward;
        case 2:
            return Direction::Right;
        default:
            throw invalid_argument("Direction char can only be: 0,1,2!");
    }
}

bool util::carsTakeALook(
        Compass location,
        Direction carDirection,
        shared_mutex& semaphoresDisplayMutex,
        TrafficIntersectionState& semaphoresDisplay,
        Colour& colourTemp,
        uint64_t id
        )
{
    /*
                   * Depending on this car's location, it will have to look at different ports
                   */
    switch(location) {
        case Compass::North:
            switch(carDirection) {
                case Direction::Forward:
                {
                    shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                    colourTemp = semaphoresDisplay.northSouthCarsForwardColour;
                }
                    break;
                case Direction::Left:
                {
                    shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                    colourTemp = semaphoresDisplay.northCarsLeft;
                }
                    break;
                case Direction::Right:
                {
                    shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                    colourTemp = semaphoresDisplay.northCarsRight;
                }
                    break;
                default:
                    throw invalid_argument("Cars can only go: Left, Forward, or Right!");
            }

            break;
        case Compass::South:
            // Needs to look at the Car North-South Semaphore Port!
            switch(carDirection) {
                case Direction::Forward:
                {
                    shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                    colourTemp = semaphoresDisplay.northSouthCarsForwardColour;
                }
                    break;
                case Direction::Left:
                {
                    shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                    colourTemp = semaphoresDisplay.southCarsLeft;
                }
                    break;
                case Direction::Right:
                {
                    shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                    colourTemp = semaphoresDisplay.southCarsRight;
                }
                    break;
                default:
                    throw invalid_argument("Cars can only go: Left, Forward, or Right!");
            }

            break;
        case Compass::East:
            switch(carDirection) {
                case Direction::Forward:
                {
                    shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                    colourTemp = semaphoresDisplay.eastWestCarsForwardColour;
                }
                    break;
                case Direction::Left:
                {
                    shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                    colourTemp = semaphoresDisplay.eastCarsLeft;
                }
                    break;
                case Direction::Right:
                {
                    shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                    colourTemp = semaphoresDisplay.eastCarsRight;
                }
                    break;
                default:
                    throw invalid_argument("Cars can only go: Left, Forward, or Right!");
            }

            break;
        case Compass::West:
            // Needs to look at the Car East-West Semaphore Port!
            switch(carDirection) {
                case Direction::Forward:
                {
                    shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                    colourTemp = semaphoresDisplay.eastWestCarsForwardColour;
                }
                    break;
                case Direction::Left:
                {
                    shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                    colourTemp = semaphoresDisplay.westCarsLeft;
                }
                    break;
                case Direction::Right:
                {
                    shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                    colourTemp = semaphoresDisplay.westCarsRight;
                }
                    break;
                default:
                    throw invalid_argument("Cars can only go: Left, Forward, or Right!");
            }

            break;
        default:
            throw invalid_argument("Cars can only be located at: North, South, East, or West!");
    }

//                {
//                    lock_guard<mutex> lg(util::my_stdout_mutex);
//                    printf("[CAR %lu @ %s] My corresponding semaphore colour: %s\n",
//                           id,
//                           Compass2String(location).c_str(),
//                           Colour2String(colourTemp).c_str()
//                    );
//                }

    return (colourTemp == Colour::Green);
}

bool util::pedestriansTakeALook(
        Compass carLane,
        shared_mutex& semaphoresDisplayMutex,
        TrafficIntersectionState& semaphoresDisplay,
        Colour& colourTemp,
        uint64_t id
        )
{
    /*
                * Depending on this pedestrian's location, it will have to look at different ports
                */
    switch(carLane) {
        case Compass::North:
        case Compass::South:
            // Needs to look at the Pedestrian North-South Semaphore Port!
        {
            shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
            colourTemp = semaphoresDisplay.northSouthPedestriansColour;
        }
            break;
        case Compass::East:
        case Compass::West:
            // Needs to look at the Pedestrian East-West Semaphore Port!
        {
            shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
            colourTemp = semaphoresDisplay.eastWestPedestriansColour;
        }
            break;
        default:
            throw invalid_argument("Car/Major Lanes are only: North, South, East, and West");
    }

//                {
//                    lock_guard<mutex> lg(util::my_stdout_mutex);
//                    printf("[PEDESTRIAN %lu @ %s] My corresponding semaphore colour: %s\n",
//                           id,
//                           Compass2String(location).c_str(),
//                           Colour2String(colourTemp).c_str()
//                    );
//                }

    return (colourTemp == Colour::Green);
}

char util::Direction2ASCII(Direction dir) {
    switch(dir) {
        case Direction::Left:
            return 'L';
        case Direction::Forward:
            return 'R';
        case Direction::Right:
            return 'D';
        default:
            throw invalid_argument("Direction ASCII can only be produced for Directions: Left, Forward, and Right!");
    }
}

Compass util::zebraFrom2Points(const PedestrianCrossing& pedestrianCrossing) {
    switch(pedestrianCrossing.start) {
        case Compass::NorthEast:
            switch(pedestrianCrossing.end) {
                case Compass::NorthWest:
                    return Compass::North;
                case Compass::SouthEast:
                    return Compass::East;
                default:
                    throw invalid_argument("Incorrect Pedestrian Crossing start-end point combination!");
            }
        case Compass::NorthWest:
            switch(pedestrianCrossing.end) {
                case Compass::NorthEast:
                    return Compass::North;
                case Compass::SouthWest:
                    return Compass::West;
                default:
                    throw invalid_argument("Incorrect Pedestrian Crossing start-end point combination!");
            }
        case Compass::SouthWest:
            switch(pedestrianCrossing.end) {
                case Compass::NorthWest:
                    return Compass::West;
                case Compass::SouthEast:
                    return Compass::South;
                default:
                    throw invalid_argument("Incorrect Pedestrian Crossing start-end point combination!");
            }
        case Compass::SouthEast:
            switch(pedestrianCrossing.end) {
                case Compass::SouthWest:
                    return Compass::South;
                case Compass::NorthEast:
                    return Compass::East;
                default:
                    throw invalid_argument("Incorrect Pedestrian Crossing start-end point combination!");
            }
        default:
            throw invalid_argument("Unallowed Pedestrian Crossing start point!");
    }
}

const std::array<unsigned, 2>& CarsWaitingPositions::operator[](const unsigned int index) const
{
    if(index == static_cast<int>(Direction::Left)) {
        return this->left;
    } else if(index == static_cast<int>(Direction::Forward)) {
        return this->forward;
    } else if(index == static_cast<int>(Direction::Right)) {
        return this->right;
    } else {
        throw out_of_range("Index out of range!");
    }
}

const std::vector<std::array<unsigned, 2>>& CarsPassingPositions::operator[](const unsigned int index) const
{
    if(index == static_cast<int>(Direction::Left)) {
        return this->left;
    } else if(index == static_cast<int>(Direction::Forward)) {
        return this->forward;
    } else if(index == static_cast<int>(Direction::Right)) {
        return this->right;
    } else {
        throw out_of_range("Index out of range!");
    }
}

const SemaphorePositions& TrafficSemaphorePositions::operator[](const util::Compass pos) const {
    switch(pos) {
        case Compass::North:
            return this->ATI2_NORTH_SEMAPHORE_POSITIONS;
        case Compass::South:
            return this->ATI2_SOUTH_SEMAPHORE_POSITIONS;
        case Compass::West:
            return this->ATI2_WEST_SEMAPHORE_POSITIONS;
        case Compass::East:
            return this->ATI2_EAST_SEMAPHORE_POSITIONS;
        default:
            throw invalid_argument("Semaphores can only be located at: North, South, East, and West lanes!");
    }
}
