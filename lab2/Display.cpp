//
// Created by kikyy_99 on 18. 10. 2020..
//

#include "Display.h"
#include "util.h"
#include <iostream>
#include <variant>
using namespace std;
using namespace util;

Display::Display(
        const std::string& filename,
        std::mutex& displayMutex,
        std::queue<util::DisplayMessage>& displayQueue,
        std::condition_variable& displayCv,
        bool& messageReady
        )
    : fileToWriteTo{filename},
    displayMutex{displayMutex}, displayCv{displayCv}, displayQueue {displayQueue}, messageReady{messageReady}
    {}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
void Display::run()
{
    // Now need to launch myself into a separate thread
    this->displayThread = thread(
            ref(*this)
            );
    this->displayRunning = true;
}
#pragma clang diagnostic pop


#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
void Display::operator()() {
    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        cout << "[DISPLAY] Display starting up..." << endl;
        cout << this->currentAsciiTraffic << endl;
    }

//    // Open the file for writing output into
//    ofstream outFile;
//    outFile.open(fileToWriteTo, ios::out);

    DisplayMessage tmpDisplayMessage;
    bool retBool = false;
    while(true) {
        {
            unique_lock<mutex> ul(displayMutex);
            if(displayQueue.empty()) {
                // Wait - but only if the queue is empty!
                displayCv.wait(ul, [this]() {
                    return (this->messageReady);
                });

                // A weird SIGSEGV bug can happen here if the queue isn't updated fast enough
                if(displayQueue.empty()) {
                    continue;
                }

                messageReady = false;       // Lower the flag so signal that we've popped the message
            }

            tmpDisplayMessage = displayQueue.front();
            this->displayQueue.pop();
        }

        // Now let's copy the value into the local cache, and then free the mutex
        try {
            switch(tmpDisplayMessage.sender) {
                case TrafficEntity::Car:
//                outFile << "Car sent a message!" << endl;
                    retBool = processCarMessageLab2(
                            get<CarPedestrianDisplayPayload>(tmpDisplayMessage.displayPayload)
                    );

                    if(retBool) {
                        // Semaphore ascii states must not be overwritten by car changes!
                        this->updateSemaphoresToAsciiDisplay();

                        // Now print the ascii!
                        lock_guard<mutex> lg(util::my_stdout_mutex);
                        cout << this->currentAsciiTraffic << endl;
                    }

                    break;
                case TrafficEntity::Pedestrian:
//                outFile << "Pedestrian sent a message!" << endl;
                    retBool = processPedestrianMessageLab2(
                            get<CarPedestrianDisplayPayload>(tmpDisplayMessage.displayPayload)
                    );

                    if(retBool) {
                        // Now print the ascii! No need to update Semaphore ascii states here since Pedestrians do not collide with them/overwrite them
                        lock_guard<mutex> lg(util::my_stdout_mutex);
                        cout << this->currentAsciiTraffic << endl;
                    }

                    break;
                case TrafficEntity::Semaphore:
//                outFile << "Semaphore sent a message!" << endl;
                    processSemaphoreMessageLab2(
                            get<TrafficIntersectionState>(tmpDisplayMessage.displayPayload)
                    );

                    {
                        // Now print the ascii!
                        lock_guard<mutex> lg(util::my_stdout_mutex);
                        cout << this->currentAsciiTraffic << endl;
                    }
//                    {
//                        cout << "[DISPLAY] Received a Display Message from the Semaphore!" << endl;
//                        lock_guard<mutex> lg(util::my_stdout_mutex);
//                    }

                    break;
                default:
                    throw invalid_argument("DisplayMessage handling for this sender is currently not available!");
            }
        } catch(std::bad_variant_access& e) {
            cerr << e.what() << endl;
            exit(-1);
        }


    }
}
// Old code from the above function - keeping it for the future in case I decide to improve the program
//        // Now wait until we have been notified and a message is ready
//        displayCv->wait(ul, [this]() {
//            return *(this->messageReady);
//        });
//        *messageReady = false;       // Resetting the value
#pragma clang diagnostic pop

Display::~Display()
{
    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        cout << "[MAIN] Display destructor called..." << endl;
        cout << "[MAIN] Joining Display..." << endl;
    }
    displayThread.join();
    this->displayRunning = false;
}

void Display::processSemaphoreMessageLab2(const util::TrafficIntersectionState& payload)
{
    this->currTrafficState = payload;
    
    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        // Process data here - stdout inside mutex to avoid chat mixing
        cout << "[DISPLAY] Changing lights! Current state: " << endl;
        printf("\tNorth South Cars: %s\n", Colour2String(this->currTrafficState.northSouthCarsForwardColour).c_str());
        printf("\tNorth South Pedestrians: %s\n", Colour2String(this->currTrafficState.northSouthPedestriansColour).c_str());

        printf("\tEast West Cars: %s\n", Colour2String(this->currTrafficState.eastWestCarsForwardColour).c_str());
        printf("\tEast West Pedestrians: %s\n", Colour2String(this->currTrafficState.eastWestPedestriansColour).c_str());

        printf("\tSouth Cars Left: %s\n", Colour2String(this->currTrafficState.southCarsLeft).c_str());
        printf("\tSouth Cars Right: %s\n", Colour2String(this->currTrafficState.southCarsRight).c_str());

        printf("\tNorth Cars Left: %s\n", Colour2String(this->currTrafficState.northCarsLeft).c_str());
        printf("\tNorth Cars Right: %s\n", Colour2String(this->currTrafficState.northCarsRight).c_str());

        printf("\tEast Cars Left: %s\n", Colour2String(this->currTrafficState.eastCarsLeft).c_str());
        printf("\tEast Cars Right: %s\n", Colour2String(this->currTrafficState.eastCarsRight).c_str());

        printf("\tWest Cars Left: %s\n", Colour2String(this->currTrafficState.westCarsLeft).c_str());
        printf("\tWest Cars Right: %s\n", Colour2String(this->currTrafficState.westCarsRight).c_str());
    }

    // Now turn on-off semaphores on the ASCII!
    this->updateSemaphoresToAsciiDisplay();
}

bool Display::processCarMessageLab2(const CarPedestrianDisplayPayload& payload) {
    const auto& message = get<pair<Compass,Direction>>(payload.payload);
    bool displayChanged = false;
    Compass carLocation = get<Compass>(message);
    Direction carDirection = get<Direction>(message);

    if(payload.arrival) {
        // Other 2 must be false!
        if(payload.transfer || payload.done) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        auto ret = this->displayCounters.getCarDataStructures(State::Waiting, carLocation, carDirection);

        ret.second++;
        if(ret.second == 1) {
            // Turn on the display!
            const array<unsigned,2>& asciiPos = get<reference_wrapper<const array<unsigned,2>>>(ret.first);
            this->currentAsciiTraffic[asciiPos[0]][asciiPos[1]] = util::Direction2ASCII(carDirection);
            displayChanged = true;
        }
    } else if(payload.transfer) {
        // Other 2 must be false!
        if(payload.arrival || payload.done) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        auto ret = this->displayCounters.getCarDataStructures(State::Waiting, carLocation, carDirection);
        unsigned& arrivalCounter = ret.second;

        arrivalCounter--;
        if(arrivalCounter == 0) {
            // Turn on the display!
            const array<unsigned,2>& asciiPos = get<reference_wrapper<const array<unsigned,2>>>(ret.first);
            this->currentAsciiTraffic[asciiPos[0]][asciiPos[1]] = ' ';
            displayChanged = true;
        }

        ret = this->displayCounters.getCarDataStructures(State::Passing, carLocation, carDirection);
        unsigned& transferCounter = ret.second;

        transferCounter++;
        if(transferCounter == 1) {
            const std::vector<std::array<unsigned,2>>& asciiPositions = get<reference_wrapper<const std::vector<std::array<unsigned,2>>>>(ret.first);
            const char asciiChar = util::Direction2ASCII(carDirection);
            for(const auto& asciiPos : asciiPositions) {
                this->currentAsciiTraffic[asciiPos[0]][asciiPos[1]] = asciiChar;
            }
            displayChanged = true;
        }
    } else if(payload.done) {
        // Other 2 must be false!
        if(payload.arrival || payload.transfer) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        auto ret = this->displayCounters.getCarDataStructures(State::Passing, carLocation, carDirection);
        unsigned& transferCounter = ret.second;

        transferCounter--;
        if(transferCounter == 0) {
            const std::vector<std::array<unsigned,2>>& asciiPositions = get<reference_wrapper<const std::vector<std::array<unsigned,2>>>>(ret.first);
            for(const auto& asciiPos : asciiPositions) {
                this->currentAsciiTraffic[asciiPos[0]][asciiPos[1]] = ' ';
            }

            // Sometimes Left directions can cross and accidentally delete each other's characters
            if(carDirection == Direction::Left) {
                this->check2RedrawLeftCounters(carLocation);
            }

            displayChanged = true;
        }
    } else {
        throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
    }

    return displayChanged;
}

bool Display::processPedestrianMessageLab2(const CarPedestrianDisplayPayload& payload)
{
    const PedestrianCrossing& pedestrianCrossing = get<PedestrianCrossing>(payload.payload);
    bool displayChanged = false;

    if(payload.arrival) {
        //      ########################################################################################################
        //      #########################################       ARRIVAL        #########################################
        //      ########################################################################################################
        // The other 2 must not be true!
        if(payload.transfer || payload.done) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }
        auto ret = this->displayCounters.getPedestrianDataStructures(pedestrianCrossing, State::Waiting);

        unsigned& arrivalCounter = ret.second;
        arrivalCounter++;
        if(arrivalCounter == 1) {
            const array<unsigned,2>& asciiPos = get<reference_wrapper<const array<unsigned,2>>>(ret.first);
            this->currentAsciiTraffic[asciiPos[0]][asciiPos[1]] = 'p';
            displayChanged = true;
        }
    } else if(payload.transfer) {
        //      ########################################################################################################
        //      #########################################       TRANSFER        ########################################
        //      ########################################################################################################
        // Check that everything else is false!
        if(payload.arrival || payload.done) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        auto ret = this->displayCounters.getPedestrianDataStructures(pedestrianCrossing, State::Waiting);

        unsigned& arrivalCounter = ret.second;
        arrivalCounter--;
        if(arrivalCounter == 0) {
            const array<unsigned,2>& asciiPos = get<reference_wrapper<const array<unsigned,2>>>(ret.first);
            this->currentAsciiTraffic[asciiPos[0]][asciiPos[1]] = ' ';
            displayChanged = true;
        }

        ret = this->displayCounters.getPedestrianDataStructures(pedestrianCrossing, State::Passing);
        unsigned& transferCounter = ret.second;
        transferCounter++;
        if(transferCounter == 1) {
            const array<array<unsigned,2>, 6>& asciiPositions = get<reference_wrapper<const array<array<unsigned,2>, 6>>>(ret.first);
            for(const auto& currPos : asciiPositions) {
                this->currentAsciiTraffic[currPos[0]][currPos[1]] = 'p';
            }
            displayChanged = true;
        }
    } else if(payload.done) {
        //      ########################################################################################################
        //      #########################################       DONE        ############################################
        //      ########################################################################################################

        // Check that everything else is false!
        if(payload.arrival || payload.transfer) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        auto ret = this->displayCounters.getPedestrianDataStructures(pedestrianCrossing, State::Passing);
        unsigned& transferCounter = ret.second;
        transferCounter--;
        if(transferCounter == 0) {
            const array<array<unsigned,2>, 6>& asciiPositions = get<reference_wrapper<const array<array<unsigned,2>, 6>>>(ret.first);
            for(const auto& currPos : asciiPositions) {
                this->currentAsciiTraffic[currPos[0]][currPos[1]] = ' ';
            }
            displayChanged = true;
        }
    } else {
        throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
    }

    return displayChanged;
}

void Display::updateSemaphoresToAsciiDisplay()
{
    char myChar;

    myChar = '|';
    if(this->currTrafficState.northSouthCarsForwardColour == Colour::Red) {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::North][(int)Direction::Forward];
        this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]] = myChar;

        const auto& positionsTwo = util::ATI2_SEMAPHORES_POSITIONS[Compass::South][(int)Direction::Forward];
        this->currentAsciiTraffic[positionsTwo[0]][positionsTwo[1]] = myChar;
    }
    else {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::North][(int)Direction::Forward];
        char& modifOne = this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]];
        modifOne = (modifOne != myChar) ? modifOne : ' ';

        const auto& positionsTwo = util::ATI2_SEMAPHORES_POSITIONS[Compass::South][(int)Direction::Forward];
        char& modifTwo = this->currentAsciiTraffic[positionsTwo[0]][positionsTwo[1]];
        modifTwo = (modifTwo != myChar) ? modifTwo : ' ';
    }
    myChar = '-';
    if(this->currTrafficState.eastWestCarsForwardColour == Colour::Red) {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::East][(int)Direction::Forward];
        this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]] = myChar;

        const auto& positionsTwo = util::ATI2_SEMAPHORES_POSITIONS[Compass::West][(int)Direction::Forward];
        this->currentAsciiTraffic[positionsTwo[0]][positionsTwo[1]] = myChar;
    }
    else {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::East][(int)Direction::Forward];
        char& modifOne = this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]];
        modifOne = (modifOne != myChar) ? modifOne : ' ';

        const auto& positionsTwo = util::ATI2_SEMAPHORES_POSITIONS[Compass::West][(int)Direction::Forward];
        char& modifTwo = this->currentAsciiTraffic[positionsTwo[0]][positionsTwo[1]];
        modifTwo = (modifTwo != myChar) ? modifTwo : ' ';
    }

    myChar = '\\';
    if(this->currTrafficState.northCarsLeft == Colour::Red) {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::North][(int)Direction::Left];
        this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]] = myChar;
    }
    else {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::North][(int)Direction::Left];
        char& modifOne = this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]];
        modifOne = (modifOne != myChar) ? modifOne : ' ';
    }
    myChar = '/';
    if(this->currTrafficState.northCarsRight == Colour::Red) {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::North][(int)Direction::Right];
        this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]] = myChar;
    }
    else {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::North][(int)Direction::Right];
        char& modifOne = this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]];
        modifOne = (modifOne != myChar) ? modifOne : ' ';
    }

    myChar = '\\';
    if(this->currTrafficState.southCarsLeft == Colour::Red) {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::South][(int)Direction::Left];
        this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]] = myChar;
    }
    else {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::South][(int)Direction::Left];
        char& modifOne = this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]];
        modifOne = (modifOne != myChar) ? modifOne : ' ';
    }
    myChar = '/';
    if(this->currTrafficState.southCarsRight == Colour::Red) {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::South][(int)Direction::Right];
        this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]] = myChar;
    }
    else {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::South][(int)Direction::Right];
        char& modifOne = this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]];
        modifOne = (modifOne != myChar) ? modifOne : ' ';
    }

    myChar = '/';
    if(this->currTrafficState.westCarsLeft == Colour::Red) {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::West][(int)Direction::Left];
        this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]] = myChar;
    }
    else {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::West][(int)Direction::Left];
        char& modifOne = this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]];
        modifOne = (modifOne != myChar) ? modifOne : ' ';
    }
    myChar = '\\';
    if(this->currTrafficState.westCarsRight == Colour::Red) {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::West][(int)Direction::Right];
        this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]] = myChar;
    }
    else {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::West][(int)Direction::Right];
        char& modifOne = this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]];
        modifOne = (modifOne != myChar) ? modifOne : ' ';
    }

    myChar = '/';
    if(this->currTrafficState.eastCarsLeft == Colour::Red) {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::East][(int)Direction::Left];
        this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]] = myChar;
    }
    else {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::East][(int)Direction::Left];
        char& modifOne = this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]];
        modifOne = (modifOne != myChar) ? modifOne : ' ';
    }
    myChar = '\\';
    if(this->currTrafficState.eastCarsRight == Colour::Red) {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::East][(int)Direction::Right];
        this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]] = myChar;
    }
    else {
        const auto& positionsOne = util::ATI2_SEMAPHORES_POSITIONS[Compass::East][(int)Direction::Right];
        char& modifOne = this->currentAsciiTraffic[positionsOne[0]][positionsOne[1]];
        modifOne = (modifOne != myChar) ? modifOne : ' ';
    }
}

void Display::check2RedrawLeftCounters(util::Compass location)
{
    unsigned counter;
    switch(location) {
        case Compass::North:
            // Check Southern counter!
            counter = this->displayCounters.carsPassing[(int)Compass::South][(int)Direction::Left];
            if(counter > 0) {
                // Need to redraw the Southern car Left lane because the Northern one deleted it accidentally!
                for(const asciiPosition& asciiPos : util::ATI2_CARS_SOUTH_PASSING_POS.left) {
                    this->currentAsciiTraffic[asciiPos[0]][asciiPos[1]] = 'L';
                }
            }
            return;
        case Compass::South:
            // Check Northern counter!
            counter = this->displayCounters.carsPassing[(int)Compass::North][(int)Direction::Left];
            if(counter > 0) {
                // Need to redraw!
                for(const asciiPosition& asciiPos : util::ATI2_CARS_NORTH_PASSING_POS.left) {
                    this->currentAsciiTraffic[asciiPos[0]][asciiPos[1]] = 'L';
                }
            }
            return;
        case Compass::East:
            // Look at Western counter
            counter = this->displayCounters.carsPassing[(int)Compass::West][(int)Direction::Left];
            if(counter > 0) {
                // Need to redraw!
                for(const asciiPosition& asciiPos : util::ATI2_CARS_WEST_PASSING_POS.left) {
                    this->currentAsciiTraffic[asciiPos[0]][asciiPos[1]] = 'L';
                }
            }
            return;
        case Compass::West:
            // Look at Eastern counter!
            counter = this->displayCounters.carsPassing[(int)Compass::East][(int)Direction::Left];
            if(counter > 0) {
                // Need to redraw!
                for(const asciiPosition& asciiPos : util::ATI2_CARS_EAST_PASSING_POS.left) {
                    this->currentAsciiTraffic[asciiPos[0]][asciiPos[1]] = 'L';
                }
            }
            return;
        default:
            throw invalid_argument("Cars can only be located at one of: North, South, East, or West lane!");
    }
}

ostream& operator<<(ostream& out, std::array<string,25>& arr)
{
    for(const string& currStr : arr) {
        out << currStr;
    }

    return out;
}