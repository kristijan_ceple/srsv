//
// Created by kikyy_99 on 15. 10. 2020..
//

#ifndef LAB1_UTIL_H
#define LAB1_UTIL_H

#include <cmath>
#include <string>
#include <mutex>
#include <variant>
#include <condition_variable>
#include <shared_mutex>
#include <queue>
#include <iostream>
#include <vector>

namespace util{
    // ###########################################      GLOBAL STDOUT MONITOR       ####################################
    inline std::mutex my_stdout_mutex;

    // ###########################################       DATA STRUCTURES         ###########################################
    // All time quantities are in milliseconds

    inline bool G_PROGRAM_RUNNING = true;
    inline const std::string DISPLAY_FILE = "./display.log";

    inline const bool ZAGREB_SEMAPHORES_PEDESTRIANS_MODE = true;
    inline const double EPSILON = 1e-6;

    inline const unsigned TIME_PEDESTRIAN_CROSS_MAX = 10000;
    inline const unsigned TIME_CAR_CROSS_MAX = 5000;

    inline const unsigned TIME_GREEN_FULL = 30000;
    inline const unsigned TIME_GREEN_REDUCED = 10000;
    inline const unsigned TIME_PEDESTRIAN_RED_DELTA = 10000;

    /*
     * LAB2 NEW CYCLE CONSTANTS
     */
    inline const unsigned TIME_GREEN_CARS_FW_PDSTR_FULL = 10000;
    inline const unsigned TIME_GREEN_CARS_FW_PDSTR_REDUCED = 5000;
    inline const unsigned TIME_GREEN_CARS_FW_PDSTR_INTERCYCLE = TIME_PEDESTRIAN_CROSS_MAX;      // The value of this
    inline const unsigned TIME_GREEN_CARS_ALL_FULL = 10000;
    inline const unsigned TIME_GREEN_CARS_ALL_REDUCED = 5000;

//    inline const unsigned TIME_PEDESTRIAN_CROSS_MAX = 2000;
//    inline const unsigned TIME_CAR_CROSS_MAX = 1000;
//    inline const unsigned TIME_GREEN_FULL = 6000;
//    inline const unsigned TIME_GREEN_REDUCED = 2000;
//
//    inline const unsigned TIME_PEDESTRIAN_RED_DELTA = 2000;                                                    // Pedestrians' lights turn off before cars by amount of RED DELTA milliseconds
    inline const unsigned TIME_INTERCYCLE_PERIOD = 1.25 * TIME_CAR_CROSS_MAX;

    inline const unsigned TIME_PEDESTRIAN_GREEN_FULL = TIME_GREEN_FULL - TIME_PEDESTRIAN_RED_DELTA;                // How long do pedestrians see green?
    inline const unsigned TIME_PEDESTRIAN_GREEN_REDUCED = TIME_GREEN_REDUCED - TIME_PEDESTRIAN_RED_DELTA;
    inline const bool TIME_PEDESTRIANS_FULL_CROSS_BOOL = TIME_PEDESTRIAN_GREEN_FULL >= TIME_PEDESTRIAN_CROSS_MAX;
    inline const bool TIME_PEDESTRIANS_REDUCED_CROSS_BOOL = TIME_PEDESTRIAN_GREEN_REDUCED >= TIME_PEDESTRIAN_CROSS_MAX;
//    inline const bool TIME_PEDESTRIANS_FULL_CROSS_BOOL = abs((int)TIME_PEDESTRIAN_GREEN_FULL - (int)TIME_PEDESTRIAN_CROSS_MAX) > 0;
//    inline const bool TIME_PEDESTRIANS_REDUCED_CROSS_BOOL = abs((int)TIME_PEDESTRIAN_GREEN_REDUCED - (int)TIME_PEDESTRIAN_CROSS_MAX) > 0;

    inline const unsigned TIME_CREATION_MIN = 250;
    inline const unsigned TIME_CREATION_MAX = 2000;
    inline const unsigned GARBAGE_COLLECTOR_PERIOD = TIME_CREATION_MAX;
//    inline const unsigned DISPLAY_CHECK_PERIOD = std::min(TIME_CREATION_MIN, TIME_GREEN_REDUCED);
//    inline const unsigned DISPLAY_CHECK_PERIOD = 100;
    inline const unsigned QUANTITY_CREATION_MIN = 5;
    inline const unsigned QUANTITY_CREATION_MAX = 30;
    inline const char DROUGHT_CHANCE = 5;

    inline const unsigned HUMAN_REACTION_TIME[] = { 180, 2000 };

    enum class Colour { Green, Yellow, Red };
    enum class Compass {
        North = 0,
        South = 1,
        West = 2,
        East = 3,
        NorthEast = 4,
        NorthWest = 5,
        SouthWest = 6,
        SouthEast = 7
    };

    struct PedestrianCrossing {
        Compass start;
        Compass end;
    };

    enum class Direction { Left = 0, Forward = 1, Right = 2  };
    char Direction2ASCII(Direction dir);

    enum class TrafficEntity { Car, Pedestrian, Controller, Semaphore, Main, Display };
    enum class State {  Waiting, Passing };

    // Red is the default value!
    struct TrafficIntersectionState {
        Colour northSouthCarsForwardColour = Colour::Red;
        Colour northSouthPedestriansColour = Colour::Red;

        Colour eastWestCarsForwardColour = Colour::Red;
        Colour eastWestPedestriansColour = Colour::Red;

        Colour southCarsLeft = Colour::Red;
        Colour southCarsRight = Colour::Red;

        Colour northCarsLeft = Colour::Red;
        Colour northCarsRight = Colour::Red;

        Colour eastCarsLeft = Colour::Red;
        Colour eastCarsRight = Colour::Red;

        Colour westCarsLeft = Colour::Red;
        Colour westCarsRight = Colour::Red;
    };
    inline const TrafficIntersectionState INTERCYCLE_TRAFFIC_INTERSECTION_STATE {};           // All fields are by default set to Red

    struct CarPedestrianDisplayPayload {
        bool arrival;
        bool transfer;
        bool done;
        std::variant<std::pair<Compass, Direction>, PedestrianCrossing> payload;
    };

    struct DisplayMessage {
        TrafficEntity sender;
        std::variant<CarPedestrianDisplayPayload, TrafficIntersectionState> displayPayload;
    };
    // ###########################################       DATA STRUCTURES         ###########################################

    // ###########################################       HELPER FUNCTIONS         ##########################################
    inline void printAtomic(const std::string& toPrint)
    {
        std::lock_guard<std::mutex> lg(util::my_stdout_mutex);
        std::cout << toPrint;
    }
    inline void printlnAtomic(const std::string& toPrint)
    {
        std::lock_guard<std::mutex> lg(util::my_stdout_mutex);
        std::cout << toPrint << std::endl;
    }

    std::string Colour2String(Colour toString);
    Compass Char2Compass(char side);

    std::string Compass2String(Compass toString);
    TrafficEntity Char2TrafficEntity(char num);

    Direction Char2Direction(char num);
    std::string Direction2String(Direction toString);

    [[maybe_unused]] std::string TrafficEntity2String(TrafficEntity toString);

    PedestrianCrossing generatePedestrianCrossing(Compass carLane, char sideOfRoadChar);

    bool carsTakeALook(
            Compass location,
            Direction carDirection,
            std::shared_mutex& semaphoresDisplayMutex,
            TrafficIntersectionState& semaphoresDisplay,
            Colour& colourTemp,
            uint64_t id
            );
    bool pedestriansTakeALook(
            Compass carLane,
            std::shared_mutex& semaphoresDisplayMutex,
            TrafficIntersectionState& semaphoresDisplay,
            Colour& colourTemp,
            uint64_t id
            );
    // ###########################################       HELPER FUNCTIONS         ##########################################

    // #################################             LAB2 INTERSECTION                ##################################

    struct CarsPassingPositions {
        std::vector<std::array<unsigned,2>> left;
        std::vector<std::array<unsigned,2>> forward;
        std::vector<std::array<unsigned,2>> right;

        const std::vector<std::array<unsigned,2>>& operator[](const unsigned index) const;
    };
    struct CarsWaitingPositions {
        std::array<unsigned, 2> left;
        std::array<unsigned, 2> forward;
        std::array<unsigned, 2> right;

        const std::array<unsigned, 2>& operator[](const unsigned index) const;
    };
    using SemaphorePositions = CarsWaitingPositions;
    //    using semaphorePositionsCompass = std::array<asciiPosition,3>;
    using asciiPosition = std::array<unsigned, 2>;
    struct TrafficSemaphorePositions {
        SemaphorePositions ATI2_NORTH_SEMAPHORE_POSITIONS;
        SemaphorePositions ATI2_SOUTH_SEMAPHORE_POSITIONS;
        SemaphorePositions ATI2_WEST_SEMAPHORE_POSITIONS;
        SemaphorePositions ATI2_EAST_SEMAPHORE_POSITIONS;

        const SemaphorePositions& operator[](const util::Compass) const;
    };

    inline CarsPassingPositions FILL_ATI2_CARS_NORTH_PASSING_POS()
    {
        CarsPassingPositions toRet{};

        for(unsigned i = 7; i <= 11; i++) {
            toRet.left.emplace_back(std::array<unsigned,2>{i,26});
        }
        toRet.left.emplace_back(std::array<unsigned,2>{12,28});
        for(unsigned i = 30; i <= 44; i+=2) {
            toRet.left.emplace_back(std::array<unsigned,2>{13, i});
        }


        for(unsigned i = 7; i <= 22; i++) {
            toRet.forward.emplace_back(std::array<unsigned,2>{i, 24});
        }


        toRet.right.emplace_back(std::array<unsigned,2>{7,22});
        toRet.right.emplace_back(std::array<unsigned,2>{8,22});
        toRet.right.emplace_back(std::array<unsigned,2>{9,12});
        toRet.right.emplace_back(std::array<unsigned,2>{9,14});
        toRet.right.emplace_back(std::array<unsigned,2>{9,16});
        toRet.right.emplace_back(std::array<unsigned,2>{9,18});
        toRet.right.emplace_back(std::array<unsigned,2>{9,20});
        toRet.right.emplace_back(std::array<unsigned,2>{9,22});

        return std::move(toRet);
    }
    inline CarsPassingPositions FILL_ATI2_CARS_SOUTH_PASSING_POS()
    {
        CarsPassingPositions toRet{};

        for(unsigned i = 12; i <= 22; i+=2) {
            toRet.left.emplace_back(std::array<unsigned,2>{11,i});
        }
        toRet.left.emplace_back(std::array<unsigned,2>{11,25});
        toRet.left.emplace_back(std::array<unsigned,2>{12,27});
        toRet.left.emplace_back(std::array<unsigned,2>{13,29});
        for(unsigned i = 14; i <= 17; i++) {
            toRet.left.emplace_back(std::array<unsigned,2>{i,30});
        }

        for(unsigned i = 2; i <= 17; i++) {
            toRet.forward.emplace_back(std::array<unsigned,2>{i,32});
        }

        for(unsigned i = 34; i <= 44; i+=2) {
            toRet.right.emplace_back(std::array<unsigned,2>{15,i});
        }
        toRet.right.emplace_back(std::array<unsigned,2>{16,34});
        toRet.right.emplace_back(std::array<unsigned,2>{17,34});

        return std::move(toRet);
    }



    inline const CarsWaitingPositions ATI2_CARS_NORTH_WAITING_POS{
        .left{5,26},
        .forward{5, 24},
        .right{5,22}
    };
    inline const CarsWaitingPositions ATI2_CARS_SOUTH_WAITING_POS{
            .left{19,30},
            .forward{19,32},
            .right{19,34}
    };
    inline const CarsWaitingPositions ATI2_CARS_WEST_WAITING_POS{
            .left{13,16},
            .forward{14,16},
            .right{15,16}
    };
    inline const CarsWaitingPositions ATI2_CARS_EAST_WAITING_POS{
            .left{11,40},
            .forward{10,40},
            .right{9,40}
    };

//    inline const CarsPassingPositions ATI2_CARS_NORTH_PASSING_POS = std::move(FILL_ATI2_CARS_NORTH_PASSING_POS());
//
//    inline const CarsPassingPositions ATI2_CARS_SOUTH_PASSING_POS = std::move(FILL_ATI2_CARS_SOUTH_PASSING_POS());

    inline const CarsPassingPositions ATI2_CARS_NORTH_PASSING_POS{
        .left {
                std::array<unsigned,2>{7,26},
                std::array<unsigned,2>{8,26},
                std::array<unsigned,2>{9,26},
                std::array<unsigned,2>{10,26},
                std::array<unsigned,2>{11,26},
                std::array<unsigned,2>{12,28},
                std::array<unsigned,2>{13,30},
                std::array<unsigned,2>{13,32},
                std::array<unsigned,2>{13,34},
                std::array<unsigned,2>{13,36},
                std::array<unsigned,2>{13,38},
                std::array<unsigned,2>{13,40},
                std::array<unsigned,2>{13,42},
                std::array<unsigned,2>{13,44}
        },
        .forward {
                std::array<unsigned,2>{7,24},
                std::array<unsigned,2>{8,24},
                std::array<unsigned,2>{9,24},
                std::array<unsigned,2>{10,24},
                std::array<unsigned,2>{11,24},
                std::array<unsigned,2>{12,24},
                std::array<unsigned,2>{13,24},
                std::array<unsigned,2>{14,24},
                std::array<unsigned,2>{15,24},
                std::array<unsigned,2>{16,24},
                std::array<unsigned,2>{17,24},
                std::array<unsigned,2>{18,24},
                std::array<unsigned,2>{19,24},
                std::array<unsigned,2>{20,24},
                std::array<unsigned,2>{21,24},
                std::array<unsigned,2>{22,24}
        },
        .right {
                std::array<unsigned,2>{7,22},
                std::array<unsigned,2>{8,22},
                std::array<unsigned,2>{9,12},
                std::array<unsigned,2>{9,14},
                std::array<unsigned,2>{9,16},
                std::array<unsigned,2>{9,18},
                std::array<unsigned,2>{9,20},
                std::array<unsigned,2>{9,22}
        }
    };

    inline const CarsPassingPositions ATI2_CARS_SOUTH_PASSING_POS{
        .left {
                std::array<unsigned,2>{11,12},
                std::array<unsigned,2>{11,14},
                std::array<unsigned,2>{11,16},
                std::array<unsigned,2>{11,18},
                std::array<unsigned,2>{11,20},
                std::array<unsigned,2>{11,22},
                std::array<unsigned,2>{11,24},
                std::array<unsigned,2>{11,26},
                std::array<unsigned,2>{12,28},
                std::array<unsigned,2>{13,29},
                std::array<unsigned,2>{14,30},
                std::array<unsigned,2>{15,30},
                std::array<unsigned,2>{16,30},
                std::array<unsigned,2>{17,30}
        },
        .forward {
                std::array<unsigned,2>{2,32},
                std::array<unsigned,2>{3,32},
                std::array<unsigned,2>{4,32},
                std::array<unsigned,2>{5,32},
                std::array<unsigned,2>{6,32},
                std::array<unsigned,2>{7,32},
                std::array<unsigned,2>{8,32},
                std::array<unsigned,2>{9,32},
                std::array<unsigned,2>{10,32},
                std::array<unsigned,2>{11,32},
                std::array<unsigned,2>{12,32},
                std::array<unsigned,2>{13,32},
                std::array<unsigned,2>{14,32},
                std::array<unsigned,2>{15,32},
                std::array<unsigned,2>{16,32},
                std::array<unsigned,2>{17,32}
        },
        .right {
                std::array<unsigned,2>{15,34},
                std::array<unsigned,2>{15,36},
                std::array<unsigned,2>{15,38},
                std::array<unsigned,2>{15,40},
                std::array<unsigned,2>{15,42},
                std::array<unsigned,2>{15,44},
                std::array<unsigned,2>{16,34},
                std::array<unsigned,2>{17,34}
        }
    };

    inline const CarsPassingPositions ATI2_CARS_WEST_PASSING_POS{
        .left{
                std::array<unsigned,2>{2,30},
                std::array<unsigned,2>{3,30},
                std::array<unsigned,2>{4,30},
                std::array<unsigned,2>{5,30},
                std::array<unsigned,2>{6,30},
                std::array<unsigned,2>{7,30},
                std::array<unsigned,2>{8,30},
                std::array<unsigned,2>{9,30},
                std::array<unsigned,2>{10,30},
                std::array<unsigned,2>{11,30},
                std::array<unsigned,2>{12,28},
                std::array<unsigned,2>{13,20},
                std::array<unsigned,2>{13,22},
                std::array<unsigned,2>{13,24},
                std::array<unsigned,2>{13,26}
        },
        .forward{
                std::array<unsigned,2>{14,20},
                std::array<unsigned,2>{14,22},
                std::array<unsigned,2>{14,24},
                std::array<unsigned,2>{14,26},
                std::array<unsigned,2>{14,28},
                std::array<unsigned,2>{14,30},
                std::array<unsigned,2>{14,32},
                std::array<unsigned,2>{14,34},
                std::array<unsigned,2>{14,36},
                std::array<unsigned,2>{14,38},
                std::array<unsigned,2>{14,40},
                std::array<unsigned,2>{14,42},
                std::array<unsigned,2>{14,44}
        },
        .right{
                std::array<unsigned,2>{15,20},
                std::array<unsigned,2>{15,22},
                std::array<unsigned,2>{16,22},
                std::array<unsigned,2>{17,22},
                std::array<unsigned,2>{18,22},
                std::array<unsigned,2>{19,22},
                std::array<unsigned,2>{20,22},
                std::array<unsigned,2>{21,22},
                std::array<unsigned,2>{22,22}
        }
    };

    inline const CarsPassingPositions ATI2_CARS_EAST_PASSING_POS{
        .left{
                std::array<unsigned,2>{11,30},
                std::array<unsigned,2>{11,32},
                std::array<unsigned,2>{11,34},
                std::array<unsigned,2>{11,36},
                std::array<unsigned,2>{12,28},
                std::array<unsigned,2>{13,26},
                std::array<unsigned,2>{14,26},
                std::array<unsigned,2>{15,26},
                std::array<unsigned,2>{16,26},
                std::array<unsigned,2>{17,26},
                std::array<unsigned,2>{18,26},
                std::array<unsigned,2>{19,26},
                std::array<unsigned,2>{20,26},
                std::array<unsigned,2>{21,26},
                std::array<unsigned,2>{22,26}
        },
        .forward{
                std::array<unsigned,2>{10,12},
                std::array<unsigned,2>{10,14},
                std::array<unsigned,2>{10,16},
                std::array<unsigned,2>{10,18},
                std::array<unsigned,2>{10,20},
                std::array<unsigned,2>{10,22},
                std::array<unsigned,2>{10,24},
                std::array<unsigned,2>{10,26},
                std::array<unsigned,2>{10,28},
                std::array<unsigned,2>{10,30},
                std::array<unsigned,2>{10,32},
                std::array<unsigned,2>{10,34},
                std::array<unsigned,2>{10,36}
        },
        .right{
                std::array<unsigned,2>{2,34},
                std::array<unsigned,2>{3,34},
                std::array<unsigned,2>{4,34},
                std::array<unsigned,2>{5,34},
                std::array<unsigned,2>{6,34},
                std::array<unsigned,2>{7,34},
                std::array<unsigned,2>{8,34},
                std::array<unsigned,2>{9,34},
                std::array<unsigned,2>{9,36}
        }
    };

    inline const std::array<std::array<unsigned,2>, 6> ATI2_PEDESTRIAN_NORTH_ZEBRA_POS{
            std::array<unsigned,2>{6,22},
            std::array<unsigned,2>{6,24},
            std::array<unsigned,2>{6,26},
            std::array<unsigned,2>{6,30},
            std::array<unsigned,2>{6,32},
            std::array<unsigned,2>{6,34}
    };
    inline const std::array<std::array<unsigned,2>, 6> ATI2_PEDESTRIAN_SOUTH_ZEBRA_POS{
            std::array<unsigned,2>{18,22},
            std::array<unsigned,2>{18,24},
            std::array<unsigned,2>{18,26},
            std::array<unsigned,2>{18,30},
            std::array<unsigned,2>{18,32},
            std::array<unsigned,2>{18,34}
    };
    inline const std::array<std::array<unsigned,2>, 6> ATI2_PEDESTRIAN_WEST_ZEBRA_POS{
            std::array<unsigned,2>{9,18},
            std::array<unsigned,2>{10,18},
            std::array<unsigned,2>{11,18},
            std::array<unsigned,2>{13,18},
            std::array<unsigned,2>{14,18},
            std::array<unsigned,2>{15,18}
    };
    inline const std::array<std::array<unsigned,2>, 6> ATI2_PEDESTRIAN_EAST_ZEBRA_POS{
            std::array<unsigned,2>{9,38},
            std::array<unsigned,2>{10,38},
            std::array<unsigned,2>{11,38},
            std::array<unsigned,2>{13,38},
            std::array<unsigned,2>{14,38},
            std::array<unsigned,2>{15,38}
    };

    using row = std::array<asciiPosition, 4>;
    inline const std::array<row, 4> ATI2_PEDESTRIANS_WAITING{
            row{asciiPosition{0, 0}, asciiPosition{6,37}, asciiPosition{0, 0}, asciiPosition{7,38}},
            row{asciiPosition{6,19}, asciiPosition{0, 0}, asciiPosition{7, 18}, asciiPosition{0, 0}},
            row{asciiPosition{0, 0}, asciiPosition{17, 18}, asciiPosition{0, 0}, asciiPosition{18, 19}},
            row{asciiPosition{17,38}, asciiPosition{0, 0}, asciiPosition{18,37}, asciiPosition{0, 0}},
    };

    inline const TrafficSemaphorePositions ATI2_SEMAPHORES_POSITIONS{
      .ATI2_NORTH_SEMAPHORE_POSITIONS {
            .left {8,26},
            .forward {8,24},
            .right {8,22}
      } ,
      .ATI2_SOUTH_SEMAPHORE_POSITIONS {
          .left {16,30},
          .forward {16,32},
          .right {16,34}
      },
      .ATI2_WEST_SEMAPHORE_POSITIONS {
          .left {13,20},
          .forward {14,20},
          .right {15,20}
      },
      .ATI2_EAST_SEMAPHORE_POSITIONS {
          .left {11,36},
          .forward {10,36},
          .right{9,36}
      }
    };

    inline const std::array<std::string, 25> trafficASCIIEmptyLab2 {
            std::string(R"(                         (NORTH)                       )") + "\n",
            std::string(R"(                                                       )") + "\n",
            std::string(R"(                    |       |       |                  )") + "\n",
            std::string(R"(                    |       |       |                  )") + "\n",
            std::string(R"(                    |       |       |                  )") + "\n",
            std::string(R"(                    |       |       |                  )") + "\n",
            std::string(R"(                    |       |       |                  )") + "\n",
            std::string(R"(                    |       |       |                  )") + "\n",
            std::string(R"(            --------+       |       +--------          )") + "\n",
            std::string(R"(                                                       )") + "\n",
            std::string(R"(                                                       )") + "\n",
            std::string(R"(                                                       )") + "\n",
            std::string(R"((WEST)      ---------               ---------    (EAST))") + "\n",
            std::string(R"(                                                       )") + "\n",
            std::string(R"(                                                       )") + "\n",
            std::string(R"(                                                       )") + "\n",
            std::string(R"(            --------+       |       +--------          )") + "\n",
            std::string(R"(                    |       |       |                  )") + "\n",
            std::string(R"(                    |       |       |                  )") + "\n",
            std::string(R"(                    |       |       |                  )") + "\n",
            std::string(R"(                    |       |       |                  )") + "\n",
            std::string(R"(                    |       |       |                  )") + "\n",
            std::string(R"(                    |       |       |                  )") + "\n",
            std::string(R"(                                                       )") + "\n",
            std::string(R"(                         (SOUTH)                       )") + "\n"
    };

    inline std::string trafficASCIIFilledLab2[] {
            std::string(R"(                         (NORTH)                       )") + "\n",
            std::string(R"(                                                       )") + "\n",
            std::string(R"(                    |       | W R D |                  )") + "\n",
            std::string(R"(                    |       | W R D |                  )") + "\n",
            std::string(R"(                    |       | W R D |                  )") + "\n",
            std::string(R"(                    | D R N | W R D |                  )") + "\n",
            std::string(R"(                   p| p p p | p p p |p                 )") + "\n",
            std::string(R"(                  p | D R N | W R D | p                )") + "\n",
            std::string(R"(            --------+ / | \ | W R D +--------          )") + "\n",
            std::string(R"(            D D D p D D R N   W R D \ p D              )") + "\n",
            std::string(R"(            R R R p R R R N R W R R - p R              )") + "\n",
            std::string(R"(            S S S p S S R X W E R E / p E              )") + "\n",
            std::string(R"((WEST)      ---------   R  XX   R   ---------    (EAST))") + "\n",
            std::string(R"(                W p / W R X N X R N N p N N N          )") + "\n",
            std::string(R"(                R p - R R R R R R R R p R R R          )") + "\n",
            std::string(R"(                D p \ D R E   S R D D p D D D          )") + "\n",
            std::string(R"(            --------+ D R E | \ | / +--------          )") + "\n",
            std::string(R"(                  p | D R E | S R D | p                )") + "\n",
            std::string(R"(                   p| p p p | p p p |p                 )") + "\n",
            std::string(R"(                    | D R E | S R D |                  )") + "\n",
            std::string(R"(                    | D R E |       |                  )") + "\n",
            std::string(R"(                    | D R E |       |                  )") + "\n",
            std::string(R"(                    | D R E |       |                  )") + "\n",
            std::string(R"(                                                       )") + "\n",
            std::string(R"(                         (SOUTH)                       )") + "\n"
    };

    Compass zebraFrom2Points(const PedestrianCrossing&);
    // ###########################################       ASCII TRAFFIC INTERSECTION        ##########################################
}

#endif //LAB1_UTIL_H