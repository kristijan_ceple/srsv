cmake_minimum_required(VERSION 3.17)
project(lab2)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")

add_executable(lab2 main.cpp util.h util.cpp Display.cpp Display.h DisplayCountersClassLab2.cpp DisplayCountersClassLab2.h)