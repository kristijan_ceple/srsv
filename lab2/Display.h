//
// Created by kikyy_99 on 18. 10. 2020..
//

#ifndef LAB1_DISPLAY_H
#define LAB1_DISPLAY_H

#include "util.h"
#include "DisplayCountersClassLab2.h"
#include <fstream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>

std::ostream& operator<<(std::ostream& out, std::array<std::string,25>& arr);

class Display {
private:
    std::string fileToWriteTo;

    std::array<std::string, 25> currentAsciiTraffic = util::trafficASCIIEmptyLab2;
    util::TrafficIntersectionState currTrafficState {};             // Initially - all red!

    std::mutex& displayMutex;
    std::queue<util::DisplayMessage>& displayQueue;
    std::condition_variable& displayCv;
    bool& messageReady;

    bool displayRunning = false;
    std::thread displayThread;

    DisplayCountersClassLab2 displayCounters{};

    bool processCarMessageLab2(const util::CarPedestrianDisplayPayload& payload);
    bool processPedestrianMessageLab2(const util::CarPedestrianDisplayPayload& payload);
    void processSemaphoreMessageLab2(const util::TrafficIntersectionState&payload);
    void updateSemaphoresToAsciiDisplay();

    void check2RedrawLeftCounters(util::Compass location);
public:
    Display(
            const std::string& filename,
            std::mutex& displayMutex,
            std::queue<util::DisplayMessage>& displayQueue,
            std::condition_variable& displayCv,
            bool& messageReady
            );

    void run();

    void operator()();
    ~Display();

};


#endif //LAB1_DISPLAY_H
