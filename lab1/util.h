//
// Created by kikyy_99 on 15. 10. 2020..
//

#ifndef LAB1_UTIL_H
#define LAB1_UTIL_H

#include <cmath>
#include <string>
#include <mutex>
#include <variant>

namespace util{
    // ###########################################      GLOBAL STDOUT MONITOR       ####################################
    inline std::mutex my_stdout_mutex;

    // ###########################################       DATA STRUCTURES         ###########################################
    // All time quantities are in milliseconds

    inline bool G_PROGRAM_RUNNING = true;
    inline const std::string DISPLAY_FILE = "./display.log";

    inline const bool ZAGREB_SEMAPHORES_PEDESTRIANS_MODE = true;
    inline const double EPSILON = 1e-6;

  inline const unsigned TIME_PEDESTRIAN_CROSS_MAX = 10000;
  inline const unsigned TIME_CAR_CROSS_MAX = 5000;
  inline const unsigned TIME_GREEN_FULL = 30000;
  inline const unsigned TIME_GREEN_REDUCED = 10000;

  inline const unsigned TIME_PEDESTRIAN_RED_DELTA = 10000;

//    inline const unsigned TIME_PEDESTRIAN_CROSS_MAX = 2000;
//    inline const unsigned TIME_CAR_CROSS_MAX = 1000;
//    inline const unsigned TIME_GREEN_FULL = 6000;
//    inline const unsigned TIME_GREEN_REDUCED = 2000;
//
//    inline const unsigned TIME_PEDESTRIAN_RED_DELTA = 2000;                                                    // Pedestrians' lights turn off before cars by amount of RED DELTA milliseconds
    inline const unsigned TIME_INTERCYCLE_PERIOD = 1.25 * TIME_CAR_CROSS_MAX;

    inline const unsigned TIME_PEDESTRIAN_GREEN_FULL = TIME_GREEN_FULL - TIME_PEDESTRIAN_RED_DELTA;                // How long do pedestrians see green?
    inline const unsigned TIME_PEDESTRIAN_GREEN_REDUCED = TIME_GREEN_REDUCED - TIME_PEDESTRIAN_RED_DELTA;
    inline const bool TIME_PEDESTRIANS_FULL_CROSS_BOOL = TIME_PEDESTRIAN_GREEN_FULL >= TIME_PEDESTRIAN_CROSS_MAX;
    inline const bool TIME_PEDESTRIANS_REDUCED_CROSS_BOOL = TIME_PEDESTRIAN_GREEN_REDUCED >= TIME_PEDESTRIAN_CROSS_MAX;
//    inline const bool TIME_PEDESTRIANS_FULL_CROSS_BOOL = abs((int)TIME_PEDESTRIAN_GREEN_FULL - (int)TIME_PEDESTRIAN_CROSS_MAX) > 0;
//    inline const bool TIME_PEDESTRIANS_REDUCED_CROSS_BOOL = abs((int)TIME_PEDESTRIAN_GREEN_REDUCED - (int)TIME_PEDESTRIAN_CROSS_MAX) > 0;

    inline const unsigned TIME_CREATION_MIN = 500;
    inline const unsigned TIME_CREATION_MAX = 3000;
    inline const unsigned GARBAGE_COLLECTOR_PERIOD = TIME_CREATION_MAX;
//    inline const unsigned DISPLAY_CHECK_PERIOD = std::min(TIME_CREATION_MIN, TIME_GREEN_REDUCED);
//    inline const unsigned DISPLAY_CHECK_PERIOD = 100;
    inline const unsigned QUANTITY_CREATION_MIN = 0;
    inline const unsigned QUANTITY_CREATION_MAX = 20;
    inline const char DROUGHT_CHANCE = 5;

    inline const unsigned HUMAN_REACTION_TIME[] = { 180, 2000 };

    enum class Colour { Green, Yellow, Red };
    enum class Compass { North, South, East, West, NorthWest, NorthEast, SouthWest, SouthEast };

    struct PedestrianCrossing {
        Compass start;
        Compass end;
    };

    enum class TrafficEntity { Car, Pedestrian, Controller, Semaphore, Main, Display };
    enum class State {  Waiting, Passing };

    struct TrafficIntersectionState {
        Colour northSouthCarsColour;
        Colour northSouthPedestriansColour;
        Colour eastWestCarsColour;
        Colour eastWestPedestriansColour;
    };
    inline const TrafficIntersectionState INTERCYCLE_TRAFFIC_INTERSECTION_STATE = {
            Colour::Red,
            Colour::Red,
            Colour::Red,
            Colour::Red
    };

    struct CarPedestrianDisplayPayload {
        bool arrival;
        bool transfer;
        bool done;
        std::variant<Compass, PedestrianCrossing> payload;
    };

    struct DisplayMessage {
        TrafficEntity sender;
        std::variant<CarPedestrianDisplayPayload, TrafficIntersectionState> displayPayload;
    };
    // ###########################################       DATA STRUCTURES         ###########################################

    // ###########################################       HELPER FUNCTIONS         ##########################################
    std::string Colour2String(Colour toString);
    Compass Char2Compass(char side);

    std::string Compass2String(Compass toString);
    TrafficEntity Char2TrafficEntity(char num);

    [[maybe_unused]] std::string TrafficEntity2String(TrafficEntity toString);

    PedestrianCrossing generatePedestrianCrossing(Compass carLane, char sideOfRoadChar);
    // ###########################################       HELPER FUNCTIONS         ##########################################


    // ###########################################       ASCII TRAFFIC INTERSECTION        ##########################################
    inline const unsigned ATI_COLS = 48;
    inline const unsigned ATI_ROWS = 21;
    inline const std::array<unsigned, 12> ATI_EAST_WEST_CARS_PASSING_POS = {451, 453, 455, 457, 459, 461,
                                                        547, 549, 551, 553, 555, 557};
    inline const std::array<unsigned, 14> ATI_NORTH_SOUTH_CARS_PASSING_POS = {358, 406, 454, 502, 550, 598, 646,
                                                          362, 410, 458, 506, 554, 602, 650};
    inline const unsigned ATI_CAR_NORTH_WAITING_POS = 262;
    inline const unsigned ATI_CAR_SOUTH_WAITING_POS = 746;
    inline const unsigned ATI_CAR_EAST_WAITING_POS = 464;
    inline const unsigned ATI_CAR_WEST_WAITING_POS = 544;

    inline const unsigned ATI_PEDESTRIAN_NORTH_WEST_WAITING_POS = 354;
    inline const unsigned ATI_PEDESTRIAN_NORTH_EAST_WAITING_POS = 366;
    inline const unsigned ATI_PEDESTRIAN_SOUTH_WEST_WAITING_POS = 642;
    inline const unsigned ATI_PEDESTRIAN_SOUTH_EAST_WAITING_POS = 654;
    inline const unsigned ATI_PEDESTRIAN_WEST_NORTH_WAITING_POS = 307;
    inline const unsigned ATI_PEDESTRIAN_WEST_SOUTH_WAITING_POS = 691;
    inline const unsigned ATI_PEDESTRIAN_EAST_NORTH_WAITING_POS = 317;
    inline const unsigned ATI_PEDESTRIAN_EAST_SOUTH_WAITING_POS = 701;

    inline const std::pair<unsigned, unsigned> ATI_PEDESTRIAN_NORTH_ZEBRA_POS_PAIR = {310, 314};
    inline const std::pair<unsigned, unsigned> ATI_PEDESTRIAN_SOUTH_ZEBRA_POS_PAIR = {694, 698};
    inline const std::pair<unsigned, unsigned> ATI_PEDESTRIAN_WEST_ZEBRA_POS_PAIR = {450, 546};
    inline const std::pair<unsigned, unsigned> ATI_PEDESTRIAN_EAST_ZEBRA_POS_PAIR = {462, 558};

    inline const std::array<unsigned, 2> ATI_PEDESTRIAN_NORTH_ZEBRA_POS = {310, 314};
    inline const std::array<unsigned, 2> ATI_PEDESTRIAN_SOUTH_ZEBRA_POS = {694, 698};
    inline const std::array<unsigned, 2> ATI_PEDESTRIAN_WEST_ZEBRA_POS = {450, 546};
    inline const std::array<unsigned, 2> ATI_PEDESTRIAN_EAST_ZEBRA_POS = {462, 558};

    inline std::string trafficASCIIFilled =
            "                     (NORTH)                   \n"
            "                                               \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "                    | A |   |                  \n"
            "                   p| p | p |p                 \n"
            "                  p | A | A | p                \n"
            "            --------+ A | A +--------          \n"
            "                  pA AAA AAA Ap A              \n"
            "(WEST)      --------- A   A ---------    (EAST)\n"
            "                A pA AAA AAA Ap                \n"
            "            --------+ A | A +--------          \n"
            "                  p | A | A | p                \n"
            "                   p| p | p |p                 \n"
            "                    |   | A |                  \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "                                               \n"
            "                     (SOUTH)                   \n";

    inline std::string trafficASCIIEmpty =
            "                     (NORTH)                   \n"
            "                                               \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "            --------+   |   +--------          \n"
            "                                               \n"
            "(WEST)      ---------       ---------    (EAST)\n"
            "                                               \n"
            "            --------+   |   +--------          \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "                    |   |   |                  \n"
            "                                               \n"
            "                     (SOUTH)                   \n";


    struct DisplayCounters {
        // Cars
        unsigned carsWaitingNorth;
        unsigned carsWaitingSouth;
        unsigned carsWaitingEast;
        unsigned carsWaitingWest;

        unsigned carsPassingNorthSouth;
        unsigned carsPassingEastWest;



        // Pedestrians
        unsigned pedestriansWaitingNorthWest;
        unsigned pedestriansWaitingNorthEast;
        unsigned pedestriansWaitingSouthWest;
        unsigned pedestriansWaitingSouthEast;

        unsigned pedestriansWaitingWestNorth;
        unsigned pedestriansWaitingWestSouth;
        unsigned pedestriansWaitingEastNorth;
        unsigned pedestriansWaitingEastSouth;


        unsigned pedestriansPassingNorth;
        unsigned pedestriansPassingSouth;
        unsigned pedestriansPassingEast;
        unsigned pedestriansPassingWest;
    };

    Compass zebraFrom2Points(PedestrianCrossing&);
    // ###########################################       ASCII TRAFFIC INTERSECTION        ##########################################
}

#endif //LAB1_UTIL_H

/*
 * Keeping code backed up here just in case for future use
 */
//    struct PedestriansPassingCounters {
//            unsigned northCnt;
//            unsigned southCnt;
//            unsigned eastCnt;
//            unsigned westCnt;
//    };
//
//    struct CarsPassingCounters {
//        unsigned
//    };

//    struct DisplayCounter {
//        TrafficCounters carsWaitingCounters;
//        TrafficCounters carsPassingCounters;
//        TrafficCounters pedestriansWaitingCounters;
//        TrafficCounters pedestriansPassingCounters;

//    };


/*
 * These constants are obsolete, and have been replaced by more readable and clearer ZEBRA constants
 */
//inline unsigned ATI_PEDESTRIAN_NORTH_WEST_PASSING_POS[] = {450, 546};
//
//inline unsigned ATI_PEDESTRIAN_NORTH_EAST_PASSING_POS[] = {462, 558};
//
//inline unsigned ATI_PEDESTRIAN_SOUTH_WEST_PASSING_POS[] = {450, 546};
//
//inline unsigned ATI_PEDESTRIAN_SOUTH_EAST_PASSING_POS[] = {462, 558};
//
//inline unsigned ATI_PEDESTRIAN_WEST_NORTH_PASSING_POS[] = {310, 314};
//
//inline unsigned ATI_PEDESTRIAN_WEST_SOUTH_PASSING_POS[] = {694, 698};
//
//inline unsigned ATI_PEDESTRIAN_EAST_NORTH_PASSING_POS[] = {310, 314};
//
//inline unsigned ATI_PEDESTRIAN_EAST_SOUTH_PASSING_POS[] = {694, 698};