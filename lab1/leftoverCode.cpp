//
// Created by kikyy_99 on 15. 10. 2020..
//

// ###########################################          OBSOLETE            ############################################
// ###########################################          OBSOLETE            ############################################
// ###########################################          OBSOLETE            ############################################

// ###########################################       DATA STRUCTURES         ###########################################


class AbstractSemaphore {
private:
    string name;
    long id;

public:
    string getName()
    {
        return name;
    }

    long getId()
    {
        return id;
    }

    virtual ~AbstractSemaphore() =0;
};

struct CarSemaphore: AbstractSemaphore {

};

struct PedestrianSemaphore: AbstractSemaphore {

};

struct Lane {
private:
    vector<shared_ptr<CarSemaphore>>& carSemaphores;
    vector<shared_ptr<PedestrianSemaphore>>& pedestrianSemaphores;
public:
    Lane();
};

// ###########################################       DATA STRUCTURES         ###########################################
// ###########################################          OBSOLETE            ############################################