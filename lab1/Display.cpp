//
// Created by kikyy_99 on 18. 10. 2020..
//

#include "Display.h"
#include "util.h"
#include <iostream>
#include <variant>
using namespace std;
using namespace util;

Display::Display(
        const std::string& filename,
        std::mutex& displayMutex,
        std::queue<util::DisplayMessage>& displayQueue,
        std::condition_variable& displayCv,
        bool& messageReady
        )
    : fileToWriteTo{filename},
    displayMutex{displayMutex}, displayCv{displayCv}, displayQueue {displayQueue}, messageReady{messageReady}
    {}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
void Display::run()
{
    // Now need to launch myself into a separate thread
    this->displayThread = thread(
            ref(*this)
            );
    this->displayRunning = true;
}
#pragma clang diagnostic pop


#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
void Display::operator()() {
    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        cout << "[DISPLAY] Display starting up..." << endl;
        cout << this->currentAsciiTraffic << endl;
    }

//    // Open the file for writing output into
//    ofstream outFile;
//    outFile.open(fileToWriteTo, ios::out);

    DisplayMessage tmpDisplayMessage;
    bool retBool = false;
    while(true) {
        {
            unique_lock<mutex> ul(displayMutex);
            if(displayQueue.empty()) {
                // Wait - but only if the queue is empty!
                displayCv.wait(ul, [this]() {
                    return (this->messageReady);
                });

                // A weird SIGSEGV bug can happen here if the queue isn't updated fast enough
                if(displayQueue.empty()) {
                    continue;
                }

                messageReady = false;       // Lower the flag so signal that we've popped the message
            }

            tmpDisplayMessage = displayQueue.front();
            this->displayQueue.pop();
        }

        // Now let's copy the value into the local cache, and then free the mutex
        try {
            switch(tmpDisplayMessage.sender) {
                case TrafficEntity::Car:
//                outFile << "Car sent a message!" << endl;
                    retBool = processCarMessageImproved(
                            get<CarPedestrianDisplayPayload>(tmpDisplayMessage.displayPayload)
                    );

                    if(retBool) {
                        // Now print the ascii!
                        lock_guard<mutex> lg(util::my_stdout_mutex);
                        cout << this->currentAsciiTraffic << endl;
                    }

                    break;
                case TrafficEntity::Pedestrian:
//                outFile << "Pedestrian sent a message!" << endl;
                    retBool = processPedestrianMessageImproved(
                            get<CarPedestrianDisplayPayload>(tmpDisplayMessage.displayPayload)
                    );

                    if(retBool) {
                        // Now print the ascii!
                        lock_guard<mutex> lg(util::my_stdout_mutex);
                        cout << this->currentAsciiTraffic << endl;
                    }

                    break;
                case TrafficEntity::Semaphore:
//                outFile << "Semaphore sent a message!" << endl;
                    processSemaphoreMessage(
                            get<TrafficIntersectionState>(tmpDisplayMessage.displayPayload)
                    );

//                    {
//                        cout << "[DISPLAY] Received a Display Message from the Semaphore!" << endl;
//                        lock_guard<mutex> lg(util::my_stdout_mutex);
//                    }

                    break;
                default:
                    throw invalid_argument("DisplayMessage handling for this sender is currently not available!");
            }
        } catch(std::bad_variant_access& e) {
            cerr << e.what() << endl;
            exit(-1);
        }


    }
}
// Old code from the above function - keeping it for the future in case I decide to improve the program
//        // Now wait until we have been notified and a message is ready
//        displayCv->wait(ul, [this]() {
//            return *(this->messageReady);
//        });
//        *messageReady = false;       // Resetting the value
#pragma clang diagnostic pop

Display::~Display()
{
    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        cout << "[MAIN] Display destructor called..." << endl;
        cout << "[MAIN] Joining Display..." << endl;
    }
    displayThread.join();
    this->displayRunning = false;
}

bool Display::processCarMessageImproved(const util::CarPedestrianDisplayPayload& payload)
{
    Compass carLocation = get<Compass>(payload.payload);

    bool displayChanged = false;
    if(payload.arrival) {
        // Other 2 must be false!
        if(payload.transfer || payload.done) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }


        auto ret = (*std::get<DisplayCountersClass::InnerHolderClass::DataCoreCars*>
                (this->displayCountersClass[util::TrafficEntity::Car][util::State::Waiting]))[carLocation];
        unsigned& counterWaiting = ret.first;
        unsigned position = std::get<unsigned>(ret.second);

        counterWaiting++;
        if(counterWaiting == 1) {
            // Turn the display on!
            this->currentAsciiTraffic[position] = 'A';
            displayChanged=true;
        }
    } else if(payload.transfer) {
        // Other 2 must be false!
        if(payload.arrival || payload.done) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        auto retWaiting = (*std::get<DisplayCountersClass::InnerHolderClass::DataCoreCars*>
                (this->displayCountersClass[util::TrafficEntity::Car][util::State::Waiting]))[carLocation];
        auto retPassing = (*std::get<DisplayCountersClass::InnerHolderClass::DataCoreCars*>
                (this->displayCountersClass[util::TrafficEntity::Car][util::State::Passing]))[carLocation];
        unsigned& counterWaiting = retWaiting.first;
        unsigned position = std::get<unsigned>(retWaiting.second);
        counterWaiting--;
        if(counterWaiting == 0) {
            this->currentAsciiTraffic[position] = ' ';
            displayChanged = true;
        }

        unsigned& counterPassing = retPassing.first;
        counterPassing++;

        bool holdsArray12 = std::holds_alternative<
                    std::reference_wrapper<const array<unsigned, 12>>
                >(retPassing.second);
        if(holdsArray12) {
            const array<unsigned, 12>& arr = std::get<std::reference_wrapper<const array<unsigned, 12>>>(retPassing.second);

            if(counterPassing == 1) {
                for(unsigned num : arr) {
                    this->currentAsciiTraffic[num] = 'A';
                }

                displayChanged = true;
            }
        } else {
            const array<unsigned, 14>& arr = std::get<std::reference_wrapper<const array<unsigned, 14>>>(retPassing.second);

            if(counterPassing == 1) {
                for(unsigned num : arr) {
                    this->currentAsciiTraffic[num] = 'A';
                }

                displayChanged = true;
            }
        }
    } else if(payload.done) {
        // Other 2 must be false!
        if(payload.arrival || payload.transfer) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        auto retPassing = (*std::get<DisplayCountersClass::InnerHolderClass::DataCoreCars*>
                (this->displayCountersClass[util::TrafficEntity::Car][util::State::Passing]))[carLocation];
        unsigned& counterPassing = retPassing.first;
        counterPassing--;

        bool holdsArray12 = std::holds_alternative<
                std::reference_wrapper<const array<unsigned, 12>>
        >(retPassing.second);
        if(holdsArray12) {
            const array<unsigned, 12>& arr = std::get<std::reference_wrapper<const array<unsigned, 12>>>(retPassing.second);

            if(counterPassing == 0) {
                for(unsigned num : arr) {
                    this->currentAsciiTraffic[num] = ' ';
                }

                displayChanged = true;
            }
        } else {
            const array<unsigned, 14>& arr = std::get<std::reference_wrapper<const array<unsigned, 14>>>(retPassing.second);

            if(counterPassing == 0) {
                for(unsigned num : arr) {
                    this->currentAsciiTraffic[num] = ' ';
                }

                displayChanged = true;
            }
        }
    } else {
        throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
    }

    return displayChanged;
}

bool Display::processPedestrianMessageImproved(const util::CarPedestrianDisplayPayload& payload)
{
    PedestrianCrossing pedestrianCrossing = get<PedestrianCrossing>(payload.payload);
    bool displayChanged = false;

    if(payload.arrival) {
        //      ########################################################################################################
        //      #########################################       ARRIVAL        #########################################
        //      ########################################################################################################
        // The other 2 must not be true!
        if(payload.transfer || payload.done) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        auto retWaiting = (*std::get<DisplayCountersClass::InnerHolderClass::DataCorePedestrians*>
                (this->displayCountersClass[util::TrafficEntity::Pedestrian][util::State::Waiting]))[pedestrianCrossing];
        unsigned& waitingCounter = retWaiting.first;
        const unsigned location = get<unsigned>(retWaiting.second);

        waitingCounter++;
        if(waitingCounter == 1) {
            this->currentAsciiTraffic[location] = 'p';
            displayChanged = true;
        }
    } else if(payload.transfer) {
        //      ########################################################################################################
        //      #########################################       TRANSFER        ########################################
        //      ########################################################################################################
        // Check that everything else is false!
        if(payload.arrival || payload.done) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        auto retWaiting = (*std::get<DisplayCountersClass::InnerHolderClass::DataCorePedestrians*>
                (this->displayCountersClass[util::TrafficEntity::Pedestrian][util::State::Waiting]))[pedestrianCrossing];
        unsigned& waitingCounter = retWaiting.first;
        const unsigned location = get<unsigned>(retWaiting.second);

        waitingCounter--;
        if(waitingCounter == 0) {
            this->currentAsciiTraffic[location] = ' ';
            displayChanged = true;
        }

        auto retPassing = (*std::get<DisplayCountersClass::InnerHolderClass::DataCorePedestrians*>
                (this->displayCountersClass[util::TrafficEntity::Pedestrian][util::State::Passing]))[pedestrianCrossing];
        unsigned& passingCounter = retPassing.first;
        const pair<unsigned, unsigned>& zebra = std::get<reference_wrapper<const std::pair<unsigned, unsigned>>>(retPassing.second);
        passingCounter++;
        if(passingCounter == 1) {
            this->currentAsciiTraffic[zebra.first] = 'p';
            this->currentAsciiTraffic[zebra.second] = 'p';
            displayChanged = true;
        }
    } else if(payload.done) {
        //      ########################################################################################################
        //      #########################################       DONE        ############################################
        //      ########################################################################################################

        // Check that everything else is false!
        if(payload.arrival || payload.transfer) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        auto retPassing = (*std::get<DisplayCountersClass::InnerHolderClass::DataCorePedestrians*>
                (this->displayCountersClass[util::TrafficEntity::Pedestrian][util::State::Passing]))[pedestrianCrossing];
        unsigned& passingCounter = retPassing.first;
        const pair<unsigned, unsigned>& zebra = std::get<reference_wrapper<const std::pair<unsigned, unsigned>>>(retPassing.second);
        passingCounter--;
        if(passingCounter == 0) {
            this->currentAsciiTraffic[zebra.first] = ' ';
            this->currentAsciiTraffic[zebra.second] = ' ';
            displayChanged = true;
        }

    } else {
        throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
    }

    return displayChanged;
}

//      ################################################################################################################
//      #########################################       OLD AND MISC CODE        #######################################
//      ################################################################################################################

bool Display::processCarMessage(const util::CarPedestrianDisplayPayload& payload)
{
    Compass carLocation = get<Compass>(payload.payload);

    bool displayChanged = false;

    // Received the payload - first let's check what is active
    if(payload.arrival) {
        // Check that everything else is false!
        if(payload.transfer || payload.done) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        // Increment the counter!
        // Turn on the car arrival in the ASCII!
        switch(carLocation) {
            case Compass::North:
                this->displayCounters.carsWaitingNorth++;

                if(this->displayCounters.carsWaitingNorth == 1) {
                    this->currentAsciiTraffic[ATI_CAR_NORTH_WAITING_POS] = 'A';
                    displayChanged = true;
                }

                break;
            case Compass::South:
                this->displayCounters.carsWaitingSouth++;

                if(this->displayCounters.carsWaitingSouth == 1) {
                    this->currentAsciiTraffic[ATI_CAR_SOUTH_WAITING_POS] = 'A';
                    displayChanged = true;

                }

                break;
            case Compass::East:
                this->displayCounters.carsWaitingEast++;

                if(this->displayCounters.carsWaitingEast == 1) {
                    this->currentAsciiTraffic[ATI_CAR_EAST_WAITING_POS] = 'A';
                    displayChanged = true;
                }

                break;
            case Compass::West:
                this->displayCounters.carsWaitingWest++;

                if(this->displayCounters.carsWaitingWest == 1) {
                    this->currentAsciiTraffic[ATI_CAR_WEST_WAITING_POS] = 'A';
                    displayChanged = true;
                }

                break;
            default:
                throw invalid_argument("Invalid car location!");
        }
    } else if(payload.transfer) {
        // Check that everything else is false!
        if(payload.arrival || payload.done) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        switch(carLocation) {
            case Compass::North:
                this->displayCounters.carsPassingNorthSouth++;

                this->displayCounters.carsWaitingNorth--;
                if(this->displayCounters.carsWaitingNorth == 0) {
                    // Remove the A from that spot
                    this->currentAsciiTraffic[ATI_CAR_NORTH_WAITING_POS] = ' ';
                    displayChanged = true;
                }

                // Draw the car passing if not already drawn
                if (this->displayCounters.carsPassingNorthSouth == 1) {
                    for(unsigned num : util::ATI_NORTH_SOUTH_CARS_PASSING_POS) {
                        this->currentAsciiTraffic[num] = 'A';
                    }

                    displayChanged = true;
                }

                break;
            case Compass::South:
                this->displayCounters.carsPassingNorthSouth++;

                this->displayCounters.carsWaitingSouth--;
                if(this->displayCounters.carsWaitingSouth == 0) {
                    // Remove the A from that spot
                    this->currentAsciiTraffic[ATI_CAR_SOUTH_WAITING_POS] = ' ';
                    displayChanged = true;
                }

                // Draw the car passing if not already drawn
                if (this->displayCounters.carsPassingNorthSouth == 1) {
                    for(unsigned num : util::ATI_NORTH_SOUTH_CARS_PASSING_POS) {
                        this->currentAsciiTraffic[num] = 'A';
                    }

                    displayChanged = true;
                }

                break;
            case Compass::East:
                this->displayCounters.carsPassingEastWest++;

                this->displayCounters.carsWaitingEast--;
                if(this->displayCounters.carsWaitingEast == 0) {
                    // Remove the A from that spot
                    this->currentAsciiTraffic[ATI_CAR_EAST_WAITING_POS] = ' ';
                    displayChanged = true;
                }

                // Draw the car passing if not already drawn
                if (this->displayCounters.carsPassingEastWest == 1) {
                    for(unsigned num : util::ATI_EAST_WEST_CARS_PASSING_POS) {
                        this->currentAsciiTraffic[num] = 'A';
                    }

                    displayChanged = true;
                }

                break;
            case Compass::West:
                this->displayCounters.carsPassingEastWest++;

                this->displayCounters.carsWaitingWest--;
                if(this->displayCounters.carsWaitingWest == 0) {
                    // Remove the A from that spot
                    this->currentAsciiTraffic[ATI_CAR_WEST_WAITING_POS] = ' ';
                    displayChanged = true;
                }

                // Draw the car passing if not already drawn
                if (this->displayCounters.carsPassingEastWest == 1) {
                    for(unsigned num : util::ATI_EAST_WEST_CARS_PASSING_POS) {
                        this->currentAsciiTraffic[num] = 'A';
                    }

                    displayChanged = true;
                }

                break;
            default:
                throw invalid_argument("Invalid car location!");
        }
    } else if (payload.done) {
        // Check that everything else is false!
        if(payload.arrival || payload.transfer) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        // Remove the car from the counter!
        switch(carLocation) {
            case Compass::North:
            case Compass::South:
                this->displayCounters.carsPassingNorthSouth--;

                if(this->displayCounters.carsPassingNorthSouth == 0) {
                    for(unsigned num : util::ATI_NORTH_SOUTH_CARS_PASSING_POS) {
                        this->currentAsciiTraffic[num] = ' ';
                    }

                    displayChanged = true;
                }

                break;
            case Compass::East:
            case Compass::West:
                this->displayCounters.carsPassingEastWest--;

                if(this->displayCounters.carsPassingEastWest == 0) {
                    for(unsigned num : util::ATI_EAST_WEST_CARS_PASSING_POS) {
                        this->currentAsciiTraffic[num] = ' ';
                    }

                    displayChanged = true;
                }

                break;
            default:
                throw invalid_argument("Invalid car location!");
        }
    } else {
        throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
    }

    return displayChanged;
}

/*
 * For pedestrians we need the major lane that they belong to,
 * and their starting point.
 *
 * From the above 2 we can deduce the end point as well, and therefore the
 * according Zebra! - there are 4 of them
 */
bool Display::processPedestrianMessage(const util::CarPedestrianDisplayPayload& payload)
{
    PedestrianCrossing pedestrianCrossing = get<PedestrianCrossing>(payload.payload);
    bool displayChanged = false;

    if(payload.arrival) {
        //      ########################################################################################################
        //      #########################################       ARRIVAL        #########################################
        //      ########################################################################################################
        // The other 2 must not be true!
        if(payload.transfer || payload.done) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        // 4 x 2 = 8 possibilities - oh this is gonna be fun
        switch(pedestrianCrossing.start) {
            case Compass::NorthWest:
                // From NW we can go into 2 directions
                switch (pedestrianCrossing.end) {
                    case Compass::NorthEast:
                        displayCounters.pedestriansWaitingWestNorth++;
                        if (displayCounters.pedestriansWaitingWestNorth == 1) {
                            // First pedestrian here, need to draw him!
                            this->currentAsciiTraffic[util::ATI_PEDESTRIAN_WEST_NORTH_WAITING_POS] = 'p';
                            displayChanged = true;
                        }

                        break;
                    case Compass::SouthWest:
                        displayCounters.pedestriansWaitingNorthWest++;

                        if (displayCounters.pedestriansWaitingNorthWest == 1) {
                            this->currentAsciiTraffic[util::ATI_PEDESTRIAN_NORTH_WEST_WAITING_POS] = 'p';
                            displayChanged = true;
                        }

                        break;
                    default:
                        throw logic_error("Impossible start-end pair combination!");
                }

                break;
            case Compass::NorthEast:
                switch (pedestrianCrossing.end) {
                    case Compass::NorthWest:
                        displayCounters.pedestriansWaitingEastNorth++;

                        if (displayCounters.pedestriansWaitingEastNorth == 1) {
                            this->currentAsciiTraffic[ATI_PEDESTRIAN_EAST_NORTH_WAITING_POS] = 'p';
                            displayChanged = true;
                        }

                        break;
                    case Compass::SouthEast:
                        displayCounters.pedestriansWaitingNorthEast++;

                        if (displayCounters.pedestriansWaitingNorthEast == 1) {
                            this->currentAsciiTraffic[ATI_PEDESTRIAN_NORTH_EAST_WAITING_POS] = 'p';
                            displayChanged = true;
                        }

                        break;
                    default:
                        throw logic_error("Impossible start-end pair combination!");
                }

                break;
            case Compass::SouthWest:
                switch (pedestrianCrossing.end) {
                    case Compass::NorthWest:
                        displayCounters.pedestriansWaitingSouthWest++;

                        if (displayCounters.pedestriansWaitingSouthWest == 1) {
                            this->currentAsciiTraffic[ATI_PEDESTRIAN_SOUTH_WEST_WAITING_POS] = 'p';
                            displayChanged = true;
                        }

                        break;
                    case Compass::SouthEast:
                        displayCounters.pedestriansWaitingWestSouth++;

                        if (displayCounters.pedestriansWaitingWestSouth == 1) {
                            this->currentAsciiTraffic[ATI_PEDESTRIAN_WEST_SOUTH_WAITING_POS] = 'p';
                            displayChanged = true;
                        }

                        break;
                    default:
                        throw logic_error("Impossible start-end pair combination!");
                }

                break;
            case Compass::SouthEast:
                switch (pedestrianCrossing.end) {
                    case Compass::SouthWest:
                        displayCounters.pedestriansWaitingEastSouth++;

                        if (displayCounters.pedestriansWaitingEastSouth == 1) {
                            this->currentAsciiTraffic[ATI_PEDESTRIAN_EAST_SOUTH_WAITING_POS] = 'p';
                            displayChanged = true;
                        }

                        break;
                    case Compass::NorthEast:
                        displayCounters.pedestriansWaitingSouthEast++;

                        if (displayCounters.pedestriansWaitingSouthEast == 1) {
                            this->currentAsciiTraffic[ATI_PEDESTRIAN_SOUTH_EAST_WAITING_POS] = 'p';
                            displayChanged = true;
                        }

                        break;
                    default:
                        throw logic_error("Impossible start-end pair combination!");
                }

                break;
            default:
                throw logic_error("Impossible start point!");
        }
    } else if(payload.transfer) {
        //      ########################################################################################################
        //      #########################################       TRANSFER        ########################################
        //      ########################################################################################################
        // Check that everything else is false!
        if(payload.arrival || payload.done) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        // 4 x 2 = 8 possibilities - oh this is gonna be fun
        switch(pedestrianCrossing.start) {
            case Compass::NorthWest:
                // From NW we can go into 2 directions
                switch(pedestrianCrossing.end) {
                    case Compass::NorthEast:
                        displayCounters.pedestriansWaitingWestNorth--;
                        if(displayCounters.pedestriansWaitingWestNorth == 0) {
                            // All the pedestrians left this zebra, need to erase them!
                            this->currentAsciiTraffic[util::ATI_PEDESTRIAN_WEST_NORTH_WAITING_POS] = ' ';
                            displayChanged = true;
                        }

                        // North Zebra
                        // Need to increment the Zebra counters
                        displayCounters.pedestriansPassingNorth++;
                        if(displayCounters.pedestriansPassingNorth == 1) {
                            // First passing - draw him!
                            for(unsigned num : ATI_PEDESTRIAN_NORTH_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = 'p';
                            }

                            displayChanged = true;
                        }

                        break;
                    case Compass::SouthWest:
                        displayCounters.pedestriansWaitingNorthWest--;

                        if(displayCounters.pedestriansWaitingNorthWest == 0) {
                            this->currentAsciiTraffic[util::ATI_PEDESTRIAN_NORTH_WEST_WAITING_POS] = ' ';
                            displayChanged = true;
                        }

                        // West Zebra
                        // Need to increment the Zebra counters
                        displayCounters.pedestriansPassingWest++;
                        if(displayCounters.pedestriansPassingWest == 1) {
                          for(unsigned num : ATI_PEDESTRIAN_WEST_ZEBRA_POS) {
                              this->currentAsciiTraffic[num] = 'p';
                          }

                            displayChanged = true;
                        }

                        break;
                    default:
                        throw logic_error("Impossible start-end pair combination!");
                }

                break;
            case Compass::NorthEast:
                switch(pedestrianCrossing.end) {
                    case Compass::NorthWest:
                        displayCounters.pedestriansWaitingEastNorth--;

                        if(displayCounters.pedestriansWaitingEastNorth == 0) {
                            this->currentAsciiTraffic[ATI_PEDESTRIAN_EAST_NORTH_WAITING_POS] = ' ';
                            displayChanged = true;
                        }

                        // North Zebra
                        displayCounters.pedestriansPassingNorth++;
                        if(displayCounters.pedestriansPassingNorth == 1) {
                            for(unsigned num : ATI_PEDESTRIAN_NORTH_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = 'p';
                            }

                            displayChanged = true;
                        }

                        break;
                    case Compass::SouthEast:
                        displayCounters.pedestriansWaitingNorthEast--;

                        if(displayCounters.pedestriansWaitingNorthEast == 0) {
                            this->currentAsciiTraffic[ATI_PEDESTRIAN_NORTH_EAST_WAITING_POS] = ' ';
                            displayChanged = true;
                        }

                        // East Zebra
                        displayCounters.pedestriansPassingEast++;
                        if(displayCounters.pedestriansPassingEast == 1) {
                            for(unsigned num : ATI_PEDESTRIAN_EAST_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = 'p';
                            }

                            displayChanged = true;
                        }

                        break;
                    default:
                        throw logic_error("Impossible start-end pair combination!");
                }

                break;
            case Compass::SouthWest:
                switch(pedestrianCrossing.end) {
                    case Compass::NorthWest:
                        displayCounters.pedestriansWaitingSouthWest--;

                        if(displayCounters.pedestriansWaitingSouthWest == 0) {
                            this->currentAsciiTraffic[ATI_PEDESTRIAN_SOUTH_WEST_WAITING_POS] = ' ';
                            displayChanged = true;
                        }

                        // West Zebra
                        displayCounters.pedestriansPassingWest++;
                        if(displayCounters.pedestriansPassingWest == 1) {
                            for(unsigned num : ATI_PEDESTRIAN_WEST_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = 'p';
                            }

                            displayChanged = true;
                        }

                        break;
                    case Compass::SouthEast:
                        displayCounters.pedestriansWaitingWestSouth--;

                        if(displayCounters.pedestriansWaitingWestSouth == 0) {
                            this->currentAsciiTraffic[ATI_PEDESTRIAN_WEST_SOUTH_WAITING_POS] = ' ';
                            displayChanged = true;
                        }

                        // South Zebra
                        displayCounters.pedestriansPassingSouth++;
                        if(displayCounters.pedestriansPassingSouth == 1) {
                            for(unsigned num : ATI_PEDESTRIAN_SOUTH_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = 'p';
                            }

                            displayChanged = true;
                        }

                        break;
                    default:
                        throw logic_error("Impossible start-end pair combination!");
                }

                break;
            case Compass::SouthEast:
                switch(pedestrianCrossing.end) {
                    case Compass::SouthWest:
                        displayCounters.pedestriansWaitingEastSouth--;

                        if(displayCounters.pedestriansWaitingEastSouth== 0) {
                            this->currentAsciiTraffic[ATI_PEDESTRIAN_EAST_SOUTH_WAITING_POS] = ' ';
                            displayChanged = true;
                        }

                        // South Zebra
                        displayCounters.pedestriansPassingSouth++;
                        if(displayCounters.pedestriansPassingSouth == 1) {
                            for(unsigned num : ATI_PEDESTRIAN_SOUTH_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = 'p';
                            }

                            displayChanged = true;
                        }

                        break;
                    case Compass::NorthEast:
                        displayCounters.pedestriansWaitingSouthEast--;

                        if(displayCounters.pedestriansWaitingSouthEast == 0) {
                            this->currentAsciiTraffic[ATI_PEDESTRIAN_SOUTH_EAST_WAITING_POS] = ' ';
                            displayChanged = true;
                        }

                        // East Zebra
                        displayCounters.pedestriansPassingEast++;
                        if(displayCounters.pedestriansPassingEast == 1) {
                            for(unsigned num : ATI_PEDESTRIAN_EAST_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = 'p';
                            }

                            displayChanged = true;
                        }

                        break;
                    default:
                        throw logic_error("Impossible start-end pair combination!");
                }

                break;
            default:
                throw logic_error("Impossible start point!");
        }
    } else if(payload.done) {
        //      ########################################################################################################
        //      #########################################       DONE        ############################################
        //      ########################################################################################################

        // Check that everything else is false!
        if(payload.arrival || payload.transfer) {
            throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
        }

        // 4 x 2 = 8 possibilities - oh this is gonna be fun
        switch(pedestrianCrossing.start) {
            case Compass::NorthWest:
                // From NW we can go into 2 directions
                switch(pedestrianCrossing.end) {
                    case Compass::NorthEast:
                        // North Zebra
                        displayCounters.pedestriansPassingNorth--;
                        if(displayCounters.pedestriansPassingNorth == 0) {
                            // Turn off the drawing for this char
                            for(unsigned num : ATI_PEDESTRIAN_NORTH_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = ' ';
                            }

                            displayChanged = true;
                        }

                        break;
                    case Compass::SouthWest:
                        // West Zebra
                        displayCounters.pedestriansPassingWest--;
                        if(displayCounters.pedestriansPassingWest == 0) {
                            for(unsigned num : ATI_PEDESTRIAN_WEST_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = ' ';
                            }

                            displayChanged = true;
                        }

                        break;
                    default:
                        throw logic_error("Impossible start-end pair combination!");
                }

                break;
            case Compass::NorthEast:
                switch(pedestrianCrossing.end) {
                    case Compass::NorthWest:
                        // North Zebra
                        displayCounters.pedestriansPassingNorth--;
                        if(displayCounters.pedestriansPassingNorth == 0) {
                            // Turn off the drawing for this char
                            for(unsigned num : ATI_PEDESTRIAN_NORTH_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = ' ';
                            }

                            displayChanged = true;
                        }

                        break;
                    case Compass::SouthEast:
                        // East Zebra
                        displayCounters.pedestriansPassingEast--;
                        if(displayCounters.pedestriansPassingEast == 0) {
                            // Turn off the drawing for this char
                            for(unsigned num : ATI_PEDESTRIAN_EAST_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = ' ';
                            }

                            displayChanged = true;
                        }

                        break;
                    default:
                        throw logic_error("Impossible start-end pair combination!");
                }

                break;
            case Compass::SouthWest:
                switch(pedestrianCrossing.end) {
                    case Compass::NorthWest:
                        // West Zebra
                        displayCounters.pedestriansPassingWest--;
                        if(displayCounters.pedestriansPassingWest == 0) {
                            for(unsigned num : ATI_PEDESTRIAN_WEST_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = ' ';
                            }

                            displayChanged = true;
                        }

                        break;
                    case Compass::SouthEast:
                        // South Zebra
                        displayCounters.pedestriansPassingSouth--;
                        if(displayCounters.pedestriansPassingSouth == 0) {
                            for(unsigned num : ATI_PEDESTRIAN_SOUTH_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = ' ';
                            }

                            displayChanged = true;
                        }

                        break;
                    default:
                        throw logic_error("Impossible start-end pair combination!");
                }

                break;
            case Compass::SouthEast:
                switch(pedestrianCrossing.end) {
                    case Compass::SouthWest:
                        // South Zebra
                        displayCounters.pedestriansPassingSouth--;
                        if(displayCounters.pedestriansPassingSouth == 0) {
                            for(unsigned num : ATI_PEDESTRIAN_SOUTH_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = ' ';
                            }

                            displayChanged = true;
                        }

                        break;
                    case Compass::NorthEast:
                        // East Zebra
                        displayCounters.pedestriansPassingEast--;
                        if(displayCounters.pedestriansPassingEast == 0) {
                            // Turn off the drawing for this char
                            for(unsigned num : ATI_PEDESTRIAN_EAST_ZEBRA_POS) {
                                this->currentAsciiTraffic[num] = ' ';
                            }

                            displayChanged = true;
                        }

                        break;
                    default:
                        throw logic_error("Impossible start-end pair combination!");
                }

                break;
            default:
                throw logic_error("Impossible start point!");
        }
    } else {
        throw invalid_argument("Exactly one boolean in the CarPedestrianDisplayPayload must be true!");
    }

    return displayChanged;
}

bool Display::processSemaphoreMessage(const util::TrafficIntersectionState& payload)
{
    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        // Process data here - stdout inside mutex to avoid chat mixing
        cout << "[DISPLAY] Changing lights! Current state: " << endl;
        printf("\tNorth South Cars: %s\n", Colour2String(payload.northSouthCarsColour).c_str());
        printf("\tNorth South Pedestrians: %s\n", Colour2String(payload.northSouthPedestriansColour).c_str());
        printf("\tEast West Cars: %s\n", Colour2String(payload.eastWestCarsColour).c_str());
        printf("\tEast West Pedestrians: %s\n", Colour2String(payload.eastWestPedestriansColour).c_str());
    }
}

void Display::asciiTest1(ofstream& outFile)
{
    string localAscii = util::trafficASCIIFilled;

    // Now reset all the positions and check if it works!
    localAscii[262] = ' ';
    localAscii[746] = ' ';
    localAscii[464] = ' ';
    localAscii[544] = ' ';

    outFile << localAscii << endl << endl;
}