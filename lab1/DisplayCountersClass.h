//
// Created by kikyy_99 on 21. 10. 2020..
//

#ifndef LAB1_DISPLAYCOUNTERSCLASS_H
#define LAB1_DISPLAYCOUNTERSCLASS_H

#include <functional>
#include "util.h"
/*
 * We want to have automatic choosing based on enums.
 * First useful enum is the Traffic Entity enum
 * The second one is Waiting/Passing
 * The final and third one will be:
 *      a) Compass - for Cars!
 *      b) PedestrianCrossing - for Pedestrians!
 */


class DisplayCountersClass {
friend class Display;
private:
    class InnerHolderClass {
        friend class Display;
    protected:
        InnerHolderClass() = default;
        class DataCoreCars {
        public:
            virtual std::pair<
                    unsigned&,
                    std::variant<
                            unsigned,
                            std::reference_wrapper<const std::array<unsigned, 12>>,
                            std::reference_wrapper<const std::array<unsigned, 14>>,
                            std::reference_wrapper<const std::pair<unsigned, unsigned>>
                    >
            > operator[](util::Compass index) =0;
        };

        class DataCorePedestrians {
        public:
            virtual std::pair<
                    unsigned&,
                    std::variant<
                            unsigned,
                            std::reference_wrapper<const std::array<unsigned, 12>>,
                            std::reference_wrapper<const std::array<unsigned, 14>>,
                            std::reference_wrapper<const std::pair<unsigned, unsigned>>
                    >
            > operator[](util::PedestrianCrossing index) =0;
        };

        class DataCoreCarsWaiting : public DataCoreCars {
        private:
            unsigned data[4]{};
        public:
            std::pair<
                    unsigned&,
                    std::variant<
                            unsigned,
                            std::reference_wrapper<const std::array<unsigned, 12>>,
                            std::reference_wrapper<const std::array<unsigned, 14>>,
                            std::reference_wrapper<const std::pair<unsigned, unsigned>>
                    >
            > operator[](util::Compass index) override {
                switch(index) {
                    case util::Compass::North:
                        return { data[0], util::ATI_CAR_NORTH_WAITING_POS};
                    case util::Compass::South:
                        return {data[1], util::ATI_CAR_SOUTH_WAITING_POS};
                    case util::Compass::East:
                        return {data[2], util::ATI_CAR_EAST_WAITING_POS};
                    case util::Compass::West:
                        return {data[3], util::ATI_CAR_WEST_WAITING_POS};
                    default:
                        throw std::invalid_argument("Indexing not possible for the passed argument!");
                }
            }
        };

        class DataCoreCarsPassing : public DataCoreCars {
        private:
            unsigned data[2]{};
        public:
            std::pair<
                    unsigned&,
                    std::variant<
                            unsigned,
                            std::reference_wrapper<const std::array<unsigned, 12>>,
                            std::reference_wrapper<const std::array<unsigned, 14>>,
                            std::reference_wrapper<const std::pair<unsigned, unsigned>>
                    >
            > operator[](util::Compass index) override {
                switch(index) {
                    case util::Compass::North:
                    case util::Compass::South:
                        return { data[0], util::ATI_NORTH_SOUTH_CARS_PASSING_POS };
                    case util::Compass::East:
                    case util::Compass::West:
                        return {data[1], util::ATI_EAST_WEST_CARS_PASSING_POS };
                    default:
                        throw std::invalid_argument("Indexing not possible for the passed argument!");
                }
            }
        };

        class DataCorePedestriansWaiting : public DataCorePedestrians {
        private:
            unsigned data[8]{};
        public:
            std::pair<
                    unsigned&,
                    std::variant<
                            unsigned,
                            std::reference_wrapper<const std::array<unsigned, 12>>,
                            std::reference_wrapper<const std::array<unsigned, 14>>,
                            std::reference_wrapper<const std::pair<unsigned, unsigned>>
                    >
            > operator[](util::PedestrianCrossing index) override;
        };

        class DataCorePedestriansPassing : public DataCorePedestrians {
        private:
            unsigned data[4]{};
        public:
            std::pair<
                    unsigned&,
                    std::variant<
                            unsigned,
                            std::reference_wrapper<const std::array<unsigned, 12>>,
                            std::reference_wrapper<const std::array<unsigned, 14>>,
                            std::reference_wrapper<const std::pair<unsigned, unsigned>>
                    >
            > operator[](util::PedestrianCrossing index) override;
        };
    public:
        virtual std::variant<DataCoreCars*,DataCorePedestrians*> operator[](util::State waitOrPass) =0;
    };

    class InnerHolderClassCars : InnerHolderClass {
        friend class DisplayCountersClass;
    private:
        DataCoreCarsWaiting dataCoreCarsWaiting;
        DataCoreCarsPassing dataCoreCarsPassing;
        InnerHolderClassCars() = default;
    public:
        std::variant<DataCoreCars*,DataCorePedestrians*> operator[](util::State waitOrPass) override;
    };

    class InnerHolderClassPedestrians : InnerHolderClass {
        friend class DisplayCountersClass;
    private:
        DataCorePedestriansWaiting dataCorePedestriansWaiting;
        DataCorePedestriansPassing dataCorePedestriansPassing;
        InnerHolderClassPedestrians() = default;
    public:
        std::variant<DataCoreCars*,DataCorePedestrians*> operator[](util::State waitOrPass) override;
    };

    InnerHolderClassCars innerHolderClassCars;
    InnerHolderClassPedestrians innerHolderClassPedestrians;

public:
    DisplayCountersClass() = default;
    InnerHolderClass& operator [](util::TrafficEntity te);
};

#endif //LAB1_DISPLAYCOUNTERSCLASS_H