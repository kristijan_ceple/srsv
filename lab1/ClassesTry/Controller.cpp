//
// Created by kikyy_99 on 15. 10. 2020..
//

#include "Controller.h"
using namespace std;

Controller::Controller(std::shared_mutex &semaphoresDisplaySharedMutex,
                       util::TrafficIntersectionState &semaphoresDisplay, std::mutex &metalDetectorsMutex,
                       std::queue<util::Compass> &metalDetectorsPositionsQueue, std::mutex &pressersMutex,
                       std::queue<util::Compass> &pressersPositionsQueue)
                       :
        semaphoresDisplaySharedMutex {semaphoresDisplaySharedMutex},
        semaphoresDisplay {semaphoresDisplay},
        metalDetectorsMutex {metalDetectorsMutex},
        metalDetectorsPositionsQueue{metalDetectorsPositionsQueue},
        pressersMutex {pressersMutex},
        pressersPositionsQueue{pressersPositionsQueue}
        {}

void Controller::run() {
    // Launch the thread

}

void Controller::operator()() {

}
