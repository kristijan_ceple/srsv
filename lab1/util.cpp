//
// Created by kikyy_99 on 15. 10. 2020..
//

#include "util.h"
#include <iostream>
using namespace std;
using namespace util;

std::string util::Colour2String(util::Colour toString) {
    switch(toString) {
        case Colour::Green:
            return "Green";
        case Colour::Yellow:
            return "Yellow";
        case Colour::Red:
            return "Red";
        default:
            throw invalid_argument("String returning for this Colour has not been implemented yet!");
    }
}

Compass util::Char2Compass(char side)
{
    switch(side) {
        case 0:
            return Compass::North;
        case 1:
            return Compass::South;
        case 2:
            return Compass::East;
        case 3:
            return Compass::West;
        default:
            throw invalid_argument("Compass returning for this char has not been implemented yet!");
    }
}

string util::Compass2String(Compass toString)
{
    switch(toString) {
        case Compass::North:
            return "North";
        case Compass::South:
            return "South";
        case Compass::East:
            return "East";
        case Compass::West:
            return "West";
        case Compass::NorthWest:
            return "North-West";
        case Compass::NorthEast:
            return "North-East";
        case Compass::SouthWest:
            return "South-West";
        case Compass::SouthEast:
            return "South-East";
        default:
            throw invalid_argument("String returning for this Compass has not been implemented yet!");
    }
}

TrafficEntity util::Char2TrafficEntity(char num)
{
    switch(num) {
        case 0:
            return TrafficEntity::Car;
        case 1:
            return TrafficEntity::Pedestrian;
        default:
            throw invalid_argument("TrafficEntity returning for this char has not been implemented yet!");
    }
}

[[maybe_unused]] string util::TrafficEntity2String(TrafficEntity toString)
{
    switch(toString) {
        case TrafficEntity::Car:
            return "Car";
        case TrafficEntity::Pedestrian:
            return "Pedestrian";
        default:
            throw invalid_argument("String returning for this TrafficEntity has not been implemented yet!");
    }
}

//Compass char2Compass(char toConvert)
//{
//    switch(toConvert) {
//        case 0:
//            return Compass::NorthEast;
//        case 1:
//            return Compass::NorthWest;
//        case 2:
//            return Compass::SouthWest;
//        case 3:
//            return Compass::SouthEast;
//        default:
//            throw invalid_argument("Compass returning for this char has not been implemented yet!");
//    }
//}

PedestrianCrossing util::generatePedestrianCrossing(Compass carLane, char sideOfRoadChar)
{
    /*
     * Maybe the picture would be easier for this one, I'm gonna upload it to git
     * A matrix explaining the situation would look like:
     *  -       -
     * |   1   0  |
     * |   0   1  |
     *  -       -
     * The decoding rule anti-clockwise:
     *      North:
     *          0 - NE
     *          1 - NW
     *      South:
     *          0 - SW
     *          1 - SE
     *      West:
     *          1 - NW
     *          0 - SW
     *      East:
     *          1 - SE
     *          0 - ME
     */
    PedestrianCrossing toRet{};
    switch(carLane) {
        case Compass::North:
            if(sideOfRoadChar == 0) {
                toRet.start = Compass::NorthEast;
                toRet.end = Compass::SouthEast;
            } else {
                toRet.start = Compass::NorthWest;
                toRet.end = Compass::SouthWest;
            }
            break;
        case Compass::South:
            if(sideOfRoadChar == 0) {
                toRet.start = Compass::SouthWest;
                toRet.end = Compass::NorthWest;
            } else {
                toRet.start = Compass::SouthEast;
                toRet.end = Compass::NorthEast;
            }
            break;
        case Compass::East:
            if(sideOfRoadChar == 1) {
                toRet.start = Compass::SouthEast;
                toRet.end = Compass::SouthWest;
            } else {
                toRet.start = Compass::NorthEast;
                toRet.end = Compass::NorthWest;
            }
            break;
        case Compass::West:
            if(sideOfRoadChar == 1) {
                toRet.start = Compass::NorthWest;
                toRet.end = Compass::NorthEast;
            } else {
                toRet.start = Compass::SouthWest;
                toRet.end = Compass::SouthEast;
            }
            break;
        default:
            throw invalid_argument("Pedestrians can only be assigned to 2 major car lanes: North/South or East/West!");
    }

    return toRet;
}