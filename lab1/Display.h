//
// Created by kikyy_99 on 18. 10. 2020..
//

#ifndef LAB1_DISPLAY_H
#define LAB1_DISPLAY_H

#include "util.h"
#include "DisplayCountersClass.h"
#include <fstream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>

class Display {
private:
    std::string fileToWriteTo;

    std::string currentAsciiTraffic = util::trafficASCIIEmpty;

    std::mutex& displayMutex;
    std::queue<util::DisplayMessage>& displayQueue;
    std::condition_variable& displayCv;
    bool& messageReady;

    bool displayRunning = false;
    std::thread displayThread;

    bool processCarMessage(const util::CarPedestrianDisplayPayload& payload);
    bool processPedestrianMessage(const util::CarPedestrianDisplayPayload& payload);
    bool processSemaphoreMessage(const util::TrafficIntersectionState&payload);

    bool processCarMessageImproved(const util::CarPedestrianDisplayPayload& payload);
    bool processPedestrianMessageImproved(const util::CarPedestrianDisplayPayload& payload);

    util::DisplayCounters displayCounters;
    DisplayCountersClass displayCountersClass;

    void asciiTest1(std::ofstream& outFile);
public:
    Display(
            const std::string& filename,
            std::mutex& displayMutex,
            std::queue<util::DisplayMessage>& displayQueue,
            std::condition_variable& displayCv,
            bool& messageReady
            );

    void run();

    void operator()();
    ~Display();
};


#endif //LAB1_DISPLAY_H
