#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <shared_mutex>
#include <condition_variable>
#include <random>
#include <queue>
#include <csignal>
#include "util.h"
#include "Display.h"

// Platform-independent sleep function!!
#ifdef WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

using namespace std;
using namespace util;

[[noreturn]] void semaphore(
        mutex& m, condition_variable& cv,
        bool& changeHappened,
        TrafficIntersectionState& trafficIntersectionState,
        shared_mutex& semaphoreDisplaySharedMutex,
        TrafficIntersectionState& semaphoreDisplayPorts,
        mutex& displayMutex,
        queue<DisplayMessage>& displayQueue,
        condition_variable& displayCv,
        bool& displayMessage
        )
{
    {
        // Cout inside lock_guard in order for the stdout messages to be in orderly and timely manner
        lock_guard<mutex> lg(m);
        cout << "[SEM] Semaphore revving up..." << endl;
    }

    TrafficIntersectionState trafficIntersectionStateLocal{};

    while(G_PROGRAM_RUNNING) {
        /*
         * Semaphore just waits until a change has happened, and once the change happens it reads the value from
         * the shared variable and updates its own variables(these variables are then given to cars and pedestrians)
         */

        {
            // Monitor section
            unique_lock<mutex> ul(m);                                               // We're in critical section so lock it down
            cv.wait(ul,
                    [&changeHappened]() { return changeHappened || !G_PROGRAM_RUNNING; });        // Wait until a change has happened - the wait() function is called inside a critical section because wait unlocks/locks the mutex depending on whether the thread is blocked or released. WHen the thread is released, it has to check a SHARED variable to check against spurious waits, so that's another reason.
            trafficIntersectionStateLocal = trafficIntersectionState;                               // Copy the data to the semaphore
            changeHappened = false;                                                 // Basically tell the controller that the SEM has acknowledged the data
        }

//        {
//            lock_guard<mutex> lg(util::my_stdout_mutex);
//            // Process data here - stdout inside mutex to avoid chat mixing
//            cout << "[SEM] Changing lights! Current state: " << endl;
//            printf("\tNorth South Cars: %s\n", Colour2String(trafficIntersectionStateLocal.northSouthCarsColour).c_str());
//            printf("\tNorth South Pedestrians: %s\n", Colour2String(trafficIntersectionStateLocal.northSouthPedestriansColour).c_str());
//            printf("\tEast West Cars: %s\n", Colour2String(trafficIntersectionStateLocal.eastWestCarsColour).c_str());
//            printf("\tEast West Pedestrians: %s\n", Colour2String(trafficIntersectionStateLocal.eastWestPedestriansColour).c_str());
//        }

        cv.notify_one();                                                        // Send the notification to the controller that the semaphore is done! Basically unblock it!

        {
            lock_guard<shared_mutex> lg(semaphoreDisplaySharedMutex);
            semaphoreDisplayPorts = trafficIntersectionStateLocal;
        }

        // Now tell the display to update its colours as well
        {
            lock_guard<mutex> lg(displayMutex);
            displayQueue.emplace(
                    DisplayMessage {
                        .sender = TrafficEntity::Semaphore,
                        .displayPayload = trafficIntersectionStateLocal
                    });
            displayMessage = true;
        }
        displayCv.notify_one();
    }


    cv.notify_one();
    {
        lock_guard<mutex> lg(my_stdout_mutex);
        cout << "[SEM] Semaphore turning off due to feirunt..." << endl;
    }
}

void car(
        Compass location,
        shared_mutex& semaphoresDisplayMutex,
        TrafficIntersectionState& semaphoresDisplay,
        mutex& metalDetectorsMutex,
        queue<Compass>& metalDetectorsQueue,
        mt19937_64& engine,
        uniform_int_distribution<unsigned>& humanVisualReactionRandom,
        uint64_t id,
        mutex& displayMutex,
        queue<DisplayMessage>& displayQueue,
        condition_variable& displayCv,
        bool& displayMessage
        )
{
    // Tell the Display that we've arrived!
//    CarPedestrianDisplayPayload carPedestrianDisplayPayload ;
    {
        lock_guard<mutex> lg(displayMutex);
        displayQueue.emplace(
                DisplayMessage {
                        .sender = TrafficEntity::Car,
                        .displayPayload = CarPedestrianDisplayPayload {
                                .arrival =  true,
                                .transfer = false,
                                .done = false,
                                .payload = location
                        }
                });
        displayMessage = true;
    }
    displayCv.notify_one();

    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        printf(
                "[CAR %lu @ %s] I have arrived to the intersection!\n",
                id,
                Compass2String(location).c_str()
        );
    }

    // For starters the Car has to be detected by the metal detectors which send the info to the Controller
    {
        lock_guard<mutex> lg(metalDetectorsMutex);
        metalDetectorsQueue.emplace(location);
    }

    Colour colourTemp;
    auto takeALook =
            [
                &location,
                &semaphoresDisplayMutex,
                &semaphoresDisplay,
                &colourTemp,
                id
            ]
            ()
            -> bool
            {
                /*
                * Depending on this car's location, it will have to look at different ports
                */
                switch(location) {
                    case Compass::North:
                    case Compass::South:
                        // Needs to look at the Car North-South Semaphore Port!
                    {
                        shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                        colourTemp = semaphoresDisplay.northSouthCarsColour;
                    }
                        break;
                    case Compass::East:
                    case Compass::West:
                        // Needs to look at the Car East-West Semaphore Port!
                    {
                        shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                        colourTemp = semaphoresDisplay.eastWestCarsColour;
                    }
                        break;
                    default:
                        throw invalid_argument("Cars can only be located at: North, South, East, or West!");
                }

//                {
//                    lock_guard<mutex> lg(util::my_stdout_mutex);
//                    printf("[CAR %lu @ %s] My corresponding semaphore colour: %s\n",
//                           id,
//                           Compass2String(location).c_str(),
//                           Colour2String(colourTemp).c_str()
//                    );
//                }

                return (colourTemp == Colour::Green);
            };

    bool canPass;
    while(G_PROGRAM_RUNNING) {
        /*
         * The car driver needs to take a look at the traffic lights in random intervals. Should the light be green,
         * they need to pass the zebra.
         */
        this_thread::sleep_for(chrono::milliseconds(humanVisualReactionRandom(engine)));

        canPass = takeALook();

        if(canPass){
            {
                lock_guard<mutex> lg(displayMutex);
                displayQueue.emplace(
                        DisplayMessage {
                                .sender = TrafficEntity::Car,
                                .displayPayload = CarPedestrianDisplayPayload {
                                        .arrival =  false,
                                        .transfer = true,
                                        .done = false,
                                        .payload = location
                                }
                        });
                displayMessage = true;
            }
            displayCv.notify_one();
            // Now we must cross the Traffic intersection!
            this_thread::sleep_for(chrono::milliseconds(TIME_CAR_CROSS_MAX));
            {
                lock_guard<mutex> lg(displayMutex);
                displayQueue.emplace(
                        DisplayMessage {
                                .sender = TrafficEntity::Car,
                                .displayPayload = CarPedestrianDisplayPayload {
                                        .arrival =  false,
                                        .transfer = false,
                                        .done = true,
                                        .payload = location
                                }
                        });
                displayMessage = true;
            }
            displayCv.notify_one();
            {
                lock_guard<mutex> lg(util::my_stdout_mutex);
                printf(
                        "[CAR %lu @ %s] I've crossed the intersection while my corresponding semaphore colour was %s!\n",
                        id,
                        Compass2String(location).c_str(),
                        Colour2String(colourTemp).c_str()
                );
            }
            return;
        }
    }

    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        printf(
                "[CAR %lu @ %s] leaving because feirunt...\n",
                id,
                Compass2String(location).c_str()
        );
    }
}

void pedestrian(
        Compass carLane,
        PedestrianCrossing pedestrianCrossing,
        shared_mutex& semaphoresDisplayMutex,
        TrafficIntersectionState& semaphoresDisplay,
        mutex& pressersMutex,
        queue<Compass>& pressersPositionsQueue,
        mt19937_64& engine,
        uniform_int_distribution<unsigned>& humanVisualReactionRandom,
        uint64_t id,
        mutex& displayMutex,
        queue<DisplayMessage>& displayQueue,
        condition_variable& displayCv,
        bool& displayMessage
        )
{
    // Tell the Display that we've arrived!
//    CarPedestrianDisplayPayload carPedestrianDisplayPayload ;
    {
        lock_guard<mutex> lg(displayMutex);
        displayQueue.emplace(
                DisplayMessage {
                        .sender = TrafficEntity::Pedestrian,
                        .displayPayload = CarPedestrianDisplayPayload {
                                .arrival =  true,
                                .transfer = false,
                                .done = false,
                                .payload = pedestrianCrossing
                        }
                });
        displayMessage = true;
    }
    displayCv.notify_one();

    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        printf(
                "[PEDESTRIAN %lu @ major lane %s] I have arrived to the intersection at %s and want to go to: %s!\n",
                id,
                Compass2String(carLane).c_str(),
                Compass2String(pedestrianCrossing.start).c_str(),
                Compass2String(pedestrianCrossing.end).c_str()
        );
    }

    // For starters, the Pedestrian presses the button on the small yellow box beneath the semaphore -- this signals to the Controller that the Pedestrian is waiting
    {
        lock_guard<mutex> lg(pressersMutex);
        pressersPositionsQueue.emplace(carLane);
    }

    Colour colourTemp;
    auto takeALook =
            [
                &carLane,
                &semaphoresDisplayMutex,
                &semaphoresDisplay,
                &colourTemp,
                id
            ]
            ()
            -> bool
            {
                /*
                * Depending on this pedestrian's location, it will have to look at different ports
                */
                switch(carLane) {
                    case Compass::North:
                    case Compass::South:
                        // Needs to look at the Pedestrian North-South Semaphore Port!
                    {
                        shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                        colourTemp = semaphoresDisplay.northSouthPedestriansColour;
                    }
                        break;
                    case Compass::East:
                    case Compass::West:
                        // Needs to look at the Pedestrian East-West Semaphore Port!
                    {
                        shared_lock<shared_mutex> sl(semaphoresDisplayMutex);
                        colourTemp = semaphoresDisplay.eastWestPedestriansColour;
                    }
                        break;
                    default:
                        throw invalid_argument("Car/Major Lanes are only: North, South, East, and West");
                }

//                {
//                    lock_guard<mutex> lg(util::my_stdout_mutex);
//                    printf("[PEDESTRIAN %lu @ %s] My corresponding semaphore colour: %s\n",
//                           id,
//                           Compass2String(location).c_str(),
//                           Colour2String(colourTemp).c_str()
//                    );
//                }

                return (colourTemp == Colour::Green);
            };

    bool canPass;
    while(G_PROGRAM_RUNNING) {
        /*
         * The pedestrian needs to take a look at the traffic lights in random intervals. Should the light be green,
         * they need to pass the zebra.
         */
        this_thread::sleep_for(chrono::milliseconds(humanVisualReactionRandom(engine)));

        canPass = takeALook();

        if(canPass) {
            {
                lock_guard<mutex> lg(displayMutex);
                displayQueue.emplace(
                        DisplayMessage {
                                .sender = TrafficEntity::Pedestrian,
                                .displayPayload = CarPedestrianDisplayPayload {
                                        .arrival =  false,
                                        .transfer = true,
                                        .done = false,
                                        .payload = pedestrianCrossing
                                }
                        });
                displayMessage = true;
            }
            displayCv.notify_one();

            // Simulate walking by sleeping
            this_thread::sleep_for(chrono::milliseconds(TIME_PEDESTRIAN_CROSS_MAX));

            {
                lock_guard<mutex> lg(displayMutex);
                displayQueue.emplace(
                        DisplayMessage {
                                .sender = TrafficEntity::Pedestrian,
                                .displayPayload = CarPedestrianDisplayPayload {
                                        .arrival =  false,
                                        .transfer = false,
                                        .done = true,
                                        .payload = pedestrianCrossing
                                }
                        });
                displayMessage = true;
            }
            displayCv.notify_one();

            {
                lock_guard<mutex> lg(util::my_stdout_mutex);
                printf(
                        "[PEDESTRIAN %lu @ %s] I've crossed the zebra while my corresponding semaphore colour was %s!\n",
                        id,
                        Compass2String(carLane).c_str(),
                        Colour2String(colourTemp).c_str()
                );
            }
            return;
        }
    }

    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        printf(
                "[PEDESTRIAN %lu @ %s] leaving because feirunt...\n",
                id,
                Compass2String(carLane).c_str()
        );
    }
}

/*
 * Controller needs to regulate the periods and the states!
 *
 * It is important to note, that before the Controller sleeps it needs to check whether there were any Cars or
 * Pedestrians detected by the metal detectors or pressers! While processing the queue it keeps track of the count of
 * these requests, and then depending on the quantity sleeps different amounts of time!
 */
[[noreturn]] void controller(
        shared_mutex& semaphoreDisplaySharedMutex,
        TrafficIntersectionState& semaphoreDisplay,
        mutex& metalDetectorsMutex,
        queue<Compass>& metalDetectorsPositionsQueue,
        mutex& pressersMutex,
        queue<Compass>& pressersPositionQueue,
        mutex& displayMutex,
        queue<DisplayMessage>& displayQueue,
        condition_variable& displayCv,
        bool& displayMessage
        )
{
    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        cout << "[CONTROLLER] Starting simulation..." << endl;
        cout << "[CONTROLLER] Controller thread starting..." << endl;
        cout << "[CONTROLLER] Initialising the intersection..." << endl << endl;
//        cout << "[CONTROLLER] Debug info:" << endl;
//        printf("TIME_PEDESTRIAN_GREEN_FULL = %u\n",
//               TIME_PEDESTRIAN_GREEN_FULL);
//        printf("TIME_PEDESTRIAN_GREEN_REDUCED = %u\n",
//               TIME_PEDESTRIAN_GREEN_REDUCED);
//        printf("TIME_PEDESTRIANS_FULL_CROSS_BOOL = %s\n",
//               TIME_PEDESTRIANS_FULL_CROSS_BOOL ? "true" : "false");
//        printf("TIME_PEDESTRIANS_REDUCED_CROSS_BOOL = %s\n",
//               TIME_PEDESTRIANS_REDUCED_CROSS_BOOL ? "true" : "false");
    }

    // Start the semaphore thread, pass the monitor to it
    // ###################################          MONITOR         ####################################################
    mutex controller_sem_mutex;
    condition_variable controller_sem_cv;
    bool changeHappened;
    TrafficIntersectionState trafficIntersectionState
            { Colour::Green,
              Colour::Green,
              Colour::Red,
              Colour::Red
            };

    auto updateSemaphore = [
            &controller_sem_mutex,
            &controller_sem_cv,
            &changeHappened,
            &trafficIntersectionState
            ]
            (TrafficIntersectionState toUpdateState)
            -> void
    {
//        // First check whether the program is still even running?
//        if(!G_PROGRAM_RUNNING) {
//            return;
//        }
        unique_lock<mutex> ul(controller_sem_mutex);                        // Entering critical section
        trafficIntersectionState = toUpdateState;                                                                  // Change shared variables
        changeHappened = true;                                              // Change shared variables

//        cout << toPrint << endl;                                            // To keep trace of time in the chat print before notifying the SEM

        ul.unlock();                                                        // All the shared variables changed - exit critical section

        controller_sem_cv.notify_one();                                     // Data is ready - notify the SEM thread!

        ul.lock();                                                          // Enter monitor
        controller_sem_cv.wait(                                             // Enter the queue and wait for a notification from SEM that lights have been updated
                ul,
                [&changeHappened]() { return !changeHappened || !G_PROGRAM_RUNNING; });
    };
    // ###################################          MONITOR         ####################################################
    thread semaphoreThread(
            semaphore,
            ref(controller_sem_mutex),
            ref(controller_sem_cv),
            ref(changeHappened),
            ref(trafficIntersectionState),
            ref(semaphoreDisplaySharedMutex),
            ref(semaphoreDisplay),
            ref(displayMutex),
            ref(displayQueue),
            ref(displayCv),
            ref(displayMessage)
            );

    // #######################################         HELPER VARS         #############################################
    TrafficIntersectionState toUpdateState{};

//    unsigned sleepTmp;
//    bool anyoneWaiting;                 // Regulates the phases duration
//    bool pedestriansEnoughTime;         // Regulates whether pedestrians will even get a green light(it is possible that they cannot cross due to not having enough time)

    double northSouthPedestriansCounter = 0;
    double northSouthCarsCounter = 0;
    double eastWestPedestriansCounter = 0;
    double eastWestCarsCounter = 0;

    auto updateCounters = [
            &pressersMutex,
            &pressersPositionQueue,
            &metalDetectorsMutex,
            &metalDetectorsPositionsQueue,
            &northSouthPedestriansCounter,
            &northSouthCarsCounter,
            &eastWestPedestriansCounter,
            &eastWestCarsCounter
            ] () -> void
    {
        {
            // Check if there were any pedestrians
            lock_guard<mutex> lg(pressersMutex);
            Compass compassTmp;
            while(!pressersPositionQueue.empty()) {
                compassTmp = pressersPositionQueue.front();
                pressersPositionQueue.pop();
                switch(compassTmp) {
                    case Compass::North:
                    case Compass::South:
                        northSouthPedestriansCounter++;
                        break;
                    case Compass::East:
                    case Compass::West:
                        eastWestPedestriansCounter++;
                        break;
                    default:
                        throw invalid_argument("Unsupported Compass value!");
                }
            }
        }
        {
            // Check if there are any cars waiting
            lock_guard<mutex> lg(metalDetectorsMutex);
            Compass compassTmp;
            while(!metalDetectorsPositionsQueue.empty()) {
                compassTmp = metalDetectorsPositionsQueue.front();
                metalDetectorsPositionsQueue.pop();
                switch(compassTmp) {
                    case Compass::North:
                    case Compass::South:
                        northSouthCarsCounter++;
                        break;
                    case Compass::East:
                    case Compass::West:
                        eastWestCarsCounter++;
                        break;
                    default:
                        throw invalid_argument("Unsupported Compass value!");
                }
            }
        }
    };
    // #######################################         HELPER VARS         #############################################

//    string toPrint;
    while(G_PROGRAM_RUNNING) {
        //  ---------------------------------------     EAST-WEST     --------------------------------------------------
        // First check the amount of pedestrians and cars in this lane
        /*
         * In order to check the period we have to go through the queue and count the number of cars/pedestrians waiting
         * and then depending on that regulate the phase durations.
         */
        updateCounters();

        // Now we wanna communicate with the Semaphore and tell it to change states
        {
            lock_guard<mutex> lg(util::my_stdout_mutex);
            cout << "[CONTROLLER] Changing Semaphore states > Deactivating the North-South lane..." << endl;
            cout << "[CONTROLLER] Changing Semaphore states > Activating the East-West lane..." << endl;
        }
//        toPrint = "[CONTROLLER] Changing Semaphore states > Deactivating the North-South lane...\n"
//                  "[CONTROLLER] Changing Semaphore states > Activating the East-West lane...";

        // Choose the right sleep amount depending on the number of cars and pedestrians
        // In both cases we have to take into consideration whether Pedestrians even have enough time to cross the zebra
        if(eastWestPedestriansCounter == 0 && eastWestCarsCounter == 0) {
            // Use the REDUCED mode
//            toPrint += "\n[CONTROLLER] Nobody waiting -> Reduced cycle!";
            {
                lock_guard<mutex> lg(util::my_stdout_mutex);
                cout << "[CONTROLLER] Nobody waiting -> Reduced cycle!" << endl;
            }
            if(util::TIME_PEDESTRIANS_REDUCED_CROSS_BOOL) {
                // Both Pedestrians and Cars cycles
                toUpdateState = {
                        Colour::Red,
                        Colour::Red,
                        Colour::Green,
                        Colour::Green
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_GREEN_REDUCED));

                toUpdateState = {
                        Colour::Red,
                        Colour::Red,
                        Colour::Green,
                        Colour::Red
                };
                {
                    lock_guard<mutex> lg(util::my_stdout_mutex);
                    cout << "[CONTROLLER] Changing Semaphore states > Deactivating the East-West lane pedestrians..." << endl;
                }
//                toPrint = "[CONTROLLER] Changing Semaphore states > Deactivating the East-West lane pedestrians...";
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_RED_DELTA));
            } else {
                // Only Cars cycle - don't even turn the light on for Pedestrians
//                toPrint += "\n[CONTROLLER] Not turning light on for Pedestrians since they can't cross in time!";
                toUpdateState = {
                        Colour::Red,
                        Colour::Red,
                        Colour::Green,
                        Colour::Red
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_GREEN_REDUCED));
            }
        } else {
            // Use the FULL mode
            if(util::TIME_PEDESTRIANS_FULL_CROSS_BOOL) {
                // Both Pedestrians and Cars cycles
                toUpdateState = {
                        Colour::Red,
                        Colour::Red,
                        Colour::Green,
                        Colour::Green
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_GREEN_FULL));

                toUpdateState = {
                        Colour::Red,
                        Colour::Red,
                        Colour::Green,
                        Colour::Red
                };
                {
                    lock_guard<mutex> lg(util::my_stdout_mutex);
                    cout << "[CONTROLLER] Changing Semaphore states > Deactivating the East-West lane pedestrians..." << endl;
                }
//                toPrint = "[CONTROLLER] Changing Semaphore states > Deactivating the East-West lane pedestrians...";
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_RED_DELTA));
            } else {
                // Only Cars cycle - don't even turn the light on for Pedestrians
//                toPrint += "\n[CONTROLLER] Not turning light on for Pedestrians since they can't cross in time!";
                {
                    lock_guard<mutex> lg(util::my_stdout_mutex);
                    cout << "[CONTROLLER] Not turning light on for Pedestrians since they can't cross in time!" << endl;
                }
                toUpdateState = {
                        Colour::Red,
                        Colour::Red,
                        Colour::Green,
                        Colour::Red
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_GREEN_FULL));
            }
        }

        //  ---------------------------------------     INTER-CYCLE     ------------------------------------------------
        {
            lock_guard<mutex> lg(util::my_stdout_mutex);
            cout << "[CONTROLLER] Intercycle period! All lights red!" << endl;
        }
        updateSemaphore(util::INTERCYCLE_TRAFFIC_INTERSECTION_STATE);
        this_thread::sleep_for(chrono::milliseconds(util::TIME_INTERCYCLE_PERIOD));
        //  ---------------------------------------     INTER-CYCLE     ------------------------------------------------


        // Everyone passed the intersection successfully :D
        eastWestCarsCounter = 0;
        eastWestPedestriansCounter = 0;
        //  ---------------------------------------     EAST-WEST     --------------------------------------------------


        //  ---------------------------------------     NORTH-SOUTH     ------------------------------------------------
        // Gotta count number of cars and pedestrians again
        updateCounters();

        // Now we wanna communicate with the Semaphore and tell it to change states
        {
            lock_guard<mutex> lg(util::my_stdout_mutex);
            cout << "[CONTROLLER] Changing Semaphore states > Deactivating the East-West lane..." << endl;
            cout << "[CONTROLLER] Changing Semaphore states > Activating the North-South lane..." << endl;
        }
//        toPrint = "[CONTROLLER] Changing Semaphore states > Deactivating the East-West lane...\n"
//                  "[CONTROLLER] Changing Semaphore states > Activating the North-South lane...";

        // Okay, do the same as was done with East-West except now do it with North-South
        // Choose the right sleep amount depending on the number of cars and pedestrians
        // In both cases we have to take into consideration whether Pedestrians even have enough time to cross the zebra
        if(northSouthPedestriansCounter == 0 && northSouthCarsCounter == 0) {
            // Use the REDUCED mode
            if(util::TIME_PEDESTRIANS_REDUCED_CROSS_BOOL) {
                // Both Pedestrians and Cars cycles
                toUpdateState = {
                        Colour::Green,
                        Colour::Green,
                        Colour::Red,
                        Colour::Red
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_GREEN_REDUCED));

                toUpdateState = {
                        Colour::Green,
                        Colour::Red,
                        Colour::Red,
                        Colour::Red
                };
                {
                    lock_guard<mutex> lg(util::my_stdout_mutex);
                    cout << "[CONTROLLER] Changing Semaphore states > Deactivating the North-South lane pedestrians..." << endl;
                }
//                toPrint = "[CONTROLLER] Changing Semaphore states > Deactivating the North-South lane pedestrians...";
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_RED_DELTA));
            } else {
                // Only cars cycle - don't even turn the light on for Pedestrians
                toUpdateState = {
                        Colour::Green,
                        Colour::Red,
                        Colour::Red,
                        Colour::Red
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_GREEN_REDUCED));
            }
        } else {
            // Use the FULL mode
            if(util::TIME_PEDESTRIANS_FULL_CROSS_BOOL) {
                // Both Pedestrians and Cars cycles
                toUpdateState = {
                        Colour::Green,
                        Colour::Green,
                        Colour::Red,
                        Colour::Red
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_GREEN_FULL));

                toUpdateState = {
                        Colour::Green,
                        Colour::Red,
                        Colour::Red,
                        Colour::Red
                };
                {
                    lock_guard<mutex> lg(util::my_stdout_mutex);
                    cout << "[CONTROLLER] Changing Semaphore states > Deactivating the North-South lane pedestrians..." << endl;
                }
//                toPrint = "[CONTROLLER] Changing Semaphore states > Deactivating the North-South lane pedestrians...";
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_PEDESTRIAN_RED_DELTA));
            } else {
                // Only Cars cycle - don't even turn the light on for Pedestrians
                toUpdateState = {
                        Colour::Green,
                        Colour::Red,
                        Colour::Red,
                        Colour::Red
                };
                updateSemaphore(toUpdateState);
                this_thread::sleep_for(chrono::milliseconds(util::TIME_GREEN_FULL));
            }
        }

        //  ---------------------------------------     INTER-CYCLE     ------------------------------------------------
        {
            lock_guard<mutex> lg(util::my_stdout_mutex);
            cout << "[CONTROLLER] Intercycle period! All lights red!" << endl;
        }
        updateSemaphore(util::INTERCYCLE_TRAFFIC_INTERSECTION_STATE);
        this_thread::sleep_for(chrono::milliseconds(util::TIME_INTERCYCLE_PERIOD));
        //  ---------------------------------------     INTER-CYCLE     ------------------------------------------------

        // Everyone passed the intersection successfully :D
        northSouthCarsCounter = 0;
        northSouthPedestriansCounter = 0;
        //  ---------------------------------------     NORTH-SOUTH     ------------------------------------------------
    }

    {
        lock_guard<mutex> lg(my_stdout_mutex);
        cout << "[CONTROLLER] Joining semaphore..." << endl;
    }

    controller_sem_cv.notify_one();
    semaphoreThread.join();

    {
        lock_guard<mutex> lg(my_stdout_mutex);
        cout << "[CONTROLLER] Leaving the party due to feirunt..." << endl;
    }
}

void signal_handler(int signal_num)
{
    // Have to join all the threads
    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        cout << "!!!!!!!!!!!!!!!!!!!!           RECEIVED EXIT SIGNAL            !!!!!!!!!!!!!!!!!!!!" << endl;
    }

    G_PROGRAM_RUNNING = false;        // This will cause all the loops to stop running
}

[[noreturn]] void garbageCleaner(queue<thread>& threads, mutex& queue_mutex)
{
    {
        lock_guard<mutex> lg(my_stdout_mutex);
        cout << "[GC] Garbage cleaner ready to recycle threads!" << endl;
    }

    bool queueEmpty = true;
    thread threadTmp;
    while(G_PROGRAM_RUNNING || !queueEmpty) {
//        {
//            lock_guard<mutex> lg(my_stdout_mutex);
//            cout << "[GC] G_PROGRAM_RUNNING: " << (G_PROGRAM_RUNNING ? "true" : "false") << endl;
//            cout << "[GC] !queueEmpty: " << (!queueEmpty ? "true" : "false") << endl;
//            cout << "[GC] G_PROGRAM_RUNNING || !queueEmpty: " << ((G_PROGRAM_RUNNING || !queueEmpty) ? "true" : "false") << endl;
//        }

        {
            lock_guard<mutex> lg(queue_mutex);
            // First check the size of the queue
            if(!threads.empty()) {
                queueEmpty = false;
                threadTmp = move(threads.front());
                threads.pop();
            } else {
                queueEmpty = true;
            }
        }

        if(queueEmpty) {
//            {
//                lock_guard<mutex> lg(my_stdout_mutex);
//                cout << "[GC] Queue empty - sleeping!" << endl;
//            }
            this_thread::sleep_for(chrono::milliseconds (GARBAGE_COLLECTOR_PERIOD));
        } else {
//            {
//                lock_guard<mutex> lg(my_stdout_mutex);
//                cout << "[GC] Joining a thread!" << endl;
//            }
            threadTmp.join();
        }
    }

    {
        lock_guard<mutex> lg(my_stdout_mutex);
        cout << "[GC] Garbage cleaner cleaned all the garbage left after the feirunt!" << endl;
    }
}

int main()
{
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);
    signal(SIGABRT, signal_handler);

    {
        lock_guard<mutex> lg(util::my_stdout_mutex);
        cout << "[MAIN] Welcome to Lab1! This Lab demonstrates a simple traffic lights(Semaphore) intersection simulation!" <<
            endl << endl;
    }
//    cout << "[MAIN] Cout test!" << endl;

    // Main needs to get things running. First let's make a skeleton of everything
    // Pedestrians and cars look at the Semaphores using the below 2 tools
    TrafficIntersectionState semaphoresDisplay{};
    shared_mutex semaphoreDisplaySharedMutex;

    /*
     * The Metal Detectors and Pressers are used by Cars and Pedestrians to make it clear to the Controller that they are waiting for their turn
     */

    // The Metal Detectors
    mutex metalDetectorsMutex;
    queue<Compass> metalDetectorsPositionsQueue;

    // The Pressers
    mutex pressersMutex;
    queue<Compass> pressersPositionsQueue;

    // Prepare the Display thread and its associated Data Structures
    mutex displayMutex;
    queue<DisplayMessage> displayQueue;
    bool displayMessage = false;
    condition_variable displayCv;
    unique_ptr<Display> display = make_unique<Display>(
            util::DISPLAY_FILE,
            displayMutex,
            displayQueue,
            displayCv,
            displayMessage
            );
    display->run();

    thread controllerThread(
            controller,
            ref(semaphoreDisplaySharedMutex),
            ref(semaphoresDisplay),
            ref(metalDetectorsMutex),
            ref(metalDetectorsPositionsQueue),
            ref(pressersMutex),
            ref(pressersPositionsQueue),
            ref(displayMutex),
            ref(displayQueue),
            ref(displayCv),
            ref(displayMessage)
            );

    // Aaaand voila - the simulation is running! Main now needs to randomly create cars and pedestrian threads
    // Let's initiate the random engines for Time and Quantity
    random_device rd;
    mt19937_64 engine(rd());
    uniform_real_distribution<double> timeRandom{TIME_CREATION_MIN, TIME_CREATION_MAX};
    uniform_int_distribution<int> quantityRandom{QUANTITY_CREATION_MIN, QUANTITY_CREATION_MAX};
    uniform_int_distribution<unsigned> humanVisualReactionRandom{
            util::HUMAN_REACTION_TIME[0],
            util::HUMAN_REACTION_TIME[1]
    };
    uniform_int_distribution<char> binaryRandom{0, 1};
    uniform_int_distribution<char> compassRandom{0, 3};

    uniform_int_distribution<char> chanceRandom{0, 100};
    uniform_int_distribution<char> cyclesDryRandom{0, 2};       // Bonus cycles slept!

    unsigned sleepAmount;
    unsigned howMany;
    char whereToCreateChar;
    Compass whereToCreate;
    char pedestrianRoadSideChar;
//    Compass pedestrianConcreteLocation;
    PedestrianCrossing pedestrianCrossing{};
    char whatToCreateChar;
    TrafficEntity whatToCreate;
    char droughtDice;
    char additionalDryCycles;

    uint64_t pedestrians_id = 0;
    uint64_t cars_id = 0;

    queue<thread> trafficParticipants;
    mutex queueMutex;
    thread garbageCleanerThread(garbageCleaner, ref(trafficParticipants), ref(queueMutex));

    while(G_PROGRAM_RUNNING) {
//        {
//            lock_guard<mutex> lg(my_stdout_mutex);
//            cout << "[MAIN] Thinking about adding Traffic Participants..." << endl;
//        }
        // Empty cycle perhaps?
        droughtDice = chanceRandom(engine);
        if(droughtDice > util::DROUGHT_CHANCE) {
            {
                lock_guard<mutex> lg(util::my_stdout_mutex);
                cout << "[MAIN] Drought for " << additionalDryCycles+1 << " cycles!" << endl;
            }
            additionalDryCycles = cyclesDryRandom(engine);
            this_thread::sleep_for(chrono::milliseconds(util::TIME_GREEN_FULL));
            for(int i = 0; i < additionalDryCycles; i++){
                this_thread::sleep_for(chrono::milliseconds(util::TIME_GREEN_REDUCED));
            }
        }

        // Simulate random creation
        sleepAmount = timeRandom(engine);
        this_thread::sleep_for(chrono::milliseconds (sleepAmount));

        // How many shall we create?
        howMany = quantityRandom(engine);

        // How many shall we create?
        for(int i = 0; i < howMany; i++) {
            // What shall we create?
            whatToCreateChar = binaryRandom(engine);
            whatToCreate = Char2TrafficEntity(whatToCreateChar);

            switch(whatToCreate) {
                case TrafficEntity::Car:
                    // Where? Which side of the world?
                    whereToCreateChar = compassRandom(engine);
                    whereToCreate = Char2Compass(whereToCreateChar);

                    {
                        lock_guard<mutex> lg(queueMutex);
                        trafficParticipants.emplace(
                                car,
                                whereToCreate,
                                ref(semaphoreDisplaySharedMutex),
                                ref(semaphoresDisplay),
                                ref(metalDetectorsMutex),
                                ref(metalDetectorsPositionsQueue),
                                ref(engine),
                                ref(humanVisualReactionRandom),
                                cars_id++,
                                ref(displayMutex),
                                ref(displayQueue),
                                ref(displayCv),
                                ref(displayMessage)
                        );
                    }

                    break;
                case TrafficEntity::Pedestrian:
                    // Where? Which side of the world?
                    whereToCreateChar = compassRandom(engine);
                    whereToCreate = Char2Compass(whereToCreateChar);
                    pedestrianRoadSideChar = binaryRandom(engine);
                    pedestrianCrossing = generatePedestrianCrossing(whereToCreate, pedestrianRoadSideChar);
                    {
                        lock_guard<mutex> lg(my_stdout_mutex);
                        trafficParticipants.emplace(
                                pedestrian,
                                whereToCreate,
                                pedestrianCrossing,
                                ref(semaphoreDisplaySharedMutex),
                                ref(semaphoresDisplay),
                                ref(pressersMutex),
                                ref(pressersPositionsQueue),
                                ref(engine),
                                ref(humanVisualReactionRandom),
                                pedestrians_id++,
                                ref(displayMutex),
                                ref(displayQueue),
                                ref(displayCv),
                                ref(displayMessage)
                        );
                    }

                    break;
                default:
                    throw invalid_argument("Neither car nor pedestrian!");
            }
        }
    }

    {
        lock_guard<mutex> lg(my_stdout_mutex);
        cout << "[MAIN] Joining Controller..." << endl;
    }
    controllerThread.join();

    {
        lock_guard<mutex> lg(my_stdout_mutex);
        cout << "[MAIN] Joining GC..." << endl;
    }
    garbageCleanerThread.join();

    display.reset();

    {
        lock_guard<mutex> lg(my_stdout_mutex);
        cout << "[MAIN] This party's over..." << endl;
        cout << "[MAIN] Feirunt process complete..." << endl;
        cout << "[MAIN] Exiting..." << endl;
    }
}

/*
 * TODO: Maybe add condition_variables and waiting to GC
 */