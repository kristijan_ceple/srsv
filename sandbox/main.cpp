#include <iostream>
#include <set>
using namespace std;

struct outer {
    int x = 5;

    struct inner {
        int y = 10;

        void f() {
//            x = 6;
        }
    };
};

enum class SystemPins{
    Pin1, Pin2, Pin3
};

SystemPins myPins[] = {SystemPins::Pin1, SystemPins::Pin1, SystemPins::Pin3};

int main() {
    multiset testSet {1, 2, 3, 3, 3, 4, 5};      // 3 Appears 3 times!

    auto it = testSet.lower_bound(3);
    while(it != testSet.end()) {
        cout << *it << ", ";
        it++;
    }
    cout << endl;

    it = testSet.find(3);
    while(it != testSet.end()) {
        cout << *it << ", ";
        it++;
    }
    cout << endl;

    it = testSet.upper_bound(3);
    while(it != testSet.end()) {
        cout << *it << ", ";
        it++;
    }
    cout << endl;

    outer::inner test{};
    test.x;


    return 0;
}
