//
// Created by kikyy_99 on 18.01.21.
//

#include "Generator.h"
#include <errno.h>
using namespace std;


void Generator::run() {
    // Set my env and priority first
    this->ipcName = getenv(Generator::IPC_ENV);
    pthread_t myThreadID = pthread_self();
    cout << fmt::format(
            "Setting scheduling policy SCHED_RR at priority {} for my thread ID: {}\n",
            Generator::SCHED_PARAM.sched_priority, myThreadID
            );
    pthread_setschedparam(myThreadID, SCHED_RR, &Generator::SCHED_PARAM);

    // Check whether the memory has been initialised
    this->checkMq();
    this->checkShmGeneratorMutexId();

    // Generate jobs
    this->generation();
}

void Generator::checkMq() {
    this->mqFd = mq_open(fmt::format("/{}", this->ipcName).c_str(), O_RDWR | O_CREAT, 0666, &Generator::MQ_ATTR_NEW_DEFAULT);
    perror("mq error");
    if (this->mqFd == -1) {
        perror("mq error");
        throw runtime_error("Runtime error!");
        //cout << fmt::format("errno: {}", errno);
    }
}

void Generator::checkShmGeneratorMutexId() {
    int res = shm_open(fmt::format("/{}GenMutex", this->ipcName).c_str(), O_RDWR | O_CREAT | O_EXCL, 0666);
    if(res == -1) {
        // Object already exists
        this->genMutexShm.fileDescriptor = shm_open(fmt::format("/{}GenMutex", this->ipcName).c_str(), O_RDWR | O_CREAT, 0666);
        this->genMutexShm.mappedPtr = (pthread_mutex_t*)mmap(nullptr, sizeof(pthread_mutex_t),
                                                             PROT_READ | PROT_WRITE, MAP_SHARED,
                                                             this->genMutexShm.fileDescriptor, 0);

        this->lastIdShm.fileDescriptor = this->genMutexShm.fileDescriptor = shm_open(fmt::format("/{}GenLastID", this->ipcName).c_str(), O_RDWR | O_CREAT, 0666);
        this->lastIdShm.mappedPtr = (unsigned long long*)mmap(nullptr, sizeof(unsigned long long),
                                                              PROT_READ | PROT_WRITE, MAP_SHARED,
                                                              this->lastIdShm.fileDescriptor, 0);
    } else {
        // Initialise
        this->genMutexShm.fileDescriptor = res;
        ftruncate(this->genMutexShm.fileDescriptor, sizeof(pthread_mutex_t));
        this->genMutexShm.mappedPtr = (pthread_mutex_t*)mmap(nullptr, sizeof(pthread_mutex_t),
                                                             PROT_READ | PROT_WRITE, MAP_SHARED,
                                                             this->genMutexShm.fileDescriptor, 0);

        res = pthread_mutex_init(this->genMutexShm.mappedPtr, nullptr);
        if(res == -1) {
            throw runtime_error("Mutex creation fail!");
        }

        // Lock it and initialise last ID as well!
        pthread_mutex_lock(this->genMutexShm.mappedPtr);
        this->lastIdShm.fileDescriptor = this->genMutexShm.fileDescriptor = shm_open(fmt::format("/{}GenLastID", this->ipcName).c_str(), O_RDWR | O_CREAT, 0666);
        ftruncate(this->lastIdShm.fileDescriptor, sizeof(unsigned long long));
        this->lastIdShm.mappedPtr = (unsigned long long*)mmap(nullptr, sizeof(unsigned long long),
                                                              PROT_READ | PROT_WRITE, MAP_SHARED,
                                                              this->lastIdShm.fileDescriptor, 0);
        *this->lastIdShm.mappedPtr = 0;
        pthread_mutex_unlock(this->genMutexShm.mappedPtr);
    }

    this->lastIdShm.length = sizeof(unsigned long long);
    this->genMutexShm.length = sizeof(pthread_mutex_t);

    this->lastIdShm.name = fmt::format("/{}GenLastID", this->ipcName);
    this->genMutexShm.name = fmt::format("/{}GenMutex", this->ipcName);
}

void Generator::generation() {
    for(int i = 0; i < this->jobsToGen; i++) {
        sleep(1);

        unsigned long long jobId;
        pthread_mutex_lock(this->genMutexShm.mappedPtr);
        jobId = (*this->lastIdShm.mappedPtr)++;
        pthread_mutex_unlock(this->genMutexShm.mappedPtr);

        unsigned int duration = (*this->durationRand)(this->engine);
        string name = fmt::format("/{}-{}", this->ipcName, jobId);

        auto& jobIt = this->jobsGenerated.emplace_back(jobId, duration, name.c_str());
        if(duration == 0) {
            cout << fmt::format("G: Job {} {} {} [ ]\n", jobIt.id, jobIt.duration, jobIt.name);
            continue;
        }

        ShmSegment<unsigned int> jobShmSg = jobIt.createShmSegment();

        // Print the generated Jobs
        string toPrint = fmt::format(
                "G: Job {} {} {} [ ",
                jobIt.id, jobIt.duration, jobIt.name
        );
        // Have to prepare the job array now!
        for(int j = 0; j < duration; j++) {
            unsigned int tmp = this->jobPayloadRand(this->engine);
            *(jobShmSg.mappedPtr+j) = tmp;
            toPrint += fmt::format("{} ", tmp);
        }
        toPrint += "]\n";

        jobIt.sendJobMessage(this->mqFd);
        cout << toPrint;
    }
}

Generator::~Generator() {
    // Close message queue
    mq_close(this->mqFd);

    // Unmap my pointers - necessary, is done automatically when the process terminates!
    munmap(this->genMutexShm.mappedPtr, this->genMutexShm.length);
    munmap(this->lastIdShm.mappedPtr, this->lastIdShm.length);

    cout << "G: Over and out!" << endl;
}

/**
 * Program entry point
 * @return sys exit code
 */
int main(int argc, char* argv[]) {
    if(argc != 3) {
        return -1;
    }

    unsigned int jobsToGen = std::stoul(argv[1]);
    unsigned int maxTimeUnits = std::stoul(argv[2]);

    Generator generator{jobsToGen, maxTimeUnits};
    generator.run();
    return 0;
}