//
// Created by kikyy_99 on 19.01.21.
//

#ifndef LAB5_JOB_H
#define LAB5_JOB_H

#include <iostream>
#include "ShmSegment.h"

class Job {
    friend class Generator;
private:
    template<class T>
    static T* getShmSegmentPtr(int fileDescriptor);
    void sendJobMessage(int mqfD);

    std::unique_ptr<ShmSegment<unsigned int>> shmSegment = nullptr;
public:
    const unsigned long long id = 0;
    const unsigned int duration = 0;
    const std::string name = "nullptr";

    Job(unsigned long long id, unsigned int duration, const std::string& name);
    explicit Job(const std::string& fromMessageQueueJob);
    Job(const Job& toDoJob);

    ShmSegment<unsigned int>& createShmSegment();
    ShmSegment<unsigned int>& getShmSegment();
    int destroyShmSegment();

    friend std::ostream& operator<<(std::ostream& os, const Job& j);
};


#endif //LAB5_JOB_H
