//
// Created by kikyy_99 on 19.01.21.
//

#ifndef LAB5_SERVER_H
#define LAB5_SERVER_H

#include <iostream>
#include <vector>
#include <csignal>
#include <fmt/format.h>
#include <fcntl.h>
#include <mqueue.h>
#include "WorkerThread.h"


class Server {
    friend class WorkerThread;
private:
    static inline constexpr char IPC_ENV[] = "SRSV_LAB5";
    static inline unsigned int INACTIVITY_SECONDS_MAX = 30;
    char* ipcName = nullptr;

    static inline constexpr sched_param SCHED_PARAM {
            60
    };

    mq_attr mqAttr;
    int mqFd;
    char* messageBuffer = nullptr;
    size_t messageBufferSize = 0;

    unsigned int workerThreadsNum = 0;
    unsigned int minimumTime = 0;
    std::vector<WorkerThread> workerThreads;
    int lastThreadId = 0;

    static inline bool sigtermIssued = false;
    static inline bool toEnd = false;
    static inline pthread_rwlock_t toEndMutex = PTHREAD_RWLOCK_INITIALIZER;

    std::deque<Job> jobsQueue{};
    pthread_mutex_t jobsQueueMutex;

    pthread_cond_t threadsSleepingCond;
    pthread_mutex_t threadsSleepingMutex;

    unsigned int workToDoTime = 0;
    pthread_mutex_t workToDoTimeMutex;

    // Clock-related member fields
    static inline unsigned int iterations1Sec = 1;
    timespec clockResolution;
    timespec lastActivity;

    void calculateIterations1Sec();
    static void signal_handler(int signal_num);
    void shouldEndProcess();
    bool shouldEnd = false;
public:
    Server(unsigned int workerThreadsNum, unsigned int minimumTime);
    void run();
    static inline unsigned int getIterations1Sec() {return Server::iterations1Sec;}
    ~Server();
};


#endif //LAB5_SERVER_H
