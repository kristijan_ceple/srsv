//
// Created by kikyy_99 on 18.01.21.
//

#include "Server.h"
#include <unistd.h>
using namespace std;


Server::Server(unsigned int workerThreadsNum, unsigned int minimumTime) {
    if(workerThreadsNum == 0) {
        throw invalid_argument("0 Worker Threads!?");
    }

    this->minimumTime = minimumTime;

    // Init mutexes and condition var
    int res;

    res = pthread_mutex_init(&this->jobsQueueMutex, nullptr);
    if(res == -1) {
        throw runtime_error("Mutex creation fail!");
    }

    res = pthread_mutex_init(&this->workToDoTimeMutex, nullptr);
    if(res == -1) {
        throw runtime_error("Mutex creation fail!");
    }

    res = pthread_cond_init(&this->threadsSleepingCond, nullptr);
    if(res == -1) {
        throw runtime_error("Condition variable creation fail!");
    }

    res = pthread_mutex_init(&this->threadsSleepingMutex, nullptr);
    if(res == -1) {
        throw runtime_error("Mutex creation fail!");
    }

    this->ipcName = getenv(Server::IPC_ENV);
    pthread_t myThreadID = pthread_self();
    cout << fmt::format(
            "Setting scheduling policy SCHED_RR at priority {} for my thread ID: {}\n",
            Server::SCHED_PARAM.sched_priority, myThreadID
    );
    pthread_setschedparam(myThreadID, SCHED_RR, &Server::SCHED_PARAM);

    // Init Worker Threads
    this->workerThreadsNum = workerThreadsNum;
    for(int i = 0; i < this->workerThreadsNum; i++) {
        auto& workerThread = this->workerThreads.emplace_back(
                this->lastThreadId++,
                this->jobsQueue, this->jobsQueueMutex, Server::toEnd, Server::toEndMutex,
                this->workToDoTime, this->workToDoTimeMutex, this->threadsSleepingCond, this->threadsSleepingMutex
                );
    }

    for(auto& workerThread : this->workerThreads) {
        workerThread.run();
    }

    res = mq_open(fmt::format("/{}", this->ipcName).c_str(), O_RDWR | O_CREAT | O_NONBLOCK, 0666, nullptr);
    if(res == -1) {
        throw runtime_error("Error while trying to open message queue!");
    } else {
        this->mqFd = res;
    }

    // Init msg buffer
    mq_getattr(this->mqFd, &this->mqAttr);
    //this->messageBuffer.reserve(this->mqAttr.mq_msgsize);
    this->messageBuffer = new char[this->mqAttr.mq_msgsize + 1];
    this->messageBufferSize = this->mqAttr.mq_msgsize;

    this->calculateIterations1Sec();
    signal(SIGTERM, Server::signal_handler);
//    signal(SIGINT, Server::signal_handler);         // Used for debugging
//    signal(SIGHUP, Server::signal_handler);         // Used for debugging
}

/**
 * ONly SIGTERM is handled, other signals are ignored!
 * @param signal_num the signal that is being handled!
 */
void Server::signal_handler(int signal_num) {
    if(signal_num == SIGTERM) {
        Server::sigtermIssued = true;
        cout << "Received SIGTERM!\n";
    }
}

/**
 * State machine
 */
void Server::run() {
    this->shouldEndProcess();
    // Initialise initial timepoint
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &this->lastActivity);

    // Step 4
    bool workersWokenAlready = false;
    while(!this->shouldEnd) {
        // Step 1 - Check if there are any messages in the IPC MQ
        mq_getattr(this->mqFd, &this->mqAttr);
        if(this->messageBufferSize != this->mqAttr.mq_msgsize) {
            this->messageBuffer = new char[this->mqAttr.mq_msgsize + 1];
            this->messageBufferSize = this->mqAttr.mq_msgsize;
        }

        if(this->mqAttr.mq_curmsgs > 0) {
            // Note down the time since last arrival
            clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &this->lastActivity);
            workersWokenAlready = false;
        }

        vector<string> messagesTmp(this->mqAttr.mq_curmsgs);
        for(int i = 0; i < this->mqAttr.mq_curmsgs; i++) {
            int received = mq_receive(this->mqFd, this->messageBuffer, this->messageBufferSize, nullptr);
//            this->messageBuffer[received] = '\0';
            if(received == -1) {
                continue;       // Bug-block on mq
            }

//        cout << fmt::format("Received bytes: {}\n", received);
            cout << fmt::format("S: received {}\n", this->messageBuffer);

            pthread_mutex_lock(&this->jobsQueueMutex);
            pthread_mutex_lock(&this->workToDoTimeMutex);
            auto jobIt = this->jobsQueue.emplace_back(this->messageBuffer);
            this->workToDoTime += jobIt.duration;
            pthread_mutex_unlock(&this->workToDoTimeMutex);
            pthread_mutex_unlock(&this->jobsQueueMutex);
        }

        // Step 2 - Jobs have been emplaced - check if we should signal the threads to get to work!
        bool getToWork = false;

        pthread_mutex_lock(&this->jobsQueueMutex);
        pthread_mutex_lock(&this->workToDoTimeMutex);
        if(this->jobsQueue.size() >= this->workerThreadsNum) {
            if(this->workToDoTime >= this->minimumTime) {
                getToWork = true;
            }
        }
        pthread_mutex_unlock(&this->workToDoTimeMutex);
        pthread_mutex_unlock(&this->jobsQueueMutex);

        // Steps 2 and 3
        if(getToWork && !workersWokenAlready) {
            // Signal, reset time
            cout << "S: Work to be done! Waking the workers!\n";
            pthread_cond_broadcast(&this->threadsSleepingCond);
            workersWokenAlready = true;
        } else {
            // Check if more than 30 seconds has passed
            timespec tmpTimespec{};
            clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tmpTimespec);
            auto secsPassed = tmpTimespec.tv_sec - this->lastActivity.tv_sec;

            if(secsPassed >= Server::INACTIVITY_SECONDS_MAX && !workersWokenAlready) {
                cout << "S: starting leftover jobs processing\n";
                // Signal to threads either way if there are any leftover Jobs in the queue!
                pthread_cond_broadcast(&this->threadsSleepingCond);
                workersWokenAlready = true;
            }
        }

        // Check and update whether we should end for Step 4
        this->shouldEndProcess();
        // Now go back to the beginning and do it all over again!
    }
}

void Server::calculateIterations1Sec() {
    clock_getres(CLOCK_PROCESS_CPUTIME_ID, &clockResolution);
    unsigned long long start, end;
    long long nanosecondsPassed;

    timespec clockTimePassedStart{};
    timespec clockTimePassedEnd{};
    do {
        Server::iterations1Sec<<=1;
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &clockTimePassedStart);
        start = clockTimePassedStart.tv_sec*1000000000 + clockTimePassedStart.tv_nsec;
        for(int i = 0; i < Server::iterations1Sec; i++) {
            asm volatile ("" ::: "memory");
        }
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &clockTimePassedEnd);
        end = clockTimePassedEnd.tv_sec*1000000000 + clockTimePassedEnd.tv_nsec;
        nanosecondsPassed = end - start;
    } while(nanosecondsPassed < 1000000000);

    Server::iterations1Sec /= nanosecondsPassed/1e9;
}

/**
 * The destructor joins all the threads!
 */
Server::~Server() {
    // Wake up workers!
    pthread_cond_broadcast(&this->threadsSleepingCond);

    for(const auto& workerThread : this->workerThreads) {
        pthread_t tmpId = workerThread.getMyThreadId();
        while(tmpId == 0) {
            tmpId = workerThread.getMyThreadId();
        }
        pthread_join(tmpId, nullptr);
    }

    mq_close(this->mqFd);
    pthread_mutex_destroy(&this->workToDoTimeMutex);
    pthread_mutex_destroy(&this->threadsSleepingMutex);
    pthread_mutex_destroy(&this->jobsQueueMutex);
    pthread_rwlock_destroy(&Server::toEndMutex);

    // Unlink message queue, mutex and id!
    shm_unlink(fmt::format("/{}GenMutex", this->ipcName).c_str());
    shm_unlink(fmt::format("/{}GenLastID", this->ipcName).c_str());
    mq_unlink(fmt::format("/{}", this->ipcName).c_str());

    cout << "S: Over and out!\n";
}

/**
 * Updates the static variable toEnd, and returns it. This method is thread-safe
 * @return whether the process should end
 */
void Server::shouldEndProcess() {
    if(this->shouldEnd) {
        return;
    }

    // True if sigterm was issued, and the message queue is empty
    mq_getattr(this->mqFd, &this->mqAttr);
    this->shouldEnd = Server::sigtermIssued && this->mqAttr.mq_curmsgs == 0;

    if(this->shouldEnd) {
        pthread_rwlock_wrlock(&Server::toEndMutex);
        Server::toEnd = true;
        pthread_rwlock_unlock(&Server::toEndMutex);
    }
}

int main(int argc, char* argv[]) {
    if(argc != 3) {
        return -1;
    }

    unsigned int workerThreadsNum = std::stoul(argv[1]);
    unsigned int minimumTime = std::stoul(argv[2]);

    Server server{workerThreadsNum, minimumTime};
    server.run();
}