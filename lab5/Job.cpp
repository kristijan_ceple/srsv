//
// Created by kikyy_99 on 19.01.21.
//

#include <string>
#include <iostream>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fmt/format.h>
#include <unistd.h>
#include <sys/types.h>
#include <mqueue.h>
#include <random>
#include <utility>
#include <iostream>
#include <fmt/format.h>
#include "Job.h"
using namespace std;

Job::Job(const unsigned long long int id, const unsigned int duration, const string&  name) : id{id}, duration{duration}, name{name}
{
    //const_cast<string&>(this->name) += '\0';
}

std::ostream &operator<<(std::ostream &os, const Job &j) {
    os << fmt::format("{} {} {}", j.id, j.duration, j.name);
    return os;
}

ShmSegment<unsigned int>& Job::getShmSegment() {
    this->shmSegment = make_unique<ShmSegment<unsigned int>>(-1, nullptr, sizeof(unsigned int) * this->duration, this->name);
    if(this->duration == 0) {
        return *this->shmSegment;
    }

    this->shmSegment->fileDescriptor = shm_open(this->name.c_str(), O_RDWR, 0666);
    if(this->shmSegment->fileDescriptor == -1) {
        perror("shm_open error during getShmSegment()");
        cerr << fmt::format("Failed: shm_open({}, O_RDWR, 0666);\n", this->name.c_str());
        throw runtime_error("Runtime error!");
    }

    this->shmSegment->mappedPtr = Job::getShmSegmentPtr<unsigned int>(this->shmSegment->fileDescriptor);
    return *this->shmSegment;
}

ShmSegment<unsigned int>& Job::createShmSegment() {
    this->shmSegment = make_unique<ShmSegment<unsigned int>>(-1, nullptr, sizeof(unsigned int) * this->duration, this->name);
    if(this->duration == 0) {
        return *this->shmSegment;
    }

    this->shmSegment->fileDescriptor = shm_open(this->name.c_str(), O_RDWR | O_CREAT, 0666);
    if(this->shmSegment->fileDescriptor == -1) {
        perror("shm_open error during getShmSegment() - shared memory segment already exists!?");
        cerr << "Recreating the memory segment!\n";
    }

    ftruncate(this->shmSegment->fileDescriptor, this->duration * sizeof(unsigned int));

    this->shmSegment->mappedPtr = Job::getShmSegmentPtr<unsigned int>(this->shmSegment->fileDescriptor);
    return *this->shmSegment;
}

template<class T>
T *Job::getShmSegmentPtr(int fileDescriptor) {
    if(fileDescriptor == -1) {
        throw invalid_argument("Job shared memory segment file descriptor is -1!?");
    }

    T* toRet;
    toRet = (T*)mmap(
            nullptr, sizeof(unsigned int), PROT_READ | PROT_WRITE, MAP_SHARED, fileDescriptor, 0
    );

    if(toRet == (void*)-1) {
        perror("mmap error during getSHmSegment()");
        throw runtime_error("Runtime error!");
    }

    return toRet;
}

void Job::sendJobMessage(int mqfD) {
    string toSend = fmt::format("{} {} {}", this->id, this->duration, this->name);
    int res = mq_send(mqfD, toSend.c_str(), toSend.size()+1, 0);
    if(res == -1) {
        perror("Error during sending message!");
        throw runtime_error("Runtime error!");
    }
}

Job::Job(const std::string &fromMessageQueueJob) {
    // Split name into parts
    vector<string> tokens(3);
    string toTokenise = fromMessageQueueJob;
    char* token = std::strtok(toTokenise.data(), " ");
    while(token) {
        tokens.emplace_back(token);
        token = strtok(nullptr, " ");
    }

    *const_cast<string*>(&this->name) = tokens.back();
    tokens.pop_back();

    *const_cast<unsigned int*>(&this->duration) = stoul(tokens.back());
    tokens.pop_back();

    *const_cast<unsigned long long*>(&this->id) = stoull(tokens.back());
    tokens.pop_back();
}

int Job::destroyShmSegment() {
    bool noErrors = true;

    if(this->shmSegment->mappedPtr != nullptr) {
        int res = munmap(this->shmSegment->mappedPtr, this->shmSegment->length);
        if(res != 0) {
            noErrors = false;
            perror("Munmap error on segment!");
            std::cerr << fmt::format("Errno: {}\n", errno);
            //throw std::runtime_error("Runtime error!");
        }
    }

    this->shmSegment.release();

    int res = shm_unlink(this->name.c_str());
    if(res != 0) {
        noErrors = false;
        perror("Munmap error on segment!");
        std::cerr << fmt::format("Errno: {}\n", errno);
    }

    return noErrors ? 0 : -1;
}

Job::Job(const Job& toDoJob) : name{toDoJob.name}, duration{toDoJob.duration}, id{toDoJob.id} {}

