//
// Created by kikyy_99 on 18.01.21.
//
#include <string>
#include <iostream>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fmt/format.h>
#include <unistd.h>
#include <sys/types.h>
#include <mutex>
#include <mqueue.h>
#include <ctime>
#include "Job.h"
using namespace std;

constexpr char IPC_ENV[] = "SRSV_LAB5";

void unlinker() {
//    shm_unlink(fmt::format("/{}GenLastID", IPC_ENV).c_str());
//    shm_unlink(fmt::format("/{}GenMutex", IPC_ENV).c_str());
    mq_unlink(fmt::format("/{}", IPC_ENV).c_str());

    for(int i = 0; i < 1e6; i++) {
        shm_unlink(fmt::format("/{}-{}", IPC_ENV, i).c_str());
    }
}

timespec clockResolution;
timespec clockTimePassedStart;
timespec clockTimePassedEnd;
unsigned long long iterations1Sec = 1;

void iterationsSbox() {
    clock_getres(CLOCK_PROCESS_CPUTIME_ID, &clockResolution);
    unsigned long long start, end;
    long long nanosecondsPassed;
    do {
        iterations1Sec<<=1;
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &clockTimePassedStart);
        start = clockTimePassedStart.tv_sec*1000000000 + clockTimePassedStart.tv_nsec;
        for(int i = 0; i < iterations1Sec; i++) {
            asm volatile ("" ::: "memory");
        }
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &clockTimePassedEnd);
        end = clockTimePassedEnd.tv_sec*1000000000 + clockTimePassedEnd.tv_nsec;
        nanosecondsPassed = end - start;
    } while(nanosecondsPassed < 1000000000);

    iterations1Sec /= nanosecondsPassed/1e9;

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &clockTimePassedStart);
    start = clockTimePassedStart.tv_sec*1000000000 + clockTimePassedStart.tv_nsec;
    for(int i = 0; i < iterations1Sec; i++) {
        asm volatile ("" ::: "memory");
    }
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &clockTimePassedEnd);
    end = clockTimePassedEnd.tv_sec*1000000000 + clockTimePassedEnd.tv_nsec;
    nanosecondsPassed = end - start;
    cout << "Seconds passed: " << nanosecondsPassed/1000000000 << endl;
    cout << "Nanoseconds passed: " << nanosecondsPassed << endl;
}

int main() {
//    unlinker();

    printf("Sandbox!\n");
//    cout << sizeof(unsigned long long) << endl;
//    cout << sizeof(uint64_t) << endl;
//
//    pthread_mutex_t* testPMutex = nullptr;
//    Job testJob{1, 10, "Test_Jobbie"};
//    cout << testJob << endl;

//    pthread_mutex_init(testPMutex, nullptr);

    //iterationsSbox();

    return 0;
}