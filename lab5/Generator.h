//
// Created by kikyy_99 on 19.01.21.
//

#ifndef LAB5_GENERATOR_H
#define LAB5_GENERATOR_H


#include <string>
#include <iostream>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fmt/format.h>
#include <unistd.h>
#include <sys/types.h>
#include <mqueue.h>
#include <random>
#include "ShmSegment.h"
#include "Job.h"


class Generator {
private:
    static inline constexpr char IPC_ENV[] = "SRSV_LAB5";
    static inline long MQ_MAX_MSG_SIZE = 8192;
    static inline long MQ_MAX_MSG_QUANTITY = 50;

    static inline mq_attr MQ_ATTR_NEW_DEFAULT {
        .mq_maxmsg = 70,
        .mq_msgsize = 8192
    };

    static inline constexpr sched_param SCHED_PARAM {
        50
    };

    char* ipcName = nullptr;
    mq_attr mqAttr;

    unsigned int jobsToGen;
    unsigned int maxTimeUnits;
    std::vector<Job> jobsGenerated;

    // Random number generator
    std::random_device rd{};
    std::mt19937_64 engine{this->rd()};
    std::uniform_int_distribution<unsigned int> jobPayloadRand{0, 1000};
    std::unique_ptr<std::uniform_int_distribution<unsigned int>> durationRand;

    ShmSegment<pthread_mutex_t> genMutexShm{};
    ShmSegment<unsigned long long> lastIdShm{};
    int mqFd;

    void checkMq();
    void checkShmGeneratorMutexId();
    void generation();
public:
    Generator(unsigned int jobsToGen, unsigned int maxTimeUnits) : jobsToGen{jobsToGen}, maxTimeUnits{maxTimeUnits} {
        this->durationRand = std::make_unique<std::uniform_int_distribution<unsigned int>>(0, this->maxTimeUnits);
        mqAttr.mq_maxmsg = Generator::MQ_MAX_MSG_QUANTITY;
        mqAttr.mq_msgsize = Generator::MQ_MAX_MSG_SIZE;
    };
    void run();
    ~Generator();
};

#endif //LAB5_GENERATOR_H
