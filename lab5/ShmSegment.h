//
// Created by kikyy_99 on 18.01.21.
//

#ifndef LAB5_SHMSEGMENT_H
#define LAB5_SHMSEGMENT_H

#include <sys/mman.h>
#include <cstdio>
#include <string>

template<class T>
struct ShmSegment {
    int fileDescriptor = -1;
    T* mappedPtr = nullptr;
    unsigned int length = 0;
    std::string name;

    ShmSegment() = default;
    ShmSegment(int fileDescriptor, T* mappedPtr, unsigned int length, const std::string& name)
    : fileDescriptor{fileDescriptor}, mappedPtr{mappedPtr}, length{length}, name{name} {};
};

#endif //LAB5_SHMSEGMENT_H
