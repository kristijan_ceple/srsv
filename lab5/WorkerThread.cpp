//
// Created by kikyy_99 on 19.01.21.
//

#include "WorkerThread.h"
#include "Server.h"
using namespace std;

WorkerThread::WorkerThread(
        int processThreadId,
        std::deque<Job> &jobsQueue, pthread_mutex_t &jobsQueueMutex,
        bool &toEnd, pthread_rwlock_t &toEndMutex,
        unsigned int& workToDoTime, pthread_mutex_t& workToDoTimeMutex,
        pthread_cond_t& threadsSleepingCond, pthread_mutex_t& threadsSleepingMutex)
        : processThreadId{processThreadId}, jobsQueue{jobsQueue}, jobsQueueMutex{jobsQueueMutex},
        toEnd{toEnd}, toEndMutex{toEndMutex},
        workToDoTime{workToDoTime}, workToDoTimeMutex{workToDoTimeMutex},
        threadsSleepingCond{threadsSleepingCond}, threadsSleepingMutex{threadsSleepingMutex} {}

void WorkerThread::run() {
    pthread_create(&this->myThread, nullptr, WorkerThread::threadMethod, this);
}

void *WorkerThread::threadMethod(void *vargp) {
    auto* myWorkerThreadObject = (WorkerThread*)vargp;
    cout << fmt::format("Worker {} Thread created!\n", myWorkerThreadObject->processThreadId);

//    cout << fmt::format("Worker {} pthread_self(): {}\n", myWorkerThreadObject->processThreadId, pthread_self());
//    cout << fmt::format("Worker {} this->myThread {}\n", myWorkerThreadObject->processThreadId, myWorkerThreadObject->myThread);

//    pthread_t myThreadID = pthread_self();
    cout << fmt::format(
            "Setting scheduling policy SCHED_RR at priority {} for my thread ID: {}\n",
            WorkerThread::SCHED_PARAM.sched_priority, myWorkerThreadObject->myThread
    );
    pthread_setschedparam(myWorkerThreadObject->myThread, SCHED_RR, &WorkerThread::SCHED_PARAM);

    bool contRunningThread = myWorkerThreadObject->contRunningThread();
    while(contRunningThread) {
        // Step 1
        // Check if there are any jobs in the queue, and take one if there is some available!
        pthread_mutex_lock(&myWorkerThreadObject->jobsQueueMutex);
        pthread_mutex_lock(&myWorkerThreadObject->workToDoTimeMutex);
        if(!myWorkerThreadObject->jobsQueue.empty()) {
            myWorkerThreadObject->currWorkerJob = make_unique<WorkerJob>(
                    myWorkerThreadObject->jobsQueue.front(),
                    0
                    );
            myWorkerThreadObject->jobsQueue.pop_front();
            myWorkerThreadObject->workToDoTime -= myWorkerThreadObject->currWorkerJob->toDoJob.duration;
        }
        pthread_mutex_unlock(&myWorkerThreadObject->workToDoTimeMutex);
        pthread_mutex_unlock(&myWorkerThreadObject->jobsQueueMutex);

        // Step 2 and 3 - do my Job or go to sleep
        myWorkerThreadObject->executeJob();

        // Step 4 -> go to beginning! First check whether we should continue running or end!
        contRunningThread = myWorkerThreadObject->contRunningThread();
    }

    myWorkerThreadObject->threadFinished = true;
    pthread_exit(&myWorkerThreadObject->threadExitRetval);
}

void WorkerThread::executeJob() {
    if(this->currWorkerJob == nullptr) {
        // Go to sleep or exit
        bool contRunningThread = this->contRunningThread();
        if(contRunningThread) {
            // Go to sleep and await waking up
            cout << fmt::format(
                    "W{}: no jobs, going to sleep\n",
                    this->processThreadId
            );

            pthread_mutex_lock(&this->threadsSleepingMutex);
            pthread_cond_wait(&this->threadsSleepingCond, &this->threadsSleepingMutex);
            pthread_mutex_unlock(&this->threadsSleepingMutex);

        } else {
            // No jobs and me need to end!
            this->threadFinished = true;
            pthread_exit(&this->threadExitRetval);
        }
    } else {
        // Got a job to consume!
        // Get the shared memory segment
        auto shmSg = this->currWorkerJob->toDoJob.getShmSegment();

        for(int i = 0; i < this->currWorkerJob->toDoJob.duration; i++) {
            WorkerThread::sleep1SecondLoop();
            this->currWorkerJob->timeUnitsCompleted++;
            cout << fmt::format(
                    "W{}: JobId: {} processing data piece: {} ({}/{})\n",
                    this->processThreadId, this->currWorkerJob->toDoJob.id,
                    *(shmSg.mappedPtr+i), this->currWorkerJob->timeUnitsCompleted, this->currWorkerJob->toDoJob.duration
                    );
        }
        cout << fmt::format(
                "W{}: JobId: {} processing complete\n",
                this->processThreadId, this->currWorkerJob->toDoJob.id
                );

        this->currWorkerJob->toDoJob.destroyShmSegment();
        this->completedJobs.push_back(*this->currWorkerJob);
        this->currWorkerJob.release();
    }
}

bool WorkerThread::contRunningThread() {
    bool needToEnd;
    pthread_rwlock_rdlock(&Server::toEndMutex);
    needToEnd = Server::toEnd;
    pthread_rwlock_unlock(&Server::toEndMutex);

    bool localQueueEmpty;
    pthread_mutex_lock(&this->jobsQueueMutex);
    localQueueEmpty = this->jobsQueue.empty();
    pthread_mutex_unlock(&this->jobsQueueMutex);

    return !(needToEnd && localQueueEmpty);
}

void WorkerThread::sleep1SecondLoop() {
    for(int i = 0; i < Server::iterations1Sec; i++) {
        asm volatile ("":::"memory");
    }
}

