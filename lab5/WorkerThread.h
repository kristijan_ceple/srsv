//
// Created by kikyy_99 on 19.01.21.
//

#ifndef LAB5_WORKERTHREAD_H
#define LAB5_WORKERTHREAD_H

#include <iostream>
#include <queue>
#include <memory>
#include "Job.h"

class WorkerThread {
private:
    struct WorkerJob {
        Job toDoJob;
        unsigned int timeUnitsCompleted = 0;

        WorkerJob(const Job& toDoJob, unsigned int timeUnitsCompleted) : toDoJob{toDoJob},
        timeUnitsCompleted{timeUnitsCompleted} {}
    };

    pthread_t myThread = 0;
    static inline constexpr sched_param SCHED_PARAM {
            40
    };
    int processThreadId = 0;
    bool threadFinished = false;
    int threadExitRetval;

    std::deque<Job>& jobsQueue;
    pthread_mutex_t& jobsQueueMutex;
    std::vector<WorkerJob> completedJobs;

    std::unique_ptr<WorkerJob> currWorkerJob = nullptr;
    void executeJob();
    static void sleep1SecondLoop();

    pthread_cond_t& threadsSleepingCond;
    pthread_mutex_t& threadsSleepingMutex;

    unsigned int& workToDoTime;
    pthread_mutex_t& workToDoTimeMutex;

    bool& toEnd;
    pthread_rwlock_t& toEndMutex;
    bool contRunningThread();
    static void* threadMethod(void* vargp);
public:
    WorkerThread(
            int processThreadId,
            std::deque<Job>& jobsQueue, pthread_mutex_t& jobsQueueMutex,
            bool& toEnd, pthread_rwlock_t& toEndMutex,
            unsigned int& workToDoTime, pthread_mutex_t& workToDoTimeMutex,
            pthread_cond_t& threadsSleepingCond, pthread_mutex_t& threadsSleepingMutex);
    void run();
    [[nodiscard]] inline pthread_t getMyThreadId() const {return this->myThread;}
};


#endif //LAB5_WORKERTHREAD_H
